package com.dmc.myapplication.network;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.goal.GoalEditActivity;
import com.dmc.myapplication.goal.GoalInsertActivity;
import com.dmc.myapplication.goal.goalProgressActivity;
import com.dmc.myapplication.localDB.RealmFunction;
import com.dmc.myapplication.login.LoginActivity;
import com.dmc.myapplication.navFood.navFoodBiz;
import com.dmc.myapplication.navFood.navFoodFragment;
import com.dmc.myapplication.navFood.navFoodTest2Activity;
import com.dmc.myapplication.otherBodyIndex.otherBodyMeasure_add;
import com.dmc.myapplication.otherBodyIndex.otherBodyReport_add;
import com.dmc.myapplication.otherBodyIndex.otherBody_Main;
import com.dmc.myapplication.pojo.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.cert.CertificateException;
import java.util.Date;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by tsunmingtso on 19/12/2017.
 */

public class RetrofitFunction {
    private String TAG = this.getClass().getSimpleName();

    private Context context;
    private Retrofit retrofit;
    private ProgressDialog progressDialog;

    public RetrofitFunction(Context context, String baseURL) {
        this.context = context;
        retrofit = new Retrofit.Builder()
                .baseUrl(baseURL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(getUnsafeOkHttpClient())
                .build();
    }

    private static OkHttpClient getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            OkHttpClient okHttpClient = builder.build();
            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void getFoodRecord(int userID, String foodDate){
        RetrofitInterface api = retrofit.create(RetrofitInterface.class);
        Call<ResponseBody> call = api.getFoodRecord(userID, foodDate);
        progressDialog = ProgressDialog.show(context, "", "Loading...");

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.i(TAG, response.raw().toString());

                String body;
                if (response.isSuccessful()) {
                    try {
                        body = response.body().string();
                        navFoodFragment.setFoodRecord(context, body);
                        Log.i(TAG, body);
                    } catch (IOException e) {
                        Log.e(TAG, "Fail", e);
                        navFoodFragment.sendRetrieveFailedMessage();
                    }
                }
                navFoodFragment.sendRetrieveSuccessMessage();
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "Fail", t);
                navFoodFragment.sendRetrieveFailedMessage();
                progressDialog.dismiss();
            }
        });
    }

    public void insertFoodAssessment(int userID, Date insertDate, double rating, String question2, String question3a, String question3b1, String question3b2, String question3b3, String currentTimestamp) {
        RetrofitInterface api = retrofit.create(RetrofitInterface.class);
        Call<ResponseBody> call = api.saveFoodAssessment(userID, insertDate, rating, question2, question3a, question3b1, question3b2, question3b3, currentTimestamp);
        progressDialog = ProgressDialog.show(context, "", "Loading...");

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.i(TAG, response.raw().toString());

                String body;
                if (response.isSuccessful()) {
                    try {
                        body = response.body().string();
                        Log.i(TAG, body);
                    } catch (IOException e) {
                        Log.e(TAG, "Fail", e);
                        if (context instanceof navFoodTest2Activity) {
                            ((navFoodTest2Activity) context).sendFailMessage();
                        }
                    }
                }

                if (context instanceof navFoodTest2Activity) {
                    ((navFoodTest2Activity) context).sendSuccessMessage();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "Fail", t);
                if (context instanceof navFoodTest2Activity) {
                    ((navFoodTest2Activity) context).sendFailMessage();
                }
                progressDialog.dismiss();
            }
        });
    }

    public void editGoalSetting(int userID, int length, int type, String witness, String edittimestamp, JSONArray BRList, int goalID) {
        RetrofitInterface api = retrofit.create(RetrofitInterface.class);
        Call<ResponseBody> call = api.editGoalSetting(userID, length, type, witness, edittimestamp, BRList, goalID);
        progressDialog = ProgressDialog.show(context, "", "Loading...");

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.i(TAG, response.raw().toString());

                String body;
                if (response.isSuccessful()) {
                    try {
                        body = response.body().string();
                        System.out.println("body returned: " + body);
                    } catch (IOException e) {
                        Log.e(TAG, "Fail", e);
                        if (context instanceof GoalEditActivity) {
                            ((GoalEditActivity) context).sendFailMessage();
                        }
                    }
                }

                if (context instanceof GoalEditActivity) {
                    ((GoalEditActivity) context).sendSuccessMessage();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "Fail", t);
                if (context instanceof GoalEditActivity) {
                    ((GoalEditActivity) context).sendFailMessage();
                }
                progressDialog.dismiss();
            }
        });
    }

    public void insertBodyIndexRecord(int userID, int recordType, Date insertDate, double height, double weight, double waistLine, double BMI, double fat) {
        RetrofitInterface api = retrofit.create(RetrofitInterface.class);
        Call<ResponseBody> call = api.insertBodyIndexRecord(userID, recordType, insertDate, height, weight, waistLine, BMI, fat);
        progressDialog = ProgressDialog.show(context, "", "Loading...");

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.i(TAG, response.raw().toString());

                String body;
                if (response.isSuccessful()) {
                    try {
                        body = response.body().string();
                        Log.i(TAG, body);
                    } catch (IOException e) {
                        Log.e(TAG, "Fail", e);
                        if (context instanceof otherBodyMeasure_add) {
                            ((otherBodyMeasure_add) context).sendFailMessage();
                        }
                    }
                }

                if (context instanceof otherBodyMeasure_add) {
                    ((otherBodyMeasure_add) context).sendSuccessMessage();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "Fail", t);
                if (context instanceof otherBodyMeasure_add) {
                    ((otherBodyMeasure_add) context).sendFailMessage();
                }
                progressDialog.dismiss();
            }
        });
    }

    public void getBodyIndexRecords(int userID, int recordType) {
        RetrofitInterface api = retrofit.create(RetrofitInterface.class);
        Call<ResponseBody> call = api.getBodyIndexRecords(userID, recordType);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.i(TAG, response.raw().toString());

                String body;
                if (response.isSuccessful()) {
                    try {
                        body = response.body().string();

                        if (!body.trim().isEmpty()) {
                            JSONArray jsonArray = new JSONArray(body);

                            RealmFunction realmFunction = new RealmFunction(context);
                            realmFunction.deleteAllBodyIndexRecords();
                            realmFunction.saveBodyIndexRecordsFromJSON(jsonArray);
                        } else {
                            RealmFunction realmFunction = new RealmFunction(context);
                            realmFunction.deleteAllBodyIndexRecords();
                        }
                    } catch (IOException e) {
                        Log.e(TAG, "Fail", e);
                    } catch (JSONException e) {
                        Log.e(TAG, "Fail", e);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "Fail", t);
            }
        });
    }

    public void login(int userID) {
        RetrofitInterface api = retrofit.create(RetrofitInterface.class);
        Call<ResponseBody> call = api.login(userID);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.i(TAG, response.raw().toString());

                String body;
                if (response.isSuccessful()) {
                    try {
                        body = response.body().string();

                        if (!body.trim().isEmpty()) {
                            JSONObject jsonObject = new JSONObject(body);

                            RealmFunction realmFunction = new RealmFunction(context);
                            realmFunction.saveUserInfoFromJSON(jsonObject);
                        }
                    } catch (IOException e) {
                        Log.e(TAG, "Fail", e);
                    } catch (JSONException e) {
                        Log.e(TAG, "Fail", e);
                    }
                    LoginActivity.sendLoginSuccessMessage();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "Fail", t);
            }
        });
    }

    public void getBodyIndexRecords2(int userID, int recordType, String date, int length) {

        RetrofitInterface api = retrofit.create(RetrofitInterface.class);
        Call<ResponseBody> call = api.getBodyIndexRecordsByDate(userID, recordType, date, length);
        progressDialog = ProgressDialog.show(context, "", "Loading...");

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                System.out.println(response.raw().toString());

                String body;
                if (response.isSuccessful()) {
                    try {
                        body = response.body().string();

                        if (!body.trim().isEmpty()) {
                            JSONArray jsonArray = new JSONArray(body);

                            RealmFunction realmFunction = new RealmFunction(context);
                            realmFunction.deleteAllBodyIndexRecords();
                            realmFunction.saveBodyIndexRecordsFromJSON(jsonArray);

                        } else {
                            RealmFunction realmFunction = new RealmFunction(context);
                            realmFunction.deleteAllBodyIndexRecords();
                        }
                    } catch (IOException e) {
                        Log.e(TAG, "Fail", e);
                    } catch (JSONException e) {
                        Log.e(TAG, "Fail", e);
                    }
                }
                goalProgressActivity.sendSuccessMessage();
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "Fail", t);
                progressDialog.dismiss();
            }
        });
    }

    public void insertGoalSetting(int userID, Date goalDate, int length, int type, String witness, String createTimestamp, String reason, JSONArray BRList) {
        RetrofitInterface api = retrofit.create(RetrofitInterface.class);
        Call<ResponseBody> call = api.saveGoalSetting(userID, goalDate, length, type, witness, createTimestamp, reason, BRList);
        progressDialog = ProgressDialog.show(context, "", "Loading...");

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.i(TAG, response.raw().toString());

                String body;
                if (response.isSuccessful()) {
                    try {
                        body = response.body().string();
                        Log.i(TAG, body);
                    } catch (IOException e) {
                        Log.e(TAG, "Fail", e);
                        if (context instanceof GoalInsertActivity) {
                            ((GoalInsertActivity) context).sendFailMessage();
                        }
                    }
                }

                if (context instanceof GoalInsertActivity) {
                    ((GoalInsertActivity) context).sendSuccessMessage();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "Fail", t);
                if (context instanceof GoalInsertActivity) {
                    ((GoalInsertActivity) context).sendFailMessage();
                }
                progressDialog.dismiss();
            }
        });
    }

    public void insertBloodTestRecord(int userID, int recordType, Date insertDate, double fastingBloodSugar, double hba1c, double totalC, double hdlC, double ldlC, double triglycerides) {
        RetrofitInterface api = retrofit.create(RetrofitInterface.class);
        Call<ResponseBody> call = api.insertBloodTestRecord(userID, recordType, insertDate, fastingBloodSugar, hba1c, totalC, hdlC, ldlC, triglycerides);
        progressDialog = ProgressDialog.show(context, "", "Loading...");

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.i(TAG, response.raw().toString());

                String body;
                if (response.isSuccessful()) {
                    try {
                        body = response.body().string();
                        Log.i(TAG, body);
                    } catch (IOException e) {
                        Log.e(TAG, "Fail", e);
                        if (context instanceof otherBodyReport_add) {
                            ((otherBodyReport_add) context).sendFailMessage();
                        }
                    }
                }

                if (context instanceof otherBodyReport_add) {
                    ((otherBodyReport_add) context).sendSuccessMessage();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "Fail", t);
                if (context instanceof otherBodyReport_add) {
                    ((otherBodyReport_add) context).sendFailMessage();
                }
                progressDialog.dismiss();
            }
        });
    }

    public void getBloodTestRecords(int userID, int recordType) {
        RetrofitInterface api = retrofit.create(RetrofitInterface.class);
        Call<ResponseBody> call = api.getBloodTestRecords(userID, recordType);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.i(TAG, response.raw().toString());

                String body;
                if (response.isSuccessful()) {
                    try {
                        body = response.body().string();

                        if (!body.trim().isEmpty()) {
                            JSONArray jsonArray = new JSONArray(body);

                            RealmFunction realmFunction = new RealmFunction(context);
                            realmFunction.deleteAllBloodTestRecords();
                            realmFunction.saveBloodTestRecordsFromJSON(jsonArray);
                        } else {
                            RealmFunction realmFunction = new RealmFunction(context);
                            realmFunction.deleteAllBloodTestRecords();
                        }
                    } catch (IOException e) {
                        Log.e(TAG, "Fail", e);
                    } catch (JSONException e) {
                        Log.e(TAG, "Fail", e);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "Fail", t);
            }
        });
    }
}
