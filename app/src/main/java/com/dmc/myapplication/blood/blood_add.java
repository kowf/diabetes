package com.dmc.myapplication.blood;

import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.Models.bodyRecord;
import com.dmc.myapplication.R;
import com.dmc.myapplication.bodyRecord.BodyRecord;
import com.dmc.myapplication.bodyRecord.bodyAsyncTask;
import com.dmc.myapplication.bodyRecord.bodyRecordConstant;
import com.dmc.myapplication.login.User;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.systemConstant;
import com.dmc.myapplication.tool.timeCheck;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * Created by Po on 28/2/2016.
 */
public class blood_add extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private Toolbar toolbar;
    private Context ctx;
    private EditText editTextDate, editTextTime, editTextBloodValue ;
    private Spinner spinnerPeriod, spinnerType;
    private int selectedPeriod = 1;
    private int selectedType = 1;
    private Button buttonBloodAdd;
    UserLocalStore userLocalStore;
    User user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(R.anim.slide1, R.anim.slide2);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.blood_add);
        ctx = getApplicationContext();
        userLocalStore = new UserLocalStore(this);
        user = userLocalStore.getLoggedInUser();
        BodyRecord record = new BodyRecord();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // SET return <- into left hand side
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //handle the date picker
        editTextDate = (EditText) findViewById(R.id.blood_add_date);
        Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH) + 1; // because Jan =0
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        editTextDate.setText(yy + "-" + mm + "-" + dd);

        ImageView setBloodDate = (ImageView) findViewById(R.id.setBlood_add_date);
        setBloodDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new bloodDatePickerFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        //handle the time picker
        editTextTime = (EditText) findViewById(R.id.blood_add_time);
        Calendar time = Calendar.getInstance();
        int HH = time.get(Calendar.HOUR_OF_DAY);
        int MM = time.get(Calendar.MINUTE);

        editTextTime.setText(HH + ":" + MM + ":00");
        ImageView setTime = (ImageView) findViewById(R.id.setBlood_add_time);
        setTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment3  = new bloodTimePickerFragment();
                newFragment3.show(getFragmentManager(), "Time Dialog");
            }
        });
        editTextTime.addTextChangedListener(new TextWatcher(){
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            public void afterTextChanged(Editable s) {
                selectPeriod();
            }});

        //handle the period spinner drop down menu
        spinnerPeriod = (Spinner) findViewById(R.id.spinnerPeriod);

        // Spinner Drop down elements
        List<BodyRecord.periodType> period = record.getPeriodTypeList() ;
        ArrayAdapter<BodyRecord.periodType> adp= new ArrayAdapter<BodyRecord.periodType>(this,
                android.R.layout.simple_list_item_1,period);
        adp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPeriod.setAdapter(adp);
           // Spinner click listener
        spinnerPeriod.setOnItemSelectedListener(this);

        spinnerType = (Spinner) findViewById(R.id.spinnerType);
        // Spinner Drop down elements
        List<BodyRecord.gluType> gluType = record.getGluTypeList() ;
        ArrayAdapter<BodyRecord.gluType> adp1= new ArrayAdapter<BodyRecord.gluType>(this,
                android.R.layout.simple_list_item_1,gluType);
        adp1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerType.setAdapter(adp1);
        spinnerType.setOnItemSelectedListener(this);

        selectPeriod();

        //handle the blood value field
        editTextBloodValue = (EditText) findViewById(R.id.blood_add_value);

        buttonBloodAdd = (Button) findViewById(R.id.blood_add_button);
        buttonBloodAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validGlucose();
            }
        });

    }

    private void validGlucose() {

        // Reset errors.
        editTextBloodValue.setError(null);

        // Store values at the time of the login attempt.
        String glucose = editTextBloodValue.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(glucose) || !isValid(glucose)) {
            editTextBloodValue.setError(getString(R.string.blood_error));
            focusView =  editTextBloodValue;
            cancel = true;
        }
        if (!TextUtils.isEmpty(glucose)){
            if (Double.parseDouble(glucose)>bodyRecordConstant.GET_GLUCOSE_UPPER_LIMIT){
                editTextBloodValue.setError(getString(R.string.blood_warning));
                focusView =  editTextBloodValue;
                cancel = true;
            }
        }

        if (cancel) {
                  focusView.requestFocus();
        } else {

            //bodyAsyncTask addRecord = new bodyAsyncTask(this, bodyRecordConstant.SAVE_BODY_RECORD,bodyRecordConstant.GET_GLUCOSE_TYPE_CODE,  bodyRecordConstant.VIEW_MODE_GRAPH, ctx);
            //String user_id = Integer.toString(user.userid);
            //String record_type = bodyRecordConstant.GET_GLUCOSE_TYPE_CODE;
            //String date = editTextDate.getText().toString();
            //String time = editTextTime.getText().toString();
            //String period = Integer.toString(selectedPeriod);
            //String typeGlucose = Integer.toString(selectedType);
            //addRecord.execute(user_id, record_type, date, time, period,typeGlucose, glucose);

            bodyRecord bodyRecord = new bodyRecord(getApplicationContext());
            BodyRecord a = new BodyRecord();
            BodyRecord.bodyRecord bodyrecord = bodyRecord.insert(a.new bodyRecord(0, Integer.parseInt(bodyRecordConstant.GET_GLUCOSE_TYPE_CODE), (user.userid), editTextDate.getText().toString(), editTextTime.getText().toString(), 0, 0, 0, 0, 0, 0,0 , 0, (selectedPeriod), selectedType, Double.parseDouble(glucose), 0, 0, 0, 0, ""));
            Intent intent = new Intent();
            intent.setClass(this, MainActivity.class);
            intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_BLOOD);
            this.startActivity(intent);
            this.finish();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch(parent.getId()) {

            case R.id.spinnerPeriod:
                spinnerPeriod.setSelection(position);
                selectedPeriod = position + 1;
                break;
            case R.id.spinnerType:
                spinnerType.setSelection(position);
                selectedType = position + 1;
                break;
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        //for set selected item of spinner programmatically



    }
    private void selectPeriod(){
        //get set time
       /* String time = editTextTime.getText().toString();

        //get user pref time and compare to current
        if (timeCheck.compareTime(user.morning_start, user.morning_end, time)){
            spinnerPeriod.setSelection(0);
            selectedPeriod = 1;
            if (timeCheck.compareTimePlus2(user.morning_start, user.morning_end, time)){
                spinnerType.setSelection(1);
                selectedType = 2;
            } else {
                spinnerType.setSelection(0);
                selectedType = 1;
            }
        }else if (timeCheck.compareTime(user.breakfast_start, user.breakfast_end, time)){
            spinnerPeriod.setSelection(0);
            selectedPeriod = 1;
            if (timeCheck.compareTimePlus2(user.breakfast_start, user.breakfast_end, time)){
                spinnerType.setSelection(1);
                selectedType = 2;
            }else {
                spinnerType.setSelection(0);
                selectedType = 1;
            }
        }else if (timeCheck.compareTime(user.lunch_start, user.lunch_end, time)){
            spinnerPeriod.setSelection(1);
            selectedPeriod = 2;
            if (timeCheck.compareTimePlus2(user.lunch_start, user.lunch_end, time)){
                spinnerType.setSelection(1);
                selectedType = 2;
            }else {
                spinnerType.setSelection(0);
                selectedType = 1;
            }
        }else if (timeCheck.compareTime(user.dinner_start, user.dinner_end, time)){
            spinnerPeriod.setSelection(2);
            selectedPeriod = 3;
            if (timeCheck.compareTimePlus2(user.dinner_start, user.dinner_end, time)){
                spinnerType.setSelection(0);
                selectedType = 1;
            }else {
                spinnerType.setSelection(0);
                selectedType = 1;
            }
        }else if (timeCheck.compareTime(user.bed_start, user.bed_end, time)){
            spinnerPeriod.setSelection(2);
            selectedPeriod = 3;
            if (timeCheck.compareTimePlus2(user.bed_start, user.bed_end, time)){
                spinnerType.setSelection(1);
                selectedType = 2;
            }else {
                spinnerType.setSelection(0);
                selectedType = 1;
            }
        }else {
            spinnerPeriod.setSelection(0);
            selectedPeriod = 1;
            spinnerType.setSelection(0);
            selectedType = 1;
        }*/

       //Janus
        UserLocalStore userLocalStore = new UserLocalStore(getBaseContext());
        DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        try{
            Date nowtime = formatter.parse(editTextTime.getText().toString());
            System.out.println("formatter.parse(userLocalStore.getLoggedInUser().dinner_start)="+formatter.parse(userLocalStore.getLoggedInUser().dinner_start).toString());
            System.out.println("formatter.parse(userLocalStore.getLoggedInUser().dinner_end)="+formatter.parse(userLocalStore.getLoggedInUser().dinner_end).toString());
            System.out.println("cal.getTime()="+nowtime.toString());

            if ((formatter.parse(userLocalStore.getLoggedInUser().breakfast_start).before(nowtime) || formatter.parse(userLocalStore.getLoggedInUser().breakfast_start).equals(nowtime)) && (formatter.parse(userLocalStore.getLoggedInUser().breakfast_end).after(nowtime) || formatter.parse(userLocalStore.getLoggedInUser().breakfast_end).equals(nowtime))){
               System.out.println("AAAAA");
                spinnerPeriod.setSelection(0);
                selectedPeriod = 1;

                spinnerType.setSelection(0);
                selectedType = 1;
            }else if(formatter.parse(userLocalStore.getLoggedInUser().breakfast_end).before(nowtime) && formatter.parse(userLocalStore.getLoggedInUser().lunch_start).after(nowtime)){
                System.out.println("BBBB");
                spinnerPeriod.setSelection(0);
                selectedPeriod = 1;

                spinnerType.setSelection(1);
                selectedType = 2;
            }else if((formatter.parse(userLocalStore.getLoggedInUser().lunch_start).before(nowtime) || formatter.parse(userLocalStore.getLoggedInUser().lunch_start).equals(nowtime)) && (formatter.parse(userLocalStore.getLoggedInUser().lunch_end).after(nowtime) || formatter.parse(userLocalStore.getLoggedInUser().lunch_end).equals(nowtime))){
                System.out.println("CCCC");
                spinnerPeriod.setSelection(1);
                selectedPeriod = 2;

                spinnerType.setSelection(0);
                selectedType = 1;
            }else if(formatter.parse(userLocalStore.getLoggedInUser().lunch_end).before(nowtime) && formatter.parse(userLocalStore.getLoggedInUser().dinner_start).after(nowtime)){
                System.out.println("DDDDD");

                spinnerPeriod.setSelection(1);
                selectedPeriod = 2;

                spinnerType.setSelection(1);
                selectedType = 2;
            }else if ((formatter.parse(userLocalStore.getLoggedInUser().dinner_start).before(nowtime) || formatter.parse(userLocalStore.getLoggedInUser().dinner_start).equals(nowtime)) && (formatter.parse(userLocalStore.getLoggedInUser().dinner_end).after(nowtime) || formatter.parse(userLocalStore.getLoggedInUser().dinner_end).equals(nowtime))){
                System.out.println("EEEEE");

                spinnerPeriod.setSelection(2);
                selectedPeriod = 3;

                spinnerType.setSelection(0);
                selectedType = 1;
            }else if (formatter.parse(userLocalStore.getLoggedInUser().dinner_end).before(nowtime) && formatter.parse("23:59:59").after(nowtime)){
                System.out.println("FFFFF");

                spinnerPeriod.setSelection(2);
                selectedPeriod = 3;

                spinnerType.setSelection(1);
                selectedType = 2;
            }else{
                System.out.println("GGGGGG");

                spinnerPeriod.setSelection(0);
                selectedPeriod = 1;

                spinnerType.setSelection(0);
                selectedType = 1;
            }


        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private boolean isValid(String glucose){

        Double glucoseValue = Double.parseDouble(glucose);

        if ( glucoseValue <= bodyRecordConstant.GET_GLUCOSE_LOWER_LIMIT){
            return false;
        }
        try{

            if(glucose.contains(".")){
                int integerPlaces = glucose.indexOf('.');
                int decimalPlaces = glucose.length() - integerPlaces - 1;
                //System.out.println("Blood add decimalPlaces "+decimalPlaces);
            if (decimalPlaces > 1){
                  return false;
            }
            }else{
                return true;
            }

        } catch (Exception e){
            e.printStackTrace();
            return false;
        }
       return true;
    }

    @Override
    public void onBackPressed() {
        handleBackAction();
    }


    private void handleBackAction(){
        Intent intent = new Intent();
        intent.setClass(this, MainActivity.class);
        intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_BLOOD);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        finish();
    }


}
