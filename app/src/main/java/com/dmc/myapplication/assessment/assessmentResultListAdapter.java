package com.dmc.myapplication.assessment;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.dmc.myapplication.R;

import java.util.List;

/**
 * Created by KwokSinMan on 26/3/2016.
 */
public class assessmentResultListAdapter extends ArrayAdapter<String>{

    private Activity activity;
    private String[] resultItemList;
    private String[] resultValueList;
    private int[] iconList;

    public assessmentResultListAdapter(Activity activity, String[] resultItemList, String[] resultValueList, int[] iconList) {
        super(activity, R.layout.assessment_result_list, resultItemList);
        this.activity = activity;
        this.resultItemList = resultItemList;
        this.resultValueList = resultValueList;
        this.iconList = iconList;
    }

    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.assessment_result_list, null, true);

        TextView resultItem = (TextView) rowView.findViewById(R.id.resultItem);
        TextView resultValue = (TextView) rowView.findViewById(R.id.resultValue);
        ImageView icon = (ImageView) rowView.findViewById(R.id.icon);

        resultItem.setText(resultItemList[position]);
        resultValue.setText(resultValueList[position]);
        icon.setImageResource(iconList[position]);
        if(iconList[position] == R.drawable.ic_smoke_free_black_48dp)
            icon.setColorFilter(getContext().getResources().getColor(R.color.smokeIcon));
        if(iconList[position] == R.drawable.ic_face_black_48dp)
            icon.setColorFilter(getContext().getResources().getColor(R.color.fatIcon));

        return rowView;
    }


}
