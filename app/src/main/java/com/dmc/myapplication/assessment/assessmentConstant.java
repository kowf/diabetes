package com.dmc.myapplication.assessment;

import com.dmc.myapplication.R;

/**
 * Created by KwokSinMan on 26/3/2016.
 */
public class assessmentConstant {
    public final static int assessmentResultSize = 7;
    public final static String[] assessmentResultItem = {"吸煙","肥胖","糖化血紅蛋白","血壓","血脂、膽固醇","飲食","中等強度運動"};
    public final static int[] iconPath = {R.drawable.ic_smoke_free_black_48dp,R.drawable.ic_face_black_48dp,
            R.drawable.ic_blood_48dp,R.drawable.ic_meter_48dp,
            R.drawable.ic_heart_pulse,R.drawable.ic_eat_48dp,R.drawable.ic_gym_48dp};

    public final static String GET_ASSESSMENT_RECORD = "assessment/getAssessmentRecord.php";
    public final static String GET_FOOD_ASSESSMENT_RECORD = "assessment/getFoodAssessmentRecord.php";
    public final static String DELETE_FOOD_ASSESSMENT_RECORD = "assessment/deleteFoodAssessmentRecord.php";
    public final static String SAVE_FOOD_ASSESSMENT_RECORD = "assessment/saveFoodAssessmentRecord.php";


    public final static String ansA = "A"; //完全達到
    public final static String ansB = "B"; //大部分時間做到 (4至6天)
    public final static String ansC = "C"; //有些時候做到 (2至3天)
    public final static String ansD = "D"; //未能達到
    public final static String ansN = "N"; //不適用, 我沒有見過營養師

    public final static String standardCodeA = "A"; //完全達到
    public final static String standardCodeB = "B"; //大部分時間做到
    public final static String standardCodeC = "C"; // 有些時候做到
    public final static String standardCodeD = "D"; //未能達到
    public final static String standardCodeN = "N"; //沒有數據，因此未能提供

    public final static String standardA = "完全達到";
    public final static String standardB = "大部分時間做到";
    public final static String standardC = "有些時候做到";
    public final static String standardD = "未能達到";
    public final static String standardN = "沒有數據，因此未能提供";


    public final static int notSelectedRadio = -1;

    public final static String GET_ASSESSMENT_WEEK = "redirect_assessment_week";
    public final static String GET_ASSESSMENT_YEAR = "redirect_assessment_year";

}
