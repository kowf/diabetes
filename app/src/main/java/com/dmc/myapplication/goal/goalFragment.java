package com.dmc.myapplication.goal;

import android.content.DialogInterface;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.dmc.myapplication.R;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.navFood.navFoodConstant;
import com.dmc.myapplication.navFood.navfoodAsyncTask;

/**
 * Created by lamivan on 1/1/2018.
 */

public class goalFragment extends Fragment {
  //recyclerview objects
  private RecyclerView recyclerView;
  private RecyclerView.LayoutManager layoutManager;
  private RecyclerView.Adapter adapter;
  TextView textView;
  //model object for our list data
  private List<MyList> list;
  String getFoodDate;
  int goalOptions = 0;
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    getActivity().setTitle(R.string.goal_setting_main);

    View v = inflater.inflate(R.layout.goal_setting_main, container, false);
    getFoodDate = this.getArguments().getString(navFoodConstant.GET_FOOD_DATE);

    System.out.println(getFoodDate);
    recyclerView = (RecyclerView) v.findViewById(R.id.recyclerView);
    recyclerView.setVisibility(View.VISIBLE);
    recyclerView.setHasFixedSize(true);
    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    textView = (TextView) v.findViewById(R.id.abc);
    textView.setVisibility(View.GONE);
    setHasOptionsMenu(true);
    //list = new ArrayList<>();
   // loadRecyclerViewItem();
    UserLocalStore userLocalStores = new UserLocalStore(getActivity());
    String userId = Integer.toString(userLocalStores.getLoggedInUser().userid);
    System.out.println("what f " + userId);
    navfoodAsyncTask asyncTask = new navfoodAsyncTask(getActivity(), navFoodConstant.GET_GOAL);
    asyncTask.execute(userId);

   return v;
  }

  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    inflater.inflate(R.menu.goal_setting_menu, menu);
    super.onCreateOptionsMenu(menu, inflater);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle item selection

    switch (item.getItemId()) {
      case R.id.addgoal:
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        builder.setMessage("請選擇欲訂立的目標種類")
                .setNeutralButton("每週目標", new DialogInterface.OnClickListener() {
                  @Override
                  public void onClick(DialogInterface dialogInterface, int i) {
                    goalOptions = 0;
                    dialogInterface.dismiss();
                    Intent intent = new Intent(getActivity(), GoalInsertActivity.class);
                    intent.putExtra(navFoodConstant.GET_FOOD_DATE, getFoodDate);
                    intent.putExtra("goalOptions", goalOptions);
                    getActivity().startActivity(intent);
                    getActivity().finish();
                  }
                })
                .setNegativeButton("中期目標", new DialogInterface.OnClickListener() {
                  @Override
                  public void onClick(DialogInterface dialogInterface, int i) {
                    goalOptions = 1;
                    dialogInterface.dismiss();
                    Intent intent = new Intent(getActivity(), GoalInsertActivity.class);
                    intent.putExtra(navFoodConstant.GET_FOOD_DATE, getFoodDate);
                    intent.putExtra("goalOptions", goalOptions);
                    getActivity().startActivity(intent);
                    getActivity().finish();
                  }
                })
                .setPositiveButton("年度目標", new DialogInterface.OnClickListener() {
                  @Override
                   public void onClick(DialogInterface dialogInterface, int i) {
                    goalOptions = 2;
                    dialogInterface.dismiss();
                    Intent intent = new Intent(getActivity(), GoalInsertActivity.class);
                    intent.putExtra(navFoodConstant.GET_FOOD_DATE, getFoodDate);
                    intent.putExtra("goalOptions", goalOptions);
                    getActivity().startActivity(intent);
                    getActivity().finish();
                  }
                })
                .show();


        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }
 // private void loadRecyclerViewItem() {
    //you can fetch the data from server or some apis
    //for this tutorial I am adding some dummy data directly
   // for (int i = 1; i <= 5; i++) {
     // MyList myList = new MyList(
       //       "Dummy Heading " + i,
         //     "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi molestie nisi dui."
      //);
      //list.add(myList);
    //}

//    adapter = new CustomAdapter(list, getActivity(), getActivity());
  //  recyclerView.setAdapter(adapter);
  //}

}
