package com.dmc.myapplication.goal;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Handler;
import android.os.Message;
import android.os.ParcelFileDescriptor;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.R;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.navFood.navFoodConstant;
import com.dmc.myapplication.navFood.navFoodRecordDatePickerFragment;
import com.dmc.myapplication.navFood.navFoodTime;
import com.dmc.myapplication.network.RetrofitFunction;
import com.dmc.myapplication.network.webService;
import com.dmc.myapplication.systemConstant;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class GoalEditActivity extends AppCompatActivity {
  LoginButton share; String witness, exercise0, exercise1, exercise2;
  String goalID, getFoodDate, length, steps, weightEditUnit, waistLineEditUnit, weightEdit, waistLineEdit;
  Button setFoodRecordTime, saveGoalSetting, edit, delete;
  private String TAG = this.getClass().getSimpleName();
  private EditText et_witness, et_WaistLine, et_Weight, et_SimpleEx, et_MediumEx, et_StrongEx, et_Steps;
  private CheckBox checkBox_simple, checkBox_medium, checkBox_strong;
  private Spinner sp_WaistLineUnit, sp_WeightUnit;
  final String[] waistLineUnit = {"厘米", "吋"};
  final String[] weightUnit = {"磅", "公斤"};
  private static Handler handler;
  int type = 0;
  private double weight, waistLine = 0;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    FacebookSdk.sdkInitialize(getApplicationContext());

    setContentView(R.layout.activity_goal_edit);
    setHandlerFunction();

    getFoodDate = getIntent().getBundleExtra("parameters").getString("date");
    System.out.println(getFoodDate);

    TextView navFoodDate = (TextView)  findViewById(R.id.navFoodRecordDate);
    navFoodDate.setText(new navFoodTime().getUIDateFormal(getFoodDate));

    // set data picker
    ImageView setFoodRecordDate = (ImageView) findViewById(R.id.setNavFoodRecordDate);
    setFoodRecordDate.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View arg0) {
        DialogFragment newFragment = new navFoodRecordDatePickerFragment();
        newFragment.show(getFragmentManager(), "DatePicker");
      }
    });
    // textView = (TextView) findViewById(R.id.navGoalLength);
    saveGoalSetting = (Button) findViewById(R.id.save);
    saveGoalSetting.setVisibility(View.GONE);
    share =  (LoginButton) findViewById(R.id.login_button);
    share.setTextLocale(Locale.TAIWAN);
    share.setVisibility(View.GONE);
    edit = (Button) findViewById(R.id.edit);
    delete = (Button) findViewById(R.id.delete);
    delete.setEnabled(false);

    // set time picker
    length = getIntent().getBundleExtra("parameters").getString("length");
    goalID = getIntent().getBundleExtra("parameters").getString("goalID");
    System.out.println("goal id viewing is: ... " + goalID);

    weightEdit = getIntent().getBundleExtra("parameters").getString("weight");
    waistLineEdit = getIntent().getBundleExtra("parameters").getString("waist");
    weightEditUnit = getIntent().getBundleExtra("parameters").getString("weightUnit");
    waistLineEditUnit = getIntent().getBundleExtra("parameters").getString("waistUnit");
    steps = getIntent().getBundleExtra("parameters").getString("steps");
    witness = getIntent().getBundleExtra("parameters").getString("witness");
    if (getIntent().getBundleExtra("parameters").getString("small") != null){
      exercise0 = getIntent().getBundleExtra("parameters").getString("small");
    } else {
      exercise0 = "empty";
    }
    if (getIntent().getBundleExtra("parameters").getString("middle") != null){
      exercise1 = getIntent().getBundleExtra("parameters").getString("middle");
    } else {
      exercise1 = "empty";
    }
    if (getIntent().getBundleExtra("parameters").getString("big") != null){
      exercise2 = getIntent().getBundleExtra("parameters").getString("big");
    } else {
      exercise2 = "empty";
    }
    setFoodRecordTime = (Button) findViewById(R.id.navGoalLength);
    setFoodRecordTime.setText(length);
    //weekly and yearly goals, length is fixed
    if (length.equals(String.valueOf(1)) || length.equals(String.valueOf(52))){
      setFoodRecordTime.setEnabled(false);
      if (length.equals(String.valueOf(52))){
        type = 2;
      }
    } else {
      type = 1;
      setFoodRecordTime.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View arg0) {
          final Dialog dialog = new Dialog(GoalEditActivity.this);
          dialog.setTitle("選擇目標長度（週數）");
          dialog.setContentView(R.layout.abc);
          dialog.setCancelable(true);
          NumberPicker picker = (NumberPicker) dialog.findViewById(R.id.numberPicker);
          picker.setMaxValue(51);
          picker.setMinValue(2);
          picker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
              setFoodRecordTime.setText("" + picker.getValue());

            }
          });
          Button button2 = (Button) dialog.findViewById(R.id.button2);
          button2.setText("確定");
          button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              dialog.dismiss();
            }
          });
          dialog.show();
        }
      });
    }
    et_Weight = (EditText) findViewById(R.id.et_Weight);
    sp_WeightUnit = (Spinner) findViewById(R.id.sp_WeightUnit);
    sp_WeightUnit.setEnabled(false);
    et_WaistLine = (EditText) findViewById(R.id.et_WaistLine);
    sp_WaistLineUnit = (Spinner) findViewById(R.id.sp_WaistLineUnit);
    sp_WaistLineUnit.setEnabled(false);
    et_Weight.setText(weightEdit);
    et_WaistLine.setText(waistLineEdit);

    et_witness = (EditText) findViewById(R.id.setWitness);
    et_SimpleEx = (EditText) findViewById(R.id.et_small);
    et_MediumEx = (EditText) findViewById(R.id.et_medium);
    et_StrongEx = (EditText) findViewById(R.id.et_large);
    et_Steps = (EditText) findViewById(R.id.et_step);
    et_Steps.setText(steps);
    et_witness.setText(witness);
    setupSpinner();

    checkBox_simple = (CheckBox) findViewById(R.id.ck_small);
    checkBox_medium = (CheckBox) findViewById(R.id.ck_medium);
    checkBox_strong = (CheckBox) findViewById(R.id.ck_large);

    if (exercise0 != "empty"){
      checkBox_simple.setChecked(true);
      et_SimpleEx.setText("" + exercise0);
    }
    if (exercise1 != "empty"){
      checkBox_medium.setChecked(true);
      et_MediumEx.setText("" + exercise1);
    }
    if (exercise2 != "empty"){
      checkBox_strong.setChecked(true);
      et_StrongEx.setText("" + exercise2);
    }
    edit.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        weight = Double.valueOf(et_Weight.getText().toString());
        waistLine = Double.valueOf(et_WaistLine.getText().toString());
        UserLocalStore userLocalStores = new UserLocalStore(getApplicationContext());
        String userId = Integer.toString(userLocalStores.getLoggedInUser().userid);

        SimpleDateFormat sss = new SimpleDateFormat("yyyy-mm-dd");
        Date date = null;
        try {
          date = sss.parse(getFoodDate);
        } catch (ParseException e) {
          e.printStackTrace();
        }
        System.out.println("parse-date " + date);

        SimpleDateFormat ss = new SimpleDateFormat("yyyyMMddhhmmssSSS");
        String currentTimestamp = ss.format(new Date());

        if ((et_SimpleEx.getText().toString().equals("") && checkBox_simple.isChecked()) || (et_MediumEx.getText().toString().equals("") && checkBox_medium.isChecked()) || (et_StrongEx.getText().toString().equals("") && checkBox_strong.isChecked()) || et_Steps.getText().toString().equals("") || et_witness.getText().toString().equals("") || ((weight > 7 && sp_WeightUnit.getSelectedItemPosition() == 0) || (weight > 3 && sp_WeightUnit.getSelectedItemPosition() == 1)
        ) ||  ((waistLine > 5.1 && sp_WaistLineUnit.getSelectedItemPosition() == 0) || (waistLine > 2 && sp_WaistLineUnit.getSelectedItemPosition() == 1)
        ) ){
          System.out.println("wrong");
          if (((weight > 7 && sp_WeightUnit.getSelectedItemPosition() == 0) || (weight > 3 && sp_WeightUnit.getSelectedItemPosition() == 1)
          ) ||  ((waistLine > 5.1 && sp_WaistLineUnit.getSelectedItemPosition() == 0) || (waistLine > 2 && sp_WaistLineUnit.getSelectedItemPosition() == 1)
          )  ){
            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(GoalEditActivity.this);
            builder.setMessage("十分欣賞你的決心，但訂立一個循序漸進的減重目標也十分重要！")
                    .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                      @Override
                      public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                      }
                    })
                    .show();
          }
        } else {
          System.out.println("inputted weight = " + weight + " unit = " + weightUnit[sp_WeightUnit.getSelectedItemPosition()]);
          System.out.println("inputted waist = " + waistLine + " unit = " + waistLineUnit[sp_WaistLineUnit.getSelectedItemPosition()]);

          List<Integer> listVal = new ArrayList<>();
          listVal.add(1);
          listVal.add(2);

          List<Double> expectedVal = new ArrayList<>();
          expectedVal.add(weight);
          expectedVal.add(waistLine);

          List<String> unit = new ArrayList<>();
          if (sp_WeightUnit.getSelectedItemPosition() == 0) {
            unit.add("lbs");
          } else if (sp_WeightUnit.getSelectedItemPosition() == 1){
            unit.add("kg");
          }
          if (sp_WaistLineUnit.getSelectedItemPosition() == 0){
            unit.add("cm");
          } else if (sp_WaistLineUnit.getSelectedItemPosition() == 1){
            unit.add("inch");
          }

          if (checkBox_simple.isChecked()){
            listVal.add(3);
            unit.add("min");
            expectedVal.add(Double.valueOf(et_SimpleEx.getText().toString()));
          }
          if (checkBox_medium.isChecked()){
            listVal.add(4);
            unit.add("min");
            expectedVal.add(Double.valueOf(et_MediumEx.getText().toString()));
          }
          if (checkBox_strong.isChecked()){
            listVal.add(5);
            unit.add("min");
            expectedVal.add(Double.valueOf(et_StrongEx.getText().toString()));
          }
          listVal.add(6);
          unit.add("steps");
          expectedVal.add(Double.valueOf(et_Steps.getText().toString()));


          JSONArray BRList = new JSONArray();
          for (int i = 0; i<listVal.size(); i++) {
            try {
              JSONObject item = new JSONObject();
              item.put("type", String.valueOf(listVal.get(i)));
              item.put("expectedValue", String.valueOf(expectedVal.get(i)));
             // item.put("unit", unit.get(i));
             // item.put("create", currentTimestamp);
              BRList.put(item);
            } catch (JSONException e) {
              e.printStackTrace();
            }
          }
          RetrofitFunction retrofitFunction = new RetrofitFunction(GoalEditActivity.this, webService.serverURL);
          retrofitFunction.editGoalSetting(Integer.valueOf(userId), Integer.valueOf(setFoodRecordTime.getText().toString()), type + 1, et_witness.getText().toString(), currentTimestamp, BRList, Integer.valueOf(goalID));

        }

      }
    });
    ActionBar actionBar = getSupportActionBar();
    actionBar.setDisplayHomeAsUpEnabled(true);
    actionBar.setHomeButtonEnabled(true);
    actionBar.setTitle("新增目標");
  }
  private void setupSpinner() {

    ArrayAdapter<String> weightUnitList = new ArrayAdapter<>(this,
            android.R.layout.simple_spinner_dropdown_item,
            weightUnit);
    sp_WeightUnit.setAdapter(weightUnitList);


    ArrayAdapter<String> waistLineUnitList = new ArrayAdapter<>(this,
            android.R.layout.simple_spinner_dropdown_item,
            waistLineUnit);
    sp_WaistLineUnit.setAdapter(waistLineUnitList);

    if (weightEditUnit.equals("kg")) {
      sp_WeightUnit.setSelection(1);
    } else {
      sp_WeightUnit.setSelection(0);
    }
    if (waistLineEditUnit.equals("cm")) {
      sp_WaistLineUnit.setSelection(0);
    } else {
      sp_WaistLineUnit.setSelection(1);
    }

  }


  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        handleBackAction();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }
  private void setHandlerFunction() {
    final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
    handler = new Handler() {
      @Override
      public void handleMessage(Message msg) {
        switch (msg.what) {
          case 0:
            builder.setMessage("資料已成功儲存")
                    .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                      @Override
                      public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        handleBackAction();
                      }
                    }).show();
            break;
          case 1:
            builder.setMessage("請稍後再試")
                    .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                      @Override
                      public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                      }
                    })
                    .show();
            break;
        }
      }
    };
  }

  public void sendSuccessMessage() {
    Message message = new Message();
    message.what = 0;
    handler.sendMessage(message);
  }

  public void sendFailMessage() {
    Message message = new Message();
    message.what = 1;
    handler.sendMessage(message);
  }
  @Override
  public void onBackPressed() {
    handleBackAction();
  }

  private void handleBackAction(){
    Intent intent = new Intent();
    intent.setClass(this, MainActivity.class);
    intent.putExtra(systemConstant.REDIRECT_PAGE ,systemConstant.DISPLAY_GOAL);
    startActivity(intent);
    overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
    finish();
  }
}
