package com.dmc.myapplication.goal;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dmc.myapplication.R;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.navFood.navFoodConstant;
import com.dmc.myapplication.navFood.navfoodAsyncTask;

import java.util.List;
/**
 * Created by lamivan on 1/1/2018.
 */

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.ViewHolder>{

  private List<MyList> list;
  private Context mCtx;
  private Activity activity;

  public CustomAdapter(List<MyList> list, Context mCtx, Activity activity) {
    this.list = list;
    this.mCtx = mCtx;
    this.activity = activity;
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View v = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.goal_setting_child_item, parent, false);
    return new ViewHolder(v);
  }
  @Override
  public void onBindViewHolder(final CustomAdapter.ViewHolder holder, int position) {
    final MyList myList = list.get(position);
    holder.textViewHead.setText(myList.getHead());
    holder.textViewDesc.setText(myList.getDesc());

    holder.buttonViewOption.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {

        //creating a popup menu
        PopupMenu popup = new PopupMenu(mCtx, holder.buttonViewOption);
        //inflating menu from xml resource
        popup.inflate(R.menu.options_menu);
        //adding click listener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
          @Override
          public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {
              case R.id.menu1:
                System.out.println(myList.getDesc().substring(1));
                UserLocalStore userLocalStores = new UserLocalStore(activity);
                String userId = Integer.toString(userLocalStores.getLoggedInUser().userid);
                navfoodAsyncTask asyncTask = new navfoodAsyncTask(activity, navFoodConstant.GET_GOAL_DETAIL);
                asyncTask.execute(userId, myList.getDesc().substring(1), myList.getHead().split(":")[1], myList.getHead().split(":")[2], "insert", myList.getWitness());
                break;
              case R.id.menu2:
                //handle menu2 click
                System.out.println(myList.getDesc().substring(1));
                userLocalStores = new UserLocalStore(activity);
                userId = Integer.toString(userLocalStores.getLoggedInUser().userid);
                asyncTask = new navfoodAsyncTask(activity, navFoodConstant.GET_GOAL_DETAIL);
                asyncTask.execute(userId, myList.getDesc().substring(1), myList.getHead().split(":")[1], myList.getHead().split(":")[2], "edit", myList.getWitness());

                break;
            }
            return false;
          }
        });
        //displaying the popup
        popup.show();

      }
    });
  }


  @Override
  public int getItemCount() {
    return list.size();
  }


  public class ViewHolder extends RecyclerView.ViewHolder {

    public TextView textViewHead;
    public TextView textViewDesc;
    public TextView buttonViewOption;

    public ViewHolder(View itemView) {
      super(itemView);

      textViewHead = (TextView) itemView.findViewById(R.id.textViewHead);
      textViewDesc = (TextView) itemView.findViewById(R.id.textViewDesc);
      buttonViewOption = (TextView) itemView.findViewById(R.id.textViewOptions);
    }
  }
}
