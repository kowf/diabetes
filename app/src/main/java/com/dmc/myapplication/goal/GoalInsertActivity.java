package com.dmc.myapplication.goal;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInstaller;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaCas;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.R;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.navFood.navFoodConstant;
import com.dmc.myapplication.navFood.navFoodRecordDatePickerFragment;
import com.dmc.myapplication.navFood.navFoodRecordNumberPickerFragment;
import com.dmc.myapplication.navFood.navFoodRecordTimePickerFragment;
import com.dmc.myapplication.navFood.navFoodTime;
import com.dmc.myapplication.navFood.navFoodViewDatePickerFragment;
import com.dmc.myapplication.network.RetrofitFunction;
import com.dmc.myapplication.network.webService;
import com.dmc.myapplication.systemConstant;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import android.os.ParcelFileDescriptor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.graphics.Canvas;
import android.graphics.Bitmap.Config;

import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphResponse;
import com.facebook.GraphRequest;
import com.facebook.AccessToken;
import com.facebook.HttpMethod;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.FacebookSdk;
import com.facebook.CallbackManager;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;

public class GoalInsertActivity extends AppCompatActivity {
  int goalOptions = 0;
  LoginButton share;
  CallbackManager callbackManager;
  ShareDialog shareDialog;
  Button setFoodRecordTime, saveGoalSetting, edit, delete;
  private String TAG = this.getClass().getSimpleName();
  private EditText et_witness, et_WaistLine, et_Weight, et_SimpleEx, et_MediumEx, et_StrongEx, et_Steps;
  private CheckBox checkBox_simple, checkBox_medium, checkBox_strong;
  private Spinner sp_WaistLineUnit, sp_WeightUnit;
  final String[] waistLineUnit = {"厘米", "吋"};
  final String[] weightUnit = {"磅", "公斤"};
  private static Handler handler;
  private double weight, waistLine = 0;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    FacebookSdk.sdkInitialize(getApplicationContext());
    AppEventsLogger.activateApp(this);
    setContentView(R.layout.activity_goal_insert);
    setHandlerFunction();

    System.out.println(FacebookSdk.getApplicationSignature(GoalInsertActivity.this));

    final String getFoodDate = getIntent().getExtras().getString(navFoodConstant.GET_FOOD_DATE);
    System.out.println(getFoodDate);
    goalOptions = getIntent().getExtras().getInt("goalOptions", 0);
    TextView navFoodDate = (TextView)  findViewById(R.id.navFoodRecordDate);
    navFoodDate.setText(new navFoodTime().getUIDateFormal(getFoodDate));

    // set data picker
    ImageView setFoodRecordDate = (ImageView) findViewById(R.id.setNavFoodRecordDate);
    setFoodRecordDate.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View arg0) {
        DialogFragment newFragment = new navFoodRecordDatePickerFragment();
        newFragment.show(getFragmentManager(), "DatePicker");
      }
    });
    // textView = (TextView) findViewById(R.id.navGoalLength);
    saveGoalSetting = (Button) findViewById(R.id.save);

    callbackManager = CallbackManager.Factory.create();
    LoginManager.getInstance().registerCallback(callbackManager,
            new FacebookCallback<LoginResult>() {
              @Override
              public void onSuccess(LoginResult loginResult) {
                // App code
              }

              @Override
              public void onCancel() {
                // App code
              }

              @Override
              public void onError(FacebookException exception) {
                // App code
              }
            });

    shareDialog = new ShareDialog(this);
    final boolean loggedIn = AccessToken.getCurrentAccessToken() == null;
    share =  (LoginButton) findViewById(R.id.login_button);
    share.setReadPermissions("email"); //, "user_managed_groups");
    share.setTextLocale(Locale.TAIWAN);
    share.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {

        if (!loggedIn) {
        try {
          PackageInfo info = getPackageManager().getPackageInfo(
                  "com.dmc.myapplication",
                  PackageManager.GET_SIGNATURES);
          for (android.content.pm.Signature signature : info.signatures) {
            MessageDigest md = MessageDigest.getInstance("SHA");
            md.update(signature.toByteArray());
            Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
          }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
      // LoginManager.getInstance().logInWithReadPermissions(GoalInsertActivity.this, Arrays.asList("user_managed_groups"));
         // LoginManager.getInstance().logInWithPublishPermissions(GoalInsertActivity.this, Arrays.asList("publish_actions"));
        }
        //ensure the share has correct values set
        if ((et_SimpleEx.getText().toString().equals("") && checkBox_simple.isChecked()) || (et_MediumEx.getText().toString().equals("") && checkBox_medium.isChecked()) || (et_StrongEx.getText().toString().equals("") && checkBox_strong.isChecked()) || et_Steps.getText().toString().equals("") || et_witness.getText().toString().equals("") || ((weight > 7 && sp_WeightUnit.getSelectedItemPosition() == 0) || (weight > 3 && sp_WeightUnit.getSelectedItemPosition() == 1)
        ) ||  ((waistLine > 5.1 && sp_WaistLineUnit.getSelectedItemPosition() == 0) || (waistLine > 2 && sp_WaistLineUnit.getSelectedItemPosition() == 1)
        ) ) {
          android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(GoalInsertActivity.this);
          builder.setMessage("請分享您的目標前完成目標設定！")
                  .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                      dialogInterface.dismiss();
                    }
                  })
                  .show();
        } else {
          if (!share.getText().toString().equals("登出")) {
            ScrollView view2 = (ScrollView) findViewById(R.id.scrollView);
            view2.scrollTo(0, 0);
            View view1 = getWindow().getDecorView().getRootView();
            Bitmap image = screenShot(view1);
            if (ShareDialog.canShow(ShareLinkContent.class)) {
              SharePhoto photo = new SharePhoto.Builder()
                      .setBitmap(image)
                      .build();
              SharePhotoContent content = new SharePhotoContent.Builder()
                      .addPhoto(photo)
                      .build();
              shareDialog.show(content);
            }
          }
        }
      }
    });
    edit = (Button) findViewById(R.id.edit);
    edit.setVisibility(View.GONE);
    delete = (Button) findViewById(R.id.delete);
    delete.setVisibility(View.GONE);
    // set time picker

    setFoodRecordTime = (Button) findViewById(R.id.navGoalLength);
    if (goalOptions == 0){
      setFoodRecordTime.setText("" + 1);
      setFoodRecordTime.setEnabled(false);
    } else if (goalOptions == 2) {
      setFoodRecordTime.setText("" + 52);
      setFoodRecordTime.setEnabled(false);
    } else {
      setFoodRecordTime.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View arg0) {
          final Dialog dialog = new Dialog(GoalInsertActivity.this);
          dialog.setTitle("選擇目標長度（週數）");
          dialog.setContentView(R.layout.abc);
          dialog.setCancelable(true);
          NumberPicker picker = (NumberPicker) dialog.findViewById(R.id.numberPicker);
          picker.setMaxValue(51);
          picker.setMinValue(2);
          picker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
              setFoodRecordTime.setText("" + picker.getValue());

            }
          });
          Button button2 = (Button) dialog.findViewById(R.id.button2);
          button2.setText("確定");
          button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              dialog.dismiss();
            }
          });
          dialog.show();
        }
      });
    }
    et_Weight = (EditText) findViewById(R.id.et_Weight);
    sp_WeightUnit = (Spinner) findViewById(R.id.sp_WeightUnit);
    et_WaistLine = (EditText) findViewById(R.id.et_WaistLine);
    sp_WaistLineUnit = (Spinner) findViewById(R.id.sp_WaistLineUnit);

    et_witness = (EditText) findViewById(R.id.setWitness);
    et_SimpleEx = (EditText) findViewById(R.id.et_small);
    et_MediumEx = (EditText) findViewById(R.id.et_medium);
    et_StrongEx = (EditText) findViewById(R.id.et_large);
    et_Steps = (EditText) findViewById(R.id.et_step);
    setupSpinner();
    setupEditText();

    checkBox_simple = (CheckBox) findViewById(R.id.ck_small);
    checkBox_medium = (CheckBox) findViewById(R.id.ck_medium);
    checkBox_strong = (CheckBox) findViewById(R.id.ck_large);

    saveGoalSetting.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        UserLocalStore userLocalStores = new UserLocalStore(getApplicationContext());
        String userId = Integer.toString(userLocalStores.getLoggedInUser().userid);

        SimpleDateFormat sss = new SimpleDateFormat("yyyy-mm-dd");
        Date date = null;
        try {
          date = sss.parse(getFoodDate);
        } catch (ParseException e) {
          e.printStackTrace();
        }
        System.out.println("parse-date " + date);

        SimpleDateFormat ss = new SimpleDateFormat("yyyyMMddhhmmssSSS");
        String currentTimestamp = ss.format(new Date());

        if ((et_SimpleEx.getText().toString().equals("") && checkBox_simple.isChecked()) || (et_MediumEx.getText().toString().equals("") && checkBox_medium.isChecked()) || (et_StrongEx.getText().toString().equals("") && checkBox_strong.isChecked()) || et_Steps.getText().toString().equals("") || et_witness.getText().toString().equals("") || ((weight > 7 && sp_WeightUnit.getSelectedItemPosition() == 0) || (weight > 3 && sp_WeightUnit.getSelectedItemPosition() == 1)
        ) ||  ((waistLine > 5.1 && sp_WaistLineUnit.getSelectedItemPosition() == 0) || (waistLine > 2 && sp_WaistLineUnit.getSelectedItemPosition() == 1)
        ) ){
            System.out.println("wrong");
          if (((weight > 7 && sp_WeightUnit.getSelectedItemPosition() == 0) || (weight > 3 && sp_WeightUnit.getSelectedItemPosition() == 1)
          ) ||  ((waistLine > 5.1 && sp_WaistLineUnit.getSelectedItemPosition() == 0) || (waistLine > 2 && sp_WaistLineUnit.getSelectedItemPosition() == 1)
          )  ){
            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(GoalInsertActivity.this);
            builder.setMessage("十分欣賞你的決心，但訂立一個循序漸進的減重目標也十分重要！")
                    .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                      @Override
                      public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                      }
                    })
                    .show();
          }
        } else {
          System.out.println("inputted weight = " + weight + " unit = " + weightUnit[sp_WeightUnit.getSelectedItemPosition()]);
          System.out.println("inputted waist = " + waistLine + " unit = " + waistLineUnit[sp_WaistLineUnit.getSelectedItemPosition()]);

          List<Integer> listVal = new ArrayList<>();
          listVal.add(1);
          listVal.add(2);

          List<Double> expectedVal = new ArrayList<>();
          expectedVal.add(weight);
          expectedVal.add(waistLine);

          List<String> unit = new ArrayList<>();
          if (sp_WeightUnit.getSelectedItemPosition() == 0) {
            unit.add("lbs");
          } else if (sp_WeightUnit.getSelectedItemPosition() == 1){
            unit.add("kg");
          }
          if (sp_WaistLineUnit.getSelectedItemPosition() == 0){
            unit.add("cm");
          } else if (sp_WaistLineUnit.getSelectedItemPosition() == 1){
            unit.add("inch");
          }

          if (checkBox_simple.isChecked()){
            listVal.add(3);
            unit.add("min");
            expectedVal.add(Double.valueOf(et_SimpleEx.getText().toString()));
          }
          if (checkBox_medium.isChecked()){
            listVal.add(4);
            unit.add("min");
            expectedVal.add(Double.valueOf(et_MediumEx.getText().toString()));
          }
          if (checkBox_strong.isChecked()){
            listVal.add(5);
            unit.add("min");
            expectedVal.add(Double.valueOf(et_StrongEx.getText().toString()));
          }
          listVal.add(6);
          unit.add("steps");
          expectedVal.add(Double.valueOf(et_Steps.getText().toString()));


         JSONArray BRList = new JSONArray();
          for (int i = 0; i<listVal.size(); i++) {
            try {
              JSONObject item = new JSONObject();
              item.put("type", String.valueOf(listVal.get(i)));
              item.put("expectedValue", String.valueOf(expectedVal.get(i)));
              item.put("unit", unit.get(i));
              item.put("create", currentTimestamp);
              BRList.put(item);
            } catch (JSONException e) {
              e.printStackTrace();
            }
          }
          RetrofitFunction retrofitFunction = new RetrofitFunction(GoalInsertActivity.this, webService.serverURL);
          retrofitFunction.insertGoalSetting(Integer.valueOf(userId), date, Integer.valueOf(setFoodRecordTime.getText().toString()), goalOptions + 1, et_witness.getText().toString(), currentTimestamp, "abc", BRList );

        }

      }
    });

    ActionBar actionBar = getSupportActionBar();
    actionBar.setDisplayHomeAsUpEnabled(true);
    actionBar.setHomeButtonEnabled(true);
    actionBar.setTitle("新增目標");
  }

  public Bitmap screenShot(View view) {
    Bitmap bitmap = Bitmap.createBitmap(view.getWidth(),
            view.getHeight(), Config.ARGB_8888);
    Canvas canvas = new Canvas(bitmap);
    view.draw(canvas);
    return bitmap;
  }
  @Override
  protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    callbackManager.onActivityResult(requestCode, resultCode, data);
  }

  public ParcelFileDescriptor getImageFormData(File image) {
    try {
      return ParcelFileDescriptor.open(image, ParcelFileDescriptor.MODE_READ_ONLY);
    } catch (FileNotFoundException e) {
      return null;
    }
  }
  private void setupSpinner() {

    ArrayAdapter<String> weightUnitList = new ArrayAdapter<>(this,
            android.R.layout.simple_spinner_dropdown_item,
            weightUnit);
    sp_WeightUnit.setAdapter(weightUnitList);

    sp_WeightUnit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        ((TextView) adapterView.getChildAt(0)).setTextSize(14);

        if (sp_WeightUnit.getSelectedItemId() == 0) {
          try {
            weight = Double.valueOf(et_Weight.getText().toString());
            weight = weight * 2.2;
            et_Weight.setText(String.format(Locale.TAIWAN, "%.1f", weight));

          } catch (NumberFormatException e) {
            Log.e(TAG, "Number Format is Wrong", e);
            weight = 0;
          }

        } else if (sp_WeightUnit.getSelectedItemId() == 1) {
          try {
            weight = Double.valueOf(et_Weight.getText().toString());
            weight = weight / 2.2;
            et_Weight.setText(String.format(Locale.TAIWAN, "%.1f", weight));

          } catch (NumberFormatException e) {
            Log.e(TAG, "Number Format is Wrong", e);
            weight = 0;
          }
        }
      }

      @Override
      public void onNothingSelected(AdapterView<?> adapterView) {

      }
    });



    ArrayAdapter<String> waistLineUnitList = new ArrayAdapter<>(this,
            android.R.layout.simple_spinner_dropdown_item,
            waistLineUnit);
    sp_WaistLineUnit.setAdapter(waistLineUnitList);

    sp_WaistLineUnit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        ((TextView) adapterView.getChildAt(0)).setTextSize(14);

        if (sp_WaistLineUnit.getSelectedItemId() == 0) {
          try {
            waistLine = Double.valueOf(et_WaistLine.getText().toString());
            waistLine = waistLine * 2.54;
            et_WaistLine.setText(String.format(Locale.TAIWAN, "%.1f", waistLine));

          } catch (NumberFormatException e) {
            Log.e(TAG, "Number Format is Wrong", e);
            waistLine = 0;
          }

        } else if (sp_WaistLineUnit.getSelectedItemId() == 1) {
          try {
            waistLine = Double.valueOf(et_WaistLine.getText().toString());
            waistLine = waistLine / 2.54;
            et_WaistLine.setText(String.format(Locale.TAIWAN, "%.1f", waistLine));

          } catch (NumberFormatException e) {
            Log.e(TAG, "Number Format is Wrong", e);
            waistLine = 0;
          }
        }
      }

      @Override
      public void onNothingSelected(AdapterView<?> adapterView) {

      }
    });
  }
  private void setupEditText() {
    et_Weight.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void afterTextChanged(Editable editable) {
        try {
          if (sp_WeightUnit.getSelectedItemPosition() == 0) {
            weight = Double.valueOf(et_Weight.getText().toString());
          } else {
            weight = Double.valueOf(et_Weight.getText().toString());
          }
        } catch (NumberFormatException e) {
          Log.e(TAG, "Number Format is Wrong", e);
          weight = 0;
        }

      }
    });
    et_WaistLine.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void afterTextChanged(Editable editable) {
        try {
          if (sp_WaistLineUnit.getSelectedItemPosition() == 0) {
            waistLine = Double.valueOf(et_WaistLine.getText().toString());
          } else {
            waistLine = Double.valueOf(et_WaistLine.getText().toString());
          }
        } catch (NumberFormatException e) {
          Log.e(TAG, "Number Format is Wrong", e);
          waistLine = 0;
        }
      }
    });
  }
  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        handleBackAction();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }
  private void setHandlerFunction() {
    final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
    handler = new Handler() {
      @Override
      public void handleMessage(Message msg) {
        switch (msg.what) {
          case 0:
            builder.setMessage("資料已成功儲存")
                    .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                      @Override
                      public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        handleBackAction();
                      }
                    }).show();
            break;
          case 1:
            builder.setMessage("請稍後再試")
                    .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                      @Override
                      public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                      }
                    })
                    .show();
            break;
        }
      }
    };
  }

  public void sendSuccessMessage() {
    Message message = new Message();
    message.what = 0;
    handler.sendMessage(message);
  }

  public void sendFailMessage() {
    Message message = new Message();
    message.what = 1;
    handler.sendMessage(message);
  }
  @Override
  public void onBackPressed() {
    handleBackAction();
  }

  private void handleBackAction(){
    Intent intent = new Intent();
    intent.setClass(this, MainActivity.class);
    intent.putExtra(systemConstant.REDIRECT_PAGE ,systemConstant.DISPLAY_GOAL);
    startActivity(intent);
    overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
    finish();
  }
}
