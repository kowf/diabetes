package com.dmc.myapplication.goal;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.R;
import com.dmc.myapplication.localDB.RealmFunction;
import com.dmc.myapplication.login.User;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.navFood.navFoodConstant;
import com.dmc.myapplication.network.RetrofitFunction;
import com.dmc.myapplication.network.webService;
import com.dmc.myapplication.otherBodyIndex.otherBodyMeasure_add;
import com.dmc.myapplication.pojo.BodyIndexRecord;
import com.dmc.myapplication.systemConstant;
import com.dmc.myapplication.util.DateUtil;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by lamivan on 4/1/2018.
 */

public class goalProgressActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener {
  String mParam1, date, length;
  LineChart lineChart_BMI, lineChart_waist, lineChart_BBMI;
  RealmFunction realmFunction;
  RelativeLayout relativeLayout, relativeLayout2, relativeLayout3;
  List<BodyIndexRecord> list_Weight;
  private static Handler handler;
  private SeekBar mSeekBarX, mSeekBarY;
  TextView textView, textView2, textView3;
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.goaldetail);
    setHandlerFunction();


    mParam1 = getIntent().getBundleExtra("parameters").getString("weight");
    System.out.println("weight expected val == " + mParam1);
    date = getIntent().getBundleExtra("parameters").getString("date");
    System.out.println("weight expected val == " + date);
    length = getIntent().getBundleExtra("parameters").getString("length");

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
    Date dates = null;
    try {
      dates = dateFormat.parse(date);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    UserLocalStore userLocalStores = new UserLocalStore(goalProgressActivity.this);
    String userId = Integer.toString(userLocalStores.getLoggedInUser().userid);

    RetrofitFunction retrofitFunction = new RetrofitFunction(goalProgressActivity.this, webService.serverURL);
    retrofitFunction.getBodyIndexRecords2(Integer.valueOf(userId), 1, date, Integer.valueOf(length) * 7);

//    System.out.println("realm hash list == " + realmFunction.getAllBodyIndexRecordsByDate());
    lineChart_BMI = (LineChart) findViewById(R.id.lineChart_weight);
    relativeLayout = (RelativeLayout) findViewById(R.id.relative);
    relativeLayout.setVisibility(View.VISIBLE);
    lineChart_BMI.setVisibility(View.VISIBLE);
    textView = (TextView) findViewById(R.id.abc);
    textView.setVisibility(View.GONE);

    // no description text
    lineChart_BMI.setDescription("");

    // enable touch gestures
    lineChart_BMI.setTouchEnabled(true);

    lineChart_waist = (LineChart) findViewById(R.id.lineChart_waist);
    relativeLayout2 = (RelativeLayout) findViewById(R.id.relative2);
    relativeLayout2.setVisibility(View.VISIBLE);
    lineChart_waist.setVisibility(View.VISIBLE);
    textView2 = (TextView) findViewById(R.id.abc2);
    textView2.setVisibility(View.GONE);

    // no description text
    lineChart_waist.setDescription("");

    // enable touch gestures
    lineChart_waist.setTouchEnabled(true);

    lineChart_BBMI = (LineChart) findViewById(R.id.lineChart_BMI);
    relativeLayout3 = (RelativeLayout) findViewById(R.id.relative3);

    relativeLayout3.setVisibility(View.VISIBLE);
    lineChart_BBMI.setVisibility(View.VISIBLE);
    textView3 = (TextView) findViewById(R.id.abc3);
    textView3.setVisibility(View.GONE);

    // no description text
    lineChart_BBMI.setDescription("");

    // enable touch gestures
    lineChart_BBMI.setTouchEnabled(true);
    //List<BodyIndexRecord> list_Weight = realmFunction.getAllBodyIndexRecordsByDate();
//    System.out.println(list_Weight.size());
   // System.out.println(dateFormat.format(list_Weight.get(0).getDATE()));
    mSeekBarX = (SeekBar) findViewById(R.id.seek1);
    mSeekBarY = (SeekBar) findViewById(R.id.seek2);

    mSeekBarX.setVisibility(View.VISIBLE);
    mSeekBarY.setVisibility(View.VISIBLE);
    mSeekBarX.setProgress(0);
    mSeekBarY.setProgress(0);
    mSeekBarY.setOnSeekBarChangeListener(this);
    mSeekBarX.setOnSeekBarChangeListener(this);
    ActionBar actionBar = getSupportActionBar();
    actionBar.setDisplayHomeAsUpEnabled(true);
    actionBar.setHomeButtonEnabled(true);
  }
  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        handleBackAction();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }
  private void setHandlerFunction() {
    handler = new Handler() {
      @Override
      public void handleMessage(Message msg) {

        realmFunction = new RealmFunction(goalProgressActivity.this);

        List<BodyIndexRecord> list_Weight = realmFunction.getAllBodyIndexRecordsByDate();
        System.out.println(list_Weight.size());

        if (list_Weight.size() >= 2) {
          // TODO: set up weight line chart
          System.out.println("realm hash list == " + realmFunction.getAllBodyIndexRecordsByDate());
          List<Entry> vals = new ArrayList<>();
          int index_BMI = 0;
          for (int i = 0; i < list_Weight.size(); i++) {
            vals.add(i, new Entry((float) list_Weight.get(i).getWEIGHT(), index_BMI++));
          }
          LineDataSet setComp1 = new LineDataSet(vals, "進度");
          setComp1.setColor(Color.RED);
          setComp1.setLineWidth(3f);
          setComp1.setValueTextSize(14f);

          DateUtil dateUtil = new DateUtil();

          List<String> strings = new ArrayList<>();
          for (int i = 0; i < list_Weight.size(); i++) {
            strings.add(dateUtil.dateToString_English(list_Weight.get(i).getDATE()));
          }
          setComp1.setAxisDependency(YAxis.AxisDependency.LEFT);

          List<LineDataSet> dataSets = new ArrayList<LineDataSet>();
          dataSets.add(setComp1);

          System.out.println(lineChart_BMI.getYChartMax());
          YAxis yAxis = lineChart_BMI.getAxisLeft();
          yAxis.setAxisMaxValue(100f);
          yAxis.setStartAtZero(false);
          yAxis.setAxisMinValue(40f);
          LimitLine ll1 = new LimitLine((float) list_Weight.get(0).getWEIGHT() - 3, "目標");
          ll1.setLineWidth(2.5f);
          ll1.enableDashedLine(10f, 10f, 0f);
          ll1.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_TOP);
          ll1.setTextSize(14f);
          ll1.setLineColor(Color.BLACK);
          YAxis yAxis2 = lineChart_BMI.getAxisRight();
          yAxis2.setAxisMaxValue(100f);
          yAxis2.setStartAtZero(false);
          yAxis2.setAxisMinValue(40f);

          LimitLine ll2 = new LimitLine((float) (22.9 * Math.pow(list_Weight.get(0).getHEIGHT(), 2) / 10000.0), "最大合理體重值");
          ll2.setLineWidth(2.5f);
          ll2.enableDashedLine(10f, 10f, 0f);
          ll2.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_TOP);
          ll2.setTextSize(14f);
          ll2.setLineColor(Color.BLUE);
          LimitLine ll3 = new LimitLine((float) (18.5 * Math.pow(list_Weight.get(0).getHEIGHT(), 2) / 10000.0), "最小合理體重值");
          ll3.setLineWidth(2.5f);
          ll3.enableDashedLine(10f, 10f, 0f);
          ll3.setLabelPosition(LimitLine.LimitLabelPosition.LEFT_BOTTOM);
          ll3.setTextSize(14f);
          ll3.setLineColor(Color.rgb(128, 0, 128));
          yAxis.addLimitLine(ll1);
          yAxis.addLimitLine(ll2);
          yAxis.addLimitLine(ll3);
          System.out.println(lineChart_BMI.getYChartMax());
          LineData data = new LineData(strings, dataSets);
          lineChart_BMI.setData(data);
          //lineChart_BMI.zoom(1.1f, 1.1f, lineChart_BMI.getCenter().x, lineChart_BMI.getCenter().y);
          lineChart_BMI.invalidate();
          // TODO: set up waist line chart
          List<Entry> vals2 = new ArrayList<>();
          int index_waist = 0;
          for (int i = 0; i < list_Weight.size(); i++) {
            vals2.add(i, new Entry((float) list_Weight.get(i).getWAIST(), index_waist++));
          }
          LineDataSet setComp2 = new LineDataSet(vals2, "進度");
          setComp2.setColor(Color.RED);
          setComp2.setLineWidth(3f);
          setComp2.setValueTextSize(14f);
          YAxis yAxis3 = lineChart_waist.getAxisLeft();
          yAxis3.setAxisMaxValue(120f);
          yAxis3.setStartAtZero(false);
          yAxis3.setAxisMinValue(50f);
          YAxis yAxis4 = lineChart_waist.getAxisRight();
          yAxis4.setAxisMaxValue(120f);
          yAxis4.setStartAtZero(false);
          yAxis4.setAxisMinValue(50f);
          UserLocalStore userLocalStore = new UserLocalStore(goalProgressActivity.this);
          User user = userLocalStore.getLoggedInUser();

          String gender = "";
          if (user.sex == 0){
            gender = "F";
          } else if (user.sex == 1) {
            gender = "M";
          }
          LimitLine ll4 = null;
          if (gender.equals("F")) {
            ll4 = new LimitLine((float) 80.0, "腰圍上限");
          } else if (gender.equals("M")) {
            ll4 = new LimitLine((float) 90.0, "腰圍上限");
          }
          ll4.setLineWidth(2.5f);
          ll4.enableDashedLine(10f, 10f, 0f);
          ll4.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_TOP);
          ll4.setTextSize(14f);
          ll4.setLineColor(Color.BLUE);
          yAxis3.addLimitLine(ll4);
          List<LineDataSet> dataSets2 = new ArrayList<LineDataSet>();
          dataSets2.add(setComp2);
          LineData data2 = new LineData(strings, dataSets2);
          lineChart_waist.setData(data2);
          //lineChart_BMI.zoom(1.1f, 1.1f, lineChart_BMI.getCenter().x, lineChart_BMI.getCenter().y);
          lineChart_waist.invalidate();

          // TODO: set up BMI line chart
          List<Entry> vals3 = new ArrayList<>();
          int index_BBMI = 0;
          for (int i = 0; i < list_Weight.size(); i++) {
            vals3.add(i, new Entry((float) list_Weight.get(i).getBMI(), index_BBMI++));
          }
          LineDataSet setComp3 = new LineDataSet(vals3, "進度");
          setComp3.setColor(Color.RED);
          setComp3.setLineWidth(3f);
          setComp3.setValueTextSize(14f);
          YAxis yAxis5 = lineChart_BBMI.getAxisLeft();
          yAxis5.setAxisMaxValue(25f);
          yAxis5.setStartAtZero(false);
          yAxis5.setAxisMinValue(15f);
          YAxis yAxis6 = lineChart_BBMI.getAxisRight();
          yAxis6.setAxisMaxValue(25f);
          yAxis6.setStartAtZero(false);
          yAxis6.setAxisMinValue(15f);

          LimitLine ll5 = new LimitLine((float) 22.9, "BMI上限");
          ll5.setLineWidth(2.5f);
          ll5.enableDashedLine(10f, 10f, 0f);
          ll5.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_TOP);
          ll5.setTextSize(14f);
          ll5.setLineColor(Color.BLUE);
          LimitLine ll6 = new LimitLine((float) 18.5, "BMI下限");
          ll6.setLineWidth(2.5f);
          ll6.enableDashedLine(10f, 10f, 0f);
          ll6.setLabelPosition(LimitLine.LimitLabelPosition.LEFT_BOTTOM);
          ll6.setTextSize(14f);
          ll6.setLineColor(Color.rgb(128, 0, 128));
          yAxis5.addLimitLine(ll5);
          yAxis5.addLimitLine(ll6);

          List<LineDataSet> dataSets3 = new ArrayList<LineDataSet>();
          dataSets3.add(setComp3);
          LineData data3 = new LineData(strings, dataSets3);
          lineChart_BBMI.setData(data3);
          //lineChart_BMI.zoom(1.1f, 1.1f, lineChart_BMI.getCenter().x, lineChart_BMI.getCenter().y);
          lineChart_BBMI.invalidate();
        } else {
          lineChart_BMI.setVisibility(View.GONE);
          lineChart_BBMI.setVisibility(View.GONE);
          lineChart_waist.setVisibility(View.GONE);
          relativeLayout.setVisibility(View.GONE);
          relativeLayout2.setVisibility(View.GONE);
          relativeLayout3.setVisibility(View.GONE);
          mSeekBarX.setVisibility(View.GONE);
          mSeekBarY.setVisibility(View.GONE);
          textView.setVisibility(View.VISIBLE);
          textView2.setVisibility(View.VISIBLE);
          textView3.setVisibility(View.VISIBLE);
        }
      }
    };
  }

  public static void sendSuccessMessage() {
    Message message = new Message();
    handler.sendMessage(message);
  }

  @Override
  public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

    float mXProgress = 0;
    switch (mSeekBarX.getProgress()){
      case 1:
        mXProgress = 1.1f;
        break;
      case 2:
        mXProgress = 1.2f;
        break;
      case 3:
        mXProgress = 1.3f;
        break;
      case 4:
        mXProgress = 1.4f;
        break;
      case 5:
        mXProgress = 1.5f;
        break;
    }
    float mYProgress = 0;
    switch (mSeekBarX.getProgress()){
      case 1:
        mYProgress = 1.1f;
        break;
      case 2:
        mYProgress = 1.2f;
        break;
      case 3:
        mYProgress = 1.3f;
        break;
      case 4:
        mYProgress = 1.4f;
        break;
      case 5:
        mYProgress = 1.5f;
        break;
    }
    lineChart_BMI.zoom(mXProgress, mYProgress, lineChart_BMI.getCenter().x, lineChart_BMI.getCenter().y);

    // redraw
    lineChart_BMI.invalidate();
  }

  @Override
  public void onStartTrackingTouch(SeekBar seekBar) {

  }

  @Override
  public void onStopTrackingTouch(SeekBar seekBar) {

  }

  @Override
  public void onBackPressed() {
    handleBackAction();
  }

  private void handleBackAction(){
    Intent intent = new Intent();
    intent.setClass(this, MainActivity.class);
    intent.putExtra(systemConstant.REDIRECT_PAGE ,systemConstant.DISPLAY_GOAL);
    startActivity(intent);
    overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
    finish();
  }
}
