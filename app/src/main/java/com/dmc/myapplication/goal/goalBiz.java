package com.dmc.myapplication.goal;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.dmc.myapplication.R;
import com.dmc.myapplication.assessment.assessmentConstant;
import com.dmc.myapplication.assessment.assessmentFragment;
import com.dmc.myapplication.navFood.navFoodConstant;

/**
 * Created by lamivan on 1/1/2018.
 */

public class goalBiz {
  public static void startGoal(Activity activity,String foodDateUI, FragmentTransaction ft){
    activity.setTitle(R.string.nav_forum);
    Bundle bundle = new Bundle();
    bundle.putString(navFoodConstant.GET_FOOD_DATE, foodDateUI);
    goalFragment assFrag = new goalFragment();
    assFrag.setArguments(bundle);
    ft.setCustomAnimations(R.anim.slide1, R.anim.slide2).replace(R.id.content_frame, assFrag).addToBackStack(null).commit();
  }
}
