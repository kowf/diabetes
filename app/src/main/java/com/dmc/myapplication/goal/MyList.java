package com.dmc.myapplication.goal;

/**
 * Created by lamivan on 1/1/2018.
 */

public class MyList {
    private String head;
    private String desc;
    private String witness;

    //constructor initializing values
    public MyList(String head, String desc, String witness) {
        this.head = head;
        this.desc = desc;
        this.witness = witness;
    }

    //getters
    public String getHead() {
        return head;
    }

    public String getDesc() {
        return desc;
    }

    public String getWitness() {
      return witness;
    }

}
