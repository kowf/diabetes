package com.dmc.myapplication.bodyRecord;

import android.content.Context;

/**
 * Created by Po on 10/2/2016.
 */
public class bodyRecordConstant {

    public static Context getApplicationContext = null;

    public final static String GET_BODY_RECORD ="bodyRecord/getBodyRecord.php";
    public final static String GET_INJECTION_RECORD="bodyRecord/getInjectionRecord.php";
    public final static String SAVE_BODY_RECORD="bodyRecord/saveBodyRecord.php";
    public final static String EDIT_BODY_RECORD="bodyRecord/editBodyRecord.php";
    public final static String DELETE_BODY_RECORD="bodyRecord/deleteBodyRecord.php";

    public final static int VIEW_MODE_GRAPH = 0;
    public final static int VIEW_MODE_LIST = 1;

    //record type
    public final static String GET_GLUCOSE_TYPE_CODE = "1";
    public final static String GET_BMI_TYPE_CODE = "2";
    public final static String GET_HBA1C_TYPE_CODE = "3";
    public final static String GET_BP_TYPE_CODE = "4";
    public final static String GET_TRIGLYCERIDERS_TYPE_CODE = "5";
    public final static String GET_REMARK_TYPE_CODE ="6";
    public final static String GET_REPORT_MIX_TYPE_CODE ="7";


    //Period Type
    public final static int GET_GLUCOSE_BREAKFAST_CODE = 1;
    public final static int GET_GLUCOSE_LUNCH_CODE =2;
    public final static int GET_GLUCOSE_DINNER_CODE =3;



    //Show Type - for selecting all or before or after meal record
    public final static String GET_ALL = "0";
    public final static String GET_GLUCOSE_BEFORE_MEAL = "2";
    public final static String GET_GLUCOSE_AFTER_MEAL= "1";



    //GLUCOSE LEVEL LIMIT
    //public final static double GET_GLUCOSE_BEFORE_MEAL_LOW = 4;
    //public final static double GET_GLUCOSE_BEFORE_MEAL_MIDHIGH = 6;
    //public final static double GET_GLUCOSE_BEFORE_MEAL_HIGH = 8;

    //public final static double GET_GLUCOSE_AFTER_MEAL_LOW = 4;
    //public final static double GET_GLUCOSE_AFTER_MEAL_MIDHIGH = 8;
    //public final static double GET_GLUCOSE_AFTER_MEAL_HIGH = 10;

    public final static double GET_GLUCOSE_UPPER_LIMIT = 25;
    public final static double GET_GLUCOSE_LOWER_LIMIT = 0;



    //BP & HEART RATE LEVEL LIMIT
    public final static int GET_BP_LOWER_LIMIT = 30;
    public final static int GET_BP_SYS_UPPER_LIMIT = 260;
    public final static int GET_BP_DIA_UPPER_LIMIT = 130;
    public final static int GET_BP_SYS_HIGH = 140;
    public final static int GET_BP_DIA_HIGH = 90;
    public final static int GET_BP_SYS_MIDHIGH = 130;
    public final static int GET_BP_DIA_MIDHIGH = 80;
    public final static int GET_BP_SYS_LOW = 100;
    public final static int GET_BP_DIA_LOW = 50;

    public final static int GET_HR_LOWER_LIMIT = 45;
    public final static int GET_HR_UPPER_LIMIT = 200;


    //HbA1c
    public final static double GET_HBA1C_LOWER_LIMIT = 4.9;
    public final static double GET_HBA1C_UPPER_LIMIT = 12;
    public final static double GET_HBA1C_MIDHIGH = 7;
    public final static double GET_HBA1C_HIGH = 8;

    //BMI
    public final static double GET_BMI_LOWER_LIMIT = 15;
    public final static double GET_BMI_UPPER_LIMIT = 50;
    public final static double GET_BMI_LOW = 18.5;
    public final static double GET_BMI_OVERWEIGHT = 23;
    public final static double GET_BMI_MIDFAT = 25;
    public final static double GET_BMI_HIGHFAT = 30;

    //WAIST
    public final static double GET_WAIST_LOWER_LIMIT = 40;
    public final static double GET_WAIST_UPPER_LIMIT = 250;
    public final static double GET_WAIST_MALE_FAT = 90;
    public final static double GET_WAIST_FEMALE_FAT = 80;


    //TOTAL C, LDL-C, HDL-C, TRIGLYCERIDERS
    public final static double GET_TOTALC_LOWER_LIMIT = 0;
    public final static double GET_TOTALC_UPPER_LIMIT = 15;
    public final static double GET_TOTALC_MIDHIGH = 5.2;
    public final static double GET_TOTALC_HIGH = 6.2;

    public final static double GET_LDLC_MIDHIGH = 2.6;
    public final static double GET_LDLC_HIGH = 3.5;

    public final static double GET_HDLC_MALE = 1.0;   //IF >1.0 = NICE
    public final static double GET_HDLC_FEMALE = 1.3; //IF >1.3 = NICE

    public final static double GET_TRIGLYCERIDERS_MIDHIGH = 1.8;
    public final static double GET_TRIGLYCERIDERS_HIGH = 4.5;



    //Janus
    //Chart Date Range
    public final static int CHART_ALLTIME = -1;
    public final static int CHART_DAILY = 0;
    public final static int CHART_WEEKLY = 1;
    public final static int CHART_MONTHLY = 2;
    public final static int CHART_YEARLY = 3;






}
