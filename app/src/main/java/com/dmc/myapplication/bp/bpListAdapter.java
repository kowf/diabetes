package com.dmc.myapplication.bp;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dmc.myapplication.R;
import com.dmc.myapplication.bodyRecord.BodyRecord;
import com.dmc.myapplication.bodyRecord.bodyRecordConstant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Po on 7/3/2016.
 */
public class bpListAdapter  extends BaseAdapter {
    private final Context context;
    private List<BodyRecord.bodyRecord> mListData =   new ArrayList<BodyRecord.bodyRecord>();

    private LayoutInflater layoutInflater;


    public bpListAdapter(Context aContext,  List<BodyRecord.bodyRecord> listData) {
        context = aContext;
        mListData = listData;
        layoutInflater = LayoutInflater.from(aContext);
    }

    @Override
    public int getCount() {
        return mListData.size();
    }

    @Override
    public Object getItem(int position) {
        return mListData.get(position);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        BodyRecord.bodyRecord record = (BodyRecord.bodyRecord) getItem(position);

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.bp_list_row_layout, null);
            holder = new ViewHolder();
            holder.bpHView = (TextView) convertView.findViewById(R.id.bp_h);
            holder.bpLView = (TextView) convertView.findViewById(R.id.bp_l);
            holder.dateView = (TextView) convertView.findViewById(R.id.date);
            holder.timeView = (TextView) convertView.findViewById(R.id.time);
            holder.heartRateView = (TextView) convertView.findViewById(R.id.heart_rate);
            holder.recordIdView = (TextView) convertView.findViewById(R.id.bpRecordId);
            holder.recordPeriodType = (TextView) convertView.findViewById(R.id.bpPeriodType);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (record.getRecordType()==4) {

                holder.bpHView.setText(String.valueOf(record.getBp_h()));
                holder.bpLView.setText(String.valueOf(record.getBp_l()));
            //set level color
                // low
                if (record.getBp_h() < bodyRecordConstant.GET_BP_SYS_LOW || record.getBp_l() < bodyRecordConstant.GET_BP_DIA_LOW ){
                    holder.bpHView.setTextColor(Color.parseColor("#673ab7"));
                    holder.bpLView.setTextColor(Color.parseColor("#673ab7"));
                }//medium
                else if (( record.getBp_h() >= bodyRecordConstant.GET_BP_SYS_LOW && record.getBp_h() < bodyRecordConstant.GET_BP_SYS_MIDHIGH)
                        && ( record.getBp_l() >= bodyRecordConstant.GET_BP_DIA_LOW && record.getBp_l() < bodyRecordConstant.GET_BP_DIA_MIDHIGH)){
                    holder.bpHView.setTextColor(Color.parseColor("#4caf50"));
                    holder.bpLView.setTextColor(Color.parseColor("#4caf50"));
                }
                //high
                else if (( record.getBp_h() > bodyRecordConstant.GET_BP_SYS_HIGH)
                        || ( record.getBp_l() > bodyRecordConstant.GET_BP_DIA_HIGH)){
                    holder.bpHView.setTextColor(Color.parseColor("#FF5722"));
                    holder.bpLView.setTextColor(Color.parseColor("#FF5722"));
                }//midhigh
                else if (( record.getBp_h() >= bodyRecordConstant.GET_BP_SYS_MIDHIGH && record.getBp_h() <= bodyRecordConstant.GET_BP_SYS_HIGH)
                        || ( record.getBp_l() >= bodyRecordConstant.GET_BP_DIA_MIDHIGH && record.getBp_l() <= bodyRecordConstant.GET_BP_DIA_HIGH)){
                    holder.bpHView.setTextColor(Color.parseColor("#f7c600"));
                    holder.bpLView.setTextColor(Color.parseColor("#f7c600"));
                }
                else {//high
                    holder.bpHView.setTextColor(Color.parseColor("#FF5722"));
                    holder.bpLView.setTextColor(Color.parseColor("#FF5722"));
                }

                holder.heartRateView.setText(String.valueOf(record.getHeart_rate()));

                holder.recordIdView.setText(Integer.toString(record.getRecordId()));
                holder.dateView.setText(record.getDate());
                holder.timeView.setText(record.getTime());
                holder.recordPeriodType.setText(Integer.toString(record.getType_glucose()));

        }
        return convertView;
    }

    static class ViewHolder {
        TextView bpHView;
        TextView bpLView;
        TextView heartRateView;
        TextView dateView;
        TextView timeView;
        TextView recordIdView;
        TextView recordPeriodType;
    }

}
