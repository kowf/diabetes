package com.dmc.myapplication.bp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.Models.bodyRecord;
import com.dmc.myapplication.R;
import com.dmc.myapplication.bodyRecord.BodyRecord;
import com.dmc.myapplication.systemConstant;

/**
 * Created by Po on 9/3/2016.
 */
public class bp_delete extends AppCompatActivity {

    private Context ctx;
    private TextView editTextBpHValue, editTextBpLValue, editTextHrValue;
    private String recordId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(R.anim.slide1, R.anim.slide2);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bp_del);
        ctx = getApplicationContext();


        Intent intent = getIntent();
        recordId = intent.getStringExtra("recordId");
        final String getDate = intent.getStringExtra("date");
        final String getTime = intent.getStringExtra("time");
        final int getPeriodType = Integer.parseInt(intent.getStringExtra("typePeriod"));
        final String getBpHValue = intent.getStringExtra("bpHValue");
        final String getBpLValue = intent.getStringExtra("bpLValue");
        final String getHeartRateValue = intent.getStringExtra("heartRateValue");

        BodyRecord record = new BodyRecord();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // SET return <- into left hand side
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //handle the date
        TextView editTextDate = (TextView) findViewById(R.id.bp_del_date);
        editTextDate.setEnabled(false);
        editTextDate.setText(getDate);


        //handle the time
        TextView editTextTime = (TextView) findViewById(R.id.bp_del_time);
        editTextTime.setEnabled(false);
        editTextTime.setText(getTime);


        //handle the blood value field
        editTextBpHValue = (TextView) findViewById(R.id.bp_del_bp_up_value);
        editTextBpHValue.setText(getBpHValue);
        editTextBpHValue.setEnabled(false);

        editTextBpLValue = (TextView) findViewById(R.id.bp_del_bp_low_value);
        editTextBpLValue.setText(getBpLValue);
        editTextBpLValue.setEnabled(false);

        editTextHrValue = (TextView) findViewById(R.id.bp_del_hr_value);
        editTextHrValue.setText(getHeartRateValue);
        editTextHrValue.setEnabled(false);

        Button buttonBpDel = (Button) findViewById(R.id.bp_del_button);
        buttonBpDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(bp_delete.this);
                builder.setMessage("確定刪除這條記錄嗎？")
                        .setTitle("刪除記錄")
                        .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                bodyRecord bodyRecord = new bodyRecord(getApplicationContext());
                                bodyRecord.delete(Integer.parseInt(recordId));
                                Intent intent = new Intent();
                                intent.setClass(bp_delete.this, MainActivity.class);
                                intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_OTHER_INDEX);
                                bp_delete.this.startActivity(intent);
                                bp_delete.this.finish();
                            }
                        })
                        .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .show();
            }
        });

        Button buttonBpEdit = (Button) findViewById(R.id.bp_edit_button);
        buttonBpEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBpEditRecord(recordId, getDate, getTime, Integer.toString(getPeriodType), getBpHValue, getBpLValue, getHeartRateValue);
            }
        });

    }

    public void getBpEditRecord(String recordId, String date, String time, String typePeriod, String bpHValue, String bpLValue, String heartRateValue) {

        Intent intent = new Intent();
        intent.setClass(this, bp_edit.class);
        intent.putExtra("recordId", recordId);
        intent.putExtra("date", date);
        intent.putExtra("time", time);
        intent.putExtra("bpHValue", bpHValue);
        intent.putExtra("bpLValue", bpLValue);
        intent.putExtra("heartRateValue", heartRateValue);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        handleBackAction();
    }

    private void handleBackAction() {
        Intent intent = new Intent();
        intent.setClass(this, MainActivity.class);
        intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_OTHER_INDEX);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        finish();
    }

}
