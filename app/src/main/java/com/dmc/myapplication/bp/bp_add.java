package com.dmc.myapplication.bp;

import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.Models.bodyRecord;
import com.dmc.myapplication.R;
import com.dmc.myapplication.bodyRecord.BodyRecord;
import com.dmc.myapplication.bodyRecord.bodyRecordConstant;
import com.dmc.myapplication.login.User;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.systemConstant;

import java.util.Calendar;

/**
 * Created by Po on 6/3/2016.
 */
public class bp_add extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private Toolbar toolbar;
    Context ctx;
    private EditText editTextDate, editTextTime, editTextBpUpValue, editTextBpLowValue, editTextHrValue;
    private Spinner spinnerPeriod;
    private int selectedPeriod = 1;
    private Button buttonBpAdd;
    UserLocalStore userLocalStore;
    User user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(R.anim.slide1, R.anim.slide2);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bp_add);
        ctx = getApplicationContext();
        userLocalStore = new UserLocalStore(this);
        user = userLocalStore.getLoggedInUser();

        //spinnerPeriod = (Spinner) findViewById(R.id.);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // SET return <- into left hand side
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //handle the date picker
        editTextDate = (EditText) findViewById(R.id.bp_add_date);
        Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH) + 1; // because Jan =0
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        editTextDate.setText(yy + "-" + mm + "-" + dd);

        ImageView setBpDate = (ImageView) findViewById(R.id.setBp_add_date);
        setBpDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new bpDatePickerFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        //handle the time picker
        editTextTime = (EditText) findViewById(R.id.bp_add_time);
        Calendar time = Calendar.getInstance();
        int HH = time.get(Calendar.HOUR_OF_DAY);
        int MM = time.get(Calendar.MINUTE);

        editTextTime.setText(HH + ":" + MM + ":00");
        ImageView setTime = (ImageView) findViewById(R.id.setBp_add_time);
        setTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment3 = new bpTimePickerFragment();
                newFragment3.show(getFragmentManager(), "Time Dialog");
            }
        });


        //handle the bp value field
        editTextBpUpValue = (EditText) findViewById(R.id.bp_add_bp_up_value);
        editTextBpLowValue = (EditText) findViewById(R.id.bp_add_bp_low_value);
        editTextHrValue = (EditText) findViewById(R.id.bp_add_hr_value);

        buttonBpAdd = (Button) findViewById(R.id.bp_add_button);
        buttonBpAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validBpHr();
            }
        });

    }

    private void validBpHr() {

        // Reset errors.
        editTextBpUpValue.setError(null);
        editTextBpLowValue.setError(null);
        editTextHrValue.setError(null);

        // Store values of input.
        String bpUpper = editTextBpUpValue.getText().toString();
        String bpLower = editTextBpLowValue.getText().toString();
        String heartRate = editTextHrValue.getText().toString();

        boolean empty1 = false;
        boolean empty2 = false;
        boolean cancel1 = false;
        boolean cancel2 = false;
        View focusView = null;


        // Check for a valid input, if the user entered one.

        if (TextUtils.isEmpty(bpUpper) || TextUtils.isEmpty(bpLower)) {
            if (TextUtils.isEmpty(bpUpper)) {
                editTextBpUpValue.setError("資料不能為空");
                focusView = editTextBpUpValue;
            }
            if (TextUtils.isEmpty(bpLower)) {
                editTextBpLowValue.setError("資料不能為空");
                focusView = editTextBpLowValue;
            }
            empty1 = true;
        }
        if (TextUtils.isEmpty(heartRate)) {
            editTextHrValue.setError("資料不能為空");
            focusView = editTextHrValue;
            empty2 = true;
        }

        if (!empty1) {
            if (TextUtils.isEmpty(bpUpper) || !isValidBp(bpUpper)) {
                editTextBpUpValue.setError(getString(R.string.bp_bp_error));
                focusView = editTextBpUpValue;
                cancel1 = true;
            }

            if (TextUtils.isEmpty(bpLower) || !isValidBp(bpLower)) {
                editTextBpLowValue.setError(getString(R.string.bp_bp_error));
                focusView = editTextBpLowValue;
                cancel1 = true;
            }

            if (!TextUtils.isEmpty(bpUpper) && !TextUtils.isEmpty(bpLower)) {
                if (Integer.parseInt(bpLower) >= Integer.parseInt(bpUpper)) {
                    editTextBpLowValue.setError(getString(R.string.bp_bp_error2));
                    focusView = editTextBpLowValue;
                    cancel1 = true;
                }

                if ((Integer.parseInt(bpUpper) > bodyRecordConstant.GET_BP_SYS_UPPER_LIMIT) || (Integer.parseInt(bpUpper) < bodyRecordConstant.GET_BP_LOWER_LIMIT)) {
                    editTextBpUpValue.setError(getString(R.string.bp_bp_warning));
                    focusView = editTextBpUpValue;
                    cancel1 = true;
                }

                if ((Integer.parseInt(bpLower) > bodyRecordConstant.GET_BP_DIA_UPPER_LIMIT) || (Integer.parseInt(bpLower) < bodyRecordConstant.GET_BP_LOWER_LIMIT)) {
                    editTextBpLowValue.setError(getString(R.string.bp_bp_warning));
                    focusView = editTextBpLowValue;
                    cancel1 = true;
                }
            }
        }

        if (!empty2) {
            if (TextUtils.isEmpty(heartRate) || !isValidHr(heartRate)) {
                editTextHrValue.setError(getString(R.string.bp_hr_error));
                focusView = editTextHrValue;
                cancel2 = true;
            }
        }

        if (empty1 && empty2) {
            focusView.requestFocus();

        } else if (cancel1 || cancel2) {
            focusView.requestFocus();

        } else {
            editTextBpUpValue.setError(null);
            editTextBpLowValue.setError(null);
            editTextHrValue.setError(null);

            bodyRecord bodyRecord = new bodyRecord(getApplicationContext());
            BodyRecord a = new BodyRecord();

            int bpLowerValue = 0, bpUpperValue = 0, heartRateValue = 0;
            if (!bpLower.isEmpty()) {
                bpLowerValue = Integer.parseInt(bpLower);
            }
            if (!bpUpper.isEmpty()) {
                bpUpperValue = Integer.parseInt(bpUpper);
            }
            if (!heartRate.isEmpty()) {
                heartRateValue = Integer.parseInt(heartRate);
            }

            BodyRecord.bodyRecord bodyrecord = bodyRecord.insert(a.new bodyRecord(0, Integer.parseInt(bodyRecordConstant.GET_BP_TYPE_CODE), (user.userid), editTextDate.getText().toString(), editTextTime.getText().toString(), 0, 0, 0, 0, bpUpperValue, bpLowerValue, heartRateValue, 0, 0, 0, 0, 0, 0, 0, 0, ""));
            Intent intent = new Intent();
            intent.setClass(this, MainActivity.class);
            intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_OTHER_INDEX);
            this.startActivity(intent);
            this.finish();
        }
    }

    //For spinner control - manual
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        spinnerPeriod.setSelection(position);
        selectedPeriod = position + 1;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        //
    }


    private boolean isValidBp(String bp) {

        // int bpValue = Integer.parseInt(bp);

        try {
            if (bp.contains(".")) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                handleBackAction();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private boolean isValidHr(String bp) {

        Double bpValue = Double.parseDouble(bp);

        if (bpValue > bodyRecordConstant.GET_HR_UPPER_LIMIT || bpValue <= bodyRecordConstant.GET_HR_LOWER_LIMIT) {
            return false;
        }
        try {
            if (bp.contains(".")) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        handleBackAction();
    }

    private void handleBackAction() {
        Intent intent = new Intent();
        intent.setClass(this, MainActivity.class);
        intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_OTHER_INDEX);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        finish();
    }

}
