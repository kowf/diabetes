package com.dmc.myapplication.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.dmc.myapplication.tool.logintimedb;
import com.dmc.myapplication.tool.rewardsDB;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by lamivan on 26/12/2017.
 */

public class logintime {
  public static final String TABLE_NAME = "LOGIN_RECORD";

  public static final String LOGIN_ID = "LOGIN_ID";
  public static final String USER_ID = "USER_ID";
  public static final String USER_LOGIN_TIME = "USER_LOGIN_TIME";
  public static final String create_datetime = "create_datetime";
  public static final String edit_datetime = "edit_datetime";

  public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
          LOGIN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
          USER_ID + " INTEGER NOT NULL, " +
          USER_LOGIN_TIME + " INTEGER NOT NULL, " +
          create_datetime + " TEXT NOT NULL, " +
          edit_datetime + " TEXT NOT NULL)";

  private static SQLiteDatabase db;

  public logintime(Context context) {
    db = logintimedb.getDatabase(context);
  }

  public logintime.loginrecord_class insert(logintime.loginrecord_class BR) {
    SimpleDateFormat s = new SimpleDateFormat("yyyyMMddhhmmssSSS");
    String currentTimestamp = s.format(new Date());

    ContentValues cv = new ContentValues();
    cv.put(USER_ID, BR.getUSER_ID());
    cv.put(USER_LOGIN_TIME, BR.getUSER_LOGIN_TIME());
    if (BR.create_datetime == null || BR.create_datetime == "") {
      cv.put("create_datetime", currentTimestamp);
      cv.put("edit_datetime", currentTimestamp);
    } else {
      cv.put("create_datetime", BR.create_datetime);
      cv.put("edit_datetime", BR.edit_datetime);
    }

    long id = db.insert(TABLE_NAME, null, cv);
    return BR;
  }

  public boolean update(logintime.loginrecord_class BR) {
    SimpleDateFormat s = new SimpleDateFormat("yyyyMMddhhmmssSSS");
    String currentTimestamp = s.format(new Date());

    ContentValues cv = new ContentValues();
    cv.put(USER_LOGIN_TIME, BR.getUSER_LOGIN_TIME());
    cv.put("edit_datetime", currentTimestamp);
    String where = USER_ID + "=" + BR.getUSER_ID();
    return db.update(TABLE_NAME, cv, where, null) > 0;
  }

  public logintime.loginrecord_class getRecord(Cursor cursor) {
    // 準備回傳結果用的物件

    logintime.loginrecord_class result = new logintime.loginrecord_class(
            cursor.getInt(0),
            cursor.getInt(1),
            cursor.getInt(2));

    result.create_datetime = cursor.getString(3);
    result.edit_datetime = cursor.getString(4);

    // 回傳結果
    return result;
  }
  public logintime.loginrecord_class getByCreateDatetime(String createDatetime) {
    logintime.loginrecord_class BR = null;
    String where = create_datetime + " LIKE ?";
    Cursor result = db.query(TABLE_NAME, null, where, new String[] {createDatetime + "%"}, null, null, null, null);

    if (result.moveToFirst()) {
      BR = getRecord(result);
    } else if (result.getCount() == 0 || !result.moveToFirst()){
      BR = new logintime.loginrecord_class();
    }
    result.close();
    return BR;
  }
  public List<HashMap<String, String>> getRecordHashMapByCreateDatetime(List<String> requestedList) {
    List<HashMap<String, String>> result = new ArrayList<>();
    for (int x = 0; x < requestedList.size(); x++) {
      logintime.loginrecord_class objectBR = getByCreateDatetime(requestedList.get(x));
      result.add(objectBR.toHashMap());
    }

    return result;
  }

  public List<HashMap<String, String>> getAllCreatedatetimeEditdatetime() {

    List<HashMap<String, String>> result = new ArrayList<>();
    Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null, null);

    while (cursor.moveToNext()) {
      HashMap<String, String> newL = new HashMap<>();
      logintime.loginrecord_class br = getRecord(cursor);
      newL.put(USER_LOGIN_TIME, String.valueOf(br.getUSER_LOGIN_TIME()));
      newL.put("create_datetime", br.create_datetime);
      newL.put("edit_datetime", br.edit_datetime);
      result.add(newL);
      br = null;
      newL = null;
    }

    cursor.close();
    return result;
  }
  public boolean delete() {
    return db.delete(TABLE_NAME, null, null) > 0;
  }

  public void close() {
    db.close();
  }

  public class loginrecord_class {
    private Integer LOGIN_ID;
    private Integer USER_ID;
    public String create_datetime;
    public String edit_datetime;
    private Integer USER_LOGIN_TIME;

    public loginrecord_class() {
    }

    public loginrecord_class(Integer LOGIN_ID, Integer USER_ID, Integer USER_LOGIN_TIME) {
      this.LOGIN_ID = LOGIN_ID;
      this.USER_ID = USER_ID;
      this.USER_LOGIN_TIME = USER_LOGIN_TIME;
    }

    public Integer getUSER_LOGIN_TIME() {
      return USER_LOGIN_TIME;
    }

    public void setUSER_LOGIN_TIME(Integer USER_LOGIN_TIME) {
      this.USER_LOGIN_TIME = USER_LOGIN_TIME;
    }

    public Integer getLOGIN_ID() {
      return LOGIN_ID;
    }

    public void setLOGIN_ID(Integer LOGIN_ID) {
      this.LOGIN_ID = LOGIN_ID;
    }

    public Integer getUSER_ID() {
      return USER_ID;
    }

    public void setUSER_ID(Integer USER_ID) {
      this.USER_ID = USER_ID;
    }

    public HashMap<String, String> toHashMap() {

      HashMap<String, String> out = new HashMap<>();

      out.put(logintime.LOGIN_ID, String.valueOf(this.LOGIN_ID));
      out.put(logintime.USER_ID, String.valueOf(this.USER_ID));
      out.put(logintime.USER_LOGIN_TIME, String.valueOf(this.USER_LOGIN_TIME));
      out.put(logintime.create_datetime, String.valueOf(this.create_datetime));
      out.put(logintime.edit_datetime, String.valueOf(this.edit_datetime));

      return out;

    }

  }
}