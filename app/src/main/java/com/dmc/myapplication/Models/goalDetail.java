package com.dmc.myapplication.Models;

/**
 * Created by lamivan on 2/1/2018.
 */

public class goalDetail {
  public static final String TABLE_NAME = "USER_GOAL_DETAIL";

  public static final String DETAIL_ID	 = "DETAIL_ID";
  public static final String GOAL_ID = "GOAL_ID";
  public static final String EXPECT_FIGURE = "EXPECT_FIGURE";
  // todo: possible value: cm, inch, kg, lbs, min, steps
  public static final String FIGURE_UNIT = "FIGURE_UNIT";

//todo: 1 - weight, 2 - waist circumference, 3 - low intensity exercise, 4 - middle intensity, 5 - high intensity, 6 - steps
  public static final String GOAL_DETAIL_TYPE	 = "GOAL_TYPE";
  public static final String create_datetime = "create_datetime";
  public static final String edit_datetime = "edit_datetime";

  public static final String CREATE_TABLE =  "CREATE TABLE " + TABLE_NAME + " (" +
          DETAIL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
          GOAL_ID + " INTEGER NOT NULL, " +
          EXPECT_FIGURE + " TEXT NOT NULL, " +
          GOAL_DETAIL_TYPE + " INTEGER NOT NULL, " +
          FIGURE_UNIT + " TEXT NOT NULL, " +
          create_datetime + " TEXT NOT NULL, " +
          edit_datetime + " TEXT NOT NULL)";
}
