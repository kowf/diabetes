package com.dmc.myapplication.Models;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import com.dmc.myapplication.systemConstant;
import com.dmc.myapplication.tool.imageSyncStackDB;
import com.dmc.myapplication.tool.syncDataVersionDB;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by januslin on 24/12/2016.
 */
public class imageSyncStack {
    public static final String TABLE_NAME = "imageSyncStack";

    public static final String _ID	 = "ID";
    public static final String filename = "filename";
    public static final String imageContent = "imageContent";

    public static final String CREATE_TABLE =  "CREATE TABLE " + TABLE_NAME + " (" +
            _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            filename + " TEXT NOT NULL, " +
            imageContent+" TEXT NOT NULL);";

    private static SQLiteDatabase db;

    public imageSyncStack(Context context) {
        db = imageSyncStackDB.getDatabase(context);
    }

    public void close() {
        db.close();
    }

    public imageSyncStack_class insert(imageSyncStack_class BR){
        ContentValues cv = new ContentValues();
        //cv.put(_ID, BR.get_ID());
        cv.put(filename, BR.getFilename());
        cv.put(imageContent, BR.getImageContent());

        long id = db.insert(TABLE_NAME, null, cv);
        return BR;
    }

    public Integer countAll(){
        Cursor result = db.query(TABLE_NAME, null, null, null, null, null, null, null);
        return result.getCount();
    }


    public boolean delete(long id){
        String where = _ID + "=" + id;
        return db.delete(TABLE_NAME, where, null) > 0;
    }

    public boolean update(imageSyncStack_class BR) {
        System.out.println("sync_data_version Update");
        ContentValues cv = new ContentValues();
        cv.put(filename, BR.getFilename());
        cv.put(imageContent, BR.getImageContent());

        String where = _ID + "='" + BR.get_ID() + "'";
        return db.update(TABLE_NAME, cv, where, null) > 0;
    }

    public imageSyncStack_class get(String id) {
        imageSyncStack_class BR = null;
        String where = _ID + "='" + id + "'";
        Cursor result = db.query(TABLE_NAME, null, where, null, null, null, null, null);

        if (result.moveToFirst()) {
            BR = getRecord(result);
        }
        //result.close();
        return BR;
    }

    public List<imageSyncStack_class> getAll(){
        List<imageSyncStack_class> output = new ArrayList<>();
        Cursor result = db.query(TABLE_NAME, null, null, null, null, null, null, null);

        while (result.moveToNext()){
            output.add(getRecord(result));
        }

        return output;
    }

    public imageSyncStack_class getRecord(Cursor cursor) {
        // 準備回傳結果用的物件
        imageSyncStack_class result = new imageSyncStack_class(
                cursor.getInt(0),
                cursor.getString(1),
                cursor.getString(2)
                );
        System.out.println("getRecord 0="+cursor.getString(0) + " 1="+cursor.getString(1));
        // 回傳結果
        return result;
    }

    public class imageSyncStack_class{
        private Integer _ID;
        private String filename;
        private String imageContent;

        public imageSyncStack_class(Integer _ID, String filename, String imageContent){
            this._ID = _ID;
            this.filename = filename;
            this.imageContent = imageContent;
        }

        public Integer get_ID() {
            return _ID;
        }

        public void set_ID(Integer _ID) {
            this._ID = _ID;
        }

        public String getFilename() {
            return filename;
        }

        public String getImageContent() {
            return imageContent;
        }

        public void setImageContent(String imageContent) {
            this.imageContent = imageContent;
        }

        @Deprecated
        public String getImageString(File filesdir){
            Bitmap image = BitmapFactory.decodeFile(filesdir.getAbsolutePath()+"/"+this.filename);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.JPEG, systemConstant.IMAGE_COMPRESS, stream);
            String reducedImage = Base64.encodeToString(stream.toByteArray(), Base64.DEFAULT);
            return reducedImage;
        }

        public void setFilename(String filename) {
            this.filename = filename;
        }
    }
}
