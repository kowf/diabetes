package com.dmc.myapplication.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.dmc.myapplication.tool.foodAssessmentDB;
import com.dmc.myapplication.tool.goalDB;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by lamivan on 1/1/2018.
 */

public class goal {
  public static final String TABLE_NAME = "USER_GOAL";

  public static final String EMPTY = "GOAL_EMPTY";
  public static final String USER_ID	 = "USER_ID";
  public static final String GOAL_ID = "GOAL_ID";
  public static final String GOAL_DATE = "GOAL_DATE";
  public static final String WITNESS_USER	 = "GOAL_WITNESS";

  public static final String GOAL_TYPE	 = "GOAL_TYPE";
  public static final String GOAL_REASON = "GOAL_REASON";
  public static final String GOAL_LENGTH = "GOAL_LENGTH";
  public static final String create_datetime = "create_datetime";
  public static final String edit_datetime = "edit_datetime";

  public static final String CREATE_TABLE =  "CREATE TABLE " + TABLE_NAME + " (" +
          GOAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
          USER_ID + " INTEGER NOT NULL, " +
          GOAL_DATE + " TEXT NOT NULL, " +
          GOAL_TYPE + " INTEGER NOT NULL, " +
          WITNESS_USER + " TEXT NOT NULL, " +
          GOAL_LENGTH + " INTEGER NOT NULL, " +
          GOAL_REASON + " TEXT NOT NULL, " +
          EMPTY + " INTEGER NOT NULL, " +
          create_datetime + " TEXT NOT NULL, " +
          edit_datetime + " TEXT NOT NULL)";

  private static SQLiteDatabase db;

  public goal(Context context) {
    db = goalDB.getDatabase(context);
  }
  public goal.goal_class insert(goal.goal_class BR
                                //, String create_datetime
                                ) {
    SimpleDateFormat s = new SimpleDateFormat("yyyyMMddhhmmssSSS");
    String currentTimestamp = s.format(new Date());
    String where = create_datetime + " LIKE ?";

    ContentValues cv = new ContentValues();
    cv.put(USER_ID, BR.getUSER_ID());
    cv.put(GOAL_DATE, BR.getGOAL_DATE());
    cv.put(GOAL_TYPE, BR.getGOAL_TYPE());
    cv.put(GOAL_LENGTH, BR.getGOAL_LENGTH());
    cv.put(GOAL_REASON, BR.getGOAL_REASON());
    cv.put(WITNESS_USER, BR.getWHITNESS_USER());
    cv.put(EMPTY, 1);
    if (BR.create_datetime == null || BR.create_datetime == "") {
      cv.put("create_datetime", currentTimestamp);
      cv.put("edit_datetime", currentTimestamp);
    } else {
      cv.put("create_datetime", BR.create_datetime);
      cv.put("edit_datetime", BR.edit_datetime);
    }

    long id = db.insert(TABLE_NAME, null, cv);
    return BR;
  }
  public goal.goal_class update(goal.goal_class BR
                                , String create_datetime
  ) {
    SimpleDateFormat s = new SimpleDateFormat("yyyyMMddhhmmssSSS");
    String currentTimestamp = s.format(new Date());
    String where = create_datetime + " LIKE ?";

    ContentValues cv = new ContentValues();
    cv.put(USER_ID, BR.getUSER_ID());
    cv.put(GOAL_DATE, BR.getGOAL_DATE());
    cv.put(GOAL_TYPE, BR.getGOAL_TYPE());
    cv.put(GOAL_LENGTH, BR.getGOAL_LENGTH());
    cv.put(GOAL_REASON, BR.getGOAL_REASON());
    cv.put(WITNESS_USER, BR.getWHITNESS_USER());
    cv.put(EMPTY, 1);
    if (BR.create_datetime == null || BR.create_datetime == "") {
      cv.put("create_datetime", currentTimestamp);
      cv.put("edit_datetime", currentTimestamp);
    } else {
      cv.put("create_datetime", BR.create_datetime);
      cv.put("edit_datetime", BR.edit_datetime);
    }

    long id = db.update(TABLE_NAME, cv, where, new String[] {create_datetime + "%"});

    return BR;
  }

  public goal.goal_class getRecord(Cursor cursor) {
    // 準備回傳結果用的物件

    goal.goal_class result = new goal.goal_class(
            cursor.getInt(0),
            cursor.getInt(1),
            cursor.getString(2),
            cursor.getInt(3),
            cursor.getInt(4),
            cursor.getString(5),
            cursor.getString(6));

    result.create_datetime = cursor.getString(7);
    result.edit_datetime = cursor.getString(8);

    // 回傳結果
    return result;
  }
  public goal.goal_class getByCreateDatetime(String createDatetime) {
    goal.goal_class BR = null;
    String where = create_datetime + " LIKE ?";
    Cursor result = db.query(TABLE_NAME, null, where, new String[] {createDatetime + "%"}, null, null, null, null);

    if (result.moveToFirst()) {
      BR = getRecord(result);
    } else if (result.getCount() == 0 || !result.moveToFirst()){
      BR = new goal.goal_class();
    }
    result.close();
    return BR;
  }
  public List<HashMap<String, String>> getRecordHashMapByCreateDatetime(List<String> requestedList) {
    List<HashMap<String, String>> result = new ArrayList<>();
    for (int x = 0; x < requestedList.size(); x++) {
      goal.goal_class objectBR = getByCreateDatetime(requestedList.get(x));
      result.add(objectBR.toHashMap());
    }

    return result;
  }

  public List<HashMap<String, String>> getAllCreatedatetimeEditdatetime() {

    List<HashMap<String, String>> result = new ArrayList<>();
    Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null, null);

    while (cursor.moveToNext()) {
      HashMap<String, String> newL = new HashMap<>();
      goal.goal_class br = getRecord(cursor);
      newL.put("create_datetime", br.create_datetime);
      newL.put("edit_datetime", br.edit_datetime);
      result.add(newL);
      br = null;
      newL = null;
    }

    cursor.close();
    return result;
  }

  public boolean delete() {
    return db.delete(TABLE_NAME, null, null) > 0;
  }

  public void close() {
    db.close();
  }

  public class goal_class {
    private Integer GOAL_ID;
    private Integer USER_ID;
    private String GOAL_DATE;
    private Integer GOAL_TYPE;
    private Integer GOAL_LENGTH;
    private String GOAL_REASON;
    private String WHITNESS_USER;
    public String create_datetime;
    public String edit_datetime;
    private Integer isEmpty;

    public Integer getGOAL_ID() {
      return GOAL_ID;
    }

    public void setGOAL_ID(Integer GOAL_ID) {
      this.GOAL_ID = GOAL_ID;
    }

    public Integer getUSER_ID() {
      return USER_ID;
    }

    public void setUSER_ID(Integer USER_ID) {
      this.USER_ID = USER_ID;
    }

    public String getGOAL_DATE() {
      return GOAL_DATE;
    }

    public void setGOAL_DATE(String GOAL_DATE) {
      this.GOAL_DATE = GOAL_DATE;
    }

    public Integer getGOAL_TYPE() {
      return GOAL_TYPE;
    }

    public void setGOAL_TYPE(Integer GOAL_TYPE) {
      this.GOAL_TYPE = GOAL_TYPE;
    }

    public Integer getGOAL_LENGTH() {
      return GOAL_LENGTH;
    }

    public void setGOAL_LENGTH(Integer GOAL_LENGTH) {
      this.GOAL_LENGTH = GOAL_LENGTH;
    }

    public String getGOAL_REASON() {
      return GOAL_REASON;
    }

    public void setGOAL_REASON(String GOAL_REASON) {
      this.GOAL_REASON = GOAL_REASON;
    }

    public String getWHITNESS_USER() {
      return WHITNESS_USER;
    }

    public void setWHITNESS_USER(String WHITNESS_USER) {
      this.WHITNESS_USER = WHITNESS_USER;
    }

    public goal_class(Integer GOAL_ID, Integer USER_ID, String GOAL_DATE, Integer GOAL_TYPE, Integer GOAL_LENGTH, String GOAL_REASON, String WHITNESS_USER) {
      this.GOAL_ID = GOAL_ID;
      this.USER_ID = USER_ID;
      this.GOAL_DATE = GOAL_DATE;
      this.GOAL_TYPE = GOAL_TYPE;
      this.GOAL_LENGTH = GOAL_LENGTH;
      this.GOAL_REASON = GOAL_REASON;
      this.WHITNESS_USER = WHITNESS_USER;
      this.isEmpty = 1;
    }

    public goal_class() {
      this.isEmpty = 0;
      SimpleDateFormat s = new SimpleDateFormat("yyyyMMddhhmmssSSS");
      this.create_datetime = s.format(new Date());
    }
    public HashMap<String, String> toHashMap() {

      HashMap<String, String> out = new HashMap<>();

      out.put(goal.create_datetime, String.valueOf(this.create_datetime));
      out.put(goal.EMPTY, String.valueOf(this.isEmpty));
      if (isEmpty == 1) {
        out.put(goal.GOAL_ID, String.valueOf(this.GOAL_ID));
        out.put(goal.USER_ID, String.valueOf(this.USER_ID));
        out.put(goal.GOAL_DATE, String.valueOf(this.GOAL_DATE));
        out.put(goal.GOAL_TYPE, String.valueOf(this.GOAL_TYPE));
        out.put(goal.GOAL_LENGTH, String.valueOf(this.GOAL_LENGTH));
        out.put(goal.GOAL_REASON, String.valueOf(this.GOAL_REASON));
        out.put(goal.WITNESS_USER, String.valueOf(this.WHITNESS_USER));
        out.put(foodAssessment.edit_datetime, String.valueOf(this.edit_datetime));
      }
      return out;

    }

  }

}
