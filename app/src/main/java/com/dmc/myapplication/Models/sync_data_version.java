package com.dmc.myapplication.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.dmc.myapplication.tool.localDBHelper;
import com.dmc.myapplication.tool.syncDataVersionDB;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by januslin on 24/12/2016.
 */
public class sync_data_version {
    public static final String TABLE_NAME = "SYNC_DATA_VERSION";

    public static final String TABLE	 = "oTABLE";
    public static final String VERSION = "oVERSION";

    public static final String CREATE_TABLE =  "CREATE TABLE " + TABLE_NAME + " (" +
            TABLE + " TEXT PRIMARY KEY, " +
            VERSION + " INTEGER NOT NULL);";

    private static SQLiteDatabase db;

    public sync_data_version(Context context) {
        db = syncDataVersionDB.getDatabase(context);
    }

    public void close() {
        db.close();
    }

    public sync_data_version_class insert(sync_data_version_class BR){
        ContentValues cv = new ContentValues();
        cv.put(TABLE, BR.getTABLE());
        cv.put(VERSION, BR.getVERSION());

        long id = db.insert(TABLE_NAME, null, cv);
        return BR;
    }

    public boolean update(sync_data_version_class BR) {
        System.out.println("sync_data_version Update");
        ContentValues cv = new ContentValues();
        cv.put(VERSION, BR.getVERSION());

        String where = TABLE + "='" + BR.getTABLE() + "'";
        return db.update(TABLE_NAME, cv, where, null) > 0;
    }

    public sync_data_version_class get(String id) {
        sync_data_version_class BR = null;
        String where = TABLE + "='" + id + "'";
        Cursor result = db.query(TABLE_NAME, null, where, null, null, null, null, null);

        if (result.moveToFirst()) {
            BR = getRecord(result);
        }
        //result.close();
        return BR;
    }

    public sync_data_version_class getRecord(Cursor cursor) {
        // 準備回傳結果用的物件
        sync_data_version_class result = new sync_data_version_class(
                cursor.getString(0),
                cursor.getInt(1)
        );
        System.out.println("getRecord 0="+cursor.getString(0) + " 1="+cursor.getString(1));
        // 回傳結果
        return result;
    }

    public class sync_data_version_class{
        private String TABLE;
        private Integer VERSION;

        public sync_data_version_class(String inTable, Integer inVersion){
            this.TABLE = inTable;
            this.VERSION = inVersion;
        }

        public String getTABLE(){
            return TABLE;
        }

        public Integer getVERSION(){
            return VERSION;
        }
    }
}
