package com.dmc.myapplication.otherBodyIndex;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dmc.myapplication.R;
import com.dmc.myapplication.localDB.RealmFunction;
import com.dmc.myapplication.pojo.BloodTestRecord;
import com.dmc.myapplication.util.DateUtil;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tsunmingtso on 2/11/2017.
 */

public class Page_BloodSugarChart extends Fragment {
    BarChart barChart_FBG, barChart_HbA1c;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.page_bloodsugarchart, container, false);

        barChart_FBG = (BarChart) view.findViewById(R.id.barChart_FBG);
        barChart_HbA1c = (BarChart) view.findViewById(R.id.barChart_HbA1c);

        RealmFunction realmFunction = new RealmFunction(getContext());
        DateUtil dateUtil = new DateUtil();

        /* BarChart - FBG */
        List<BloodTestRecord> list_FBG = realmFunction.getFirstFiveFBG();

        List<BarEntry> barEntries_FBG = new ArrayList<>();
        int index_FBG = 0;
        for (int i = list_FBG.size() - 1; i >= 0; i--) {
            barEntries_FBG.add(new BarEntry((float) list_FBG.get(i).getFASTING_BLOOD_SUGAR(), index_FBG++));
        }

        BarDataSet barDataSet_FBG = new BarDataSet(barEntries_FBG, "空腹血糖值");
        barDataSet_FBG.setValueTextSize(20f); // set value textSize

        Legend legend_FBG = barChart_FBG.getLegend();
        legend_FBG.setTextSize(20f); // set legend textSize

        List<String> labels_FBG = new ArrayList<>();
        for (int i = list_FBG.size() - 1; i >= 0; i--) {
            labels_FBG.add(dateUtil.dateToString_English(list_FBG.get(i).getDATE()));
        }

        BarData barData_FBG = new BarData(labels_FBG, barDataSet_FBG);
        barChart_FBG.setData(barData_FBG);
        barChart_FBG.setDescription("mmol/L");
        barChart_FBG.setDescriptionTextSize(50f); // set description textSize

        /* BarChart - HbA1c */
        List<BloodTestRecord> list_HbA1c = realmFunction.getFirstFiveHbA1c();

        List<BarEntry> barEntries_HbA1c = new ArrayList<>();
        int index_HbA1c = 0;
        for (int i = list_HbA1c.size() - 1; i >= 0; i--) {
            barEntries_HbA1c.add(new BarEntry((float) list_HbA1c.get(i).getHbA1c(), index_HbA1c++));
        }

        BarDataSet barDataSet_HbA1c = new BarDataSet(barEntries_HbA1c, "糖化血紅素");
        barDataSet_HbA1c.setValueTextSize(20f); // set value textSize

        Legend legend_HbA1c = barChart_HbA1c.getLegend();
        legend_HbA1c.setTextSize(20f); // set legend textSize

        List<String> labels_HbA1c = new ArrayList<>();
        for (int i = list_HbA1c.size() - 1; i >= 0; i--) {
            labels_HbA1c.add(dateUtil.dateToString_English(list_HbA1c.get(i).getDATE()));
        }

        BarData barData_HbA1c = new BarData(labels_HbA1c, barDataSet_HbA1c);
        barChart_HbA1c.setData(barData_HbA1c);
        barChart_HbA1c.setDescription("%");
        barChart_HbA1c.setDescriptionTextSize(50f); // set description textSize

        return view;
    }
}
