package com.dmc.myapplication.otherBodyIndex;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tsunmingtso on 6/10/2017.
 */

public class FragmentPagerAdapter_bloodRecord extends FragmentPagerAdapter {
    final int PAGE_COUNT = 2;
    //    private String tabTitles[] = new String[]{"圖表", "血糖趨勢", "血脂趨勢", "詳細資料"};
    private String tabTitles[] = new String[]{"血糖趨勢", "血脂趨勢"};
    private Context context;
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();

    public FragmentPagerAdapter_bloodRecord(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }

    public void addFragment(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }
}
