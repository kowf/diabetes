package com.dmc.myapplication.otherBodyIndex;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dmc.myapplication.R;


/**
 * Created by Po on 13/3/2016.
 */
public class otherBodyMeasurementMain extends Fragment {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    Context ctx;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.otherbodymeasure_slide, container, false);
        ctx = getActivity().getApplicationContext();

        //set Table View
        viewPager = (ViewPager) v.findViewById(R.id.viewpager);
        tabLayout = (TabLayout) v.findViewById(R.id.sliding_tabs);

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        viewPager.removeView((View) getView());
    }

    @Override
    public void onResume() {
        super.onResume();
        setHasOptionsMenu(true);
    }

    private void setupViewPager(ViewPager viewPager) {
        OtherBodyMeasurePagerAdapter adapter = new OtherBodyMeasurePagerAdapter(getChildFragmentManager(), ctx);
//        adapter.addFragment(new otherBodyMeasureFragment(), "圖表");
        adapter.addFragment(new Page_WeightChart(), "體重趨勢");
//        adapter.addFragment(new otherBodyMeasureFragmentList(), "詳細資料");
        viewPager.setAdapter(adapter);
    }


}
