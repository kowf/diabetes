package com.dmc.myapplication.otherBodyIndex;

import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.Models.bodyRecord;
import com.dmc.myapplication.R;
import com.dmc.myapplication.bodyRecord.BodyRecord;
import com.dmc.myapplication.bodyRecord.bodyAsyncTask;
import com.dmc.myapplication.bodyRecord.bodyRecordConstant;
import com.dmc.myapplication.login.User;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.systemConstant;

import java.util.Calendar;

/**
 * Created by Po on 27/3/2016.
 */
public class otherBodyReport_edit extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private Toolbar toolbar;
    Context ctx;
    private EditText editTextDate, editTextTime, editTextOrHbA1cValue, editTextOrTotalCValue, editTextOrHdlCValue, editTextOrLdlCValue, editTextOrTriValue, editTextOrRemarkCValue;
    UserLocalStore userLocalStore;
    User user;
    private String recordId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(R.anim.slide1, R.anim.slide2);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otherbodyreport_edit);
        ctx = getApplicationContext();
        userLocalStore = new UserLocalStore(this);
        user = userLocalStore.getLoggedInUser();

        final Intent intent = getIntent();
        recordId = intent.getStringExtra("recordId");
        final String recordType = intent.getStringExtra("recordType");
        final String getDate = intent.getStringExtra("date");
        final String getTime = intent.getStringExtra("time");


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // SET return <- into left hand side
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //handle the date picker
        editTextDate = (EditText) findViewById(R.id.or_add_date);
        editTextDate.setText(getDate);

        ImageView setOmDate = (ImageView) findViewById(R.id.setOr_add_date);
        setOmDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new otherBodyReportDatePickerFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        //handle the time picker
        editTextTime = (EditText) findViewById(R.id.or_add_time);
        editTextTime.setText(getTime);
        ImageView setTime = (ImageView) findViewById(R.id.setOr_add_time);
        setTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment3  = new otherBodyReportTimePickerFragment();
                newFragment3.show(getFragmentManager(), "Time Dialog");
            }
        });

        //handle the value field
        editTextOrHbA1cValue = (EditText) findViewById(R.id.or_add_hba1c_value);
        editTextOrTotalCValue = (EditText) findViewById(R.id.or_add_totalC_value);
        editTextOrHdlCValue = (EditText) findViewById(R.id.or_add_HDL_C_value);
        editTextOrLdlCValue = (EditText) findViewById(R.id.or_add_LDL_C_value);
        editTextOrTriValue = (EditText) findViewById(R.id.or_add_tri_value);
        editTextOrRemarkCValue = (EditText) findViewById(R.id.or_add_remark_value);

        if (recordType.equals("3")){
            final double getHba1c = Double.parseDouble(intent.getStringExtra("HbA1cValue"));
            editTextOrHbA1cValue.setText(Double.toString(getHba1c));
            editTextOrTotalCValue.setEnabled(false);
            editTextOrHdlCValue.setEnabled(false);
            editTextOrLdlCValue.setEnabled(false);
            editTextOrTriValue.setEnabled(false);
            editTextOrRemarkCValue.setEnabled(false);

        }else if (recordType.equals("5")){
            final double getTotalC = Double.parseDouble(intent.getStringExtra("totalCValue"));
            final double getHdlC = Double.parseDouble(intent.getStringExtra("hdlCValue"));
            final double getLdlC = Double.parseDouble(intent.getStringExtra("ldlCValue"));
            final double getTri = Double.parseDouble(intent.getStringExtra("orTriValue"));

            editTextOrTotalCValue.setText(Double.toString(getTotalC));
            editTextOrHdlCValue.setText(Double.toString(getHdlC));
            editTextOrLdlCValue.setText(Double.toString(getLdlC));
            editTextOrTriValue.setText(Double.toString(getTri));

            editTextOrHbA1cValue.setEnabled(false);
            editTextOrRemarkCValue.setEnabled(false);

        }else if(recordType.equals("6")){
            final String getRemark = intent.getStringExtra("remark");
            editTextOrRemarkCValue.setText(getRemark);
            editTextOrHbA1cValue.setEnabled(false);
            editTextOrTotalCValue.setEnabled(false);
            editTextOrHdlCValue.setEnabled(false);
            editTextOrLdlCValue.setEnabled(false);
            editTextOrTriValue.setEnabled(false);
        }

        Button buttonOrAdd = (Button) findViewById(R.id.or_add_button);
        buttonOrAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (recordType.equals("3")){
                    validHba1c();
                }else if (recordType.equals("5")){
                    validTotalC();
                }else if(recordType.equals("6")){
                    validRemark();
                }
            }
        });

    }

    private void validHba1c() {
        // Reset errors.
        editTextOrHbA1cValue.setError(null);

        // Store values of input.
        String orHbA1c = editTextOrHbA1cValue.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid input, if the user entered one.
        if (TextUtils.isEmpty(orHbA1c) || !isValidHbA1c(orHbA1c) ) {
                editTextOrHbA1cValue.setError(getString(R.string.or_HbA1c_error));
                focusView = editTextOrHbA1cValue;
                cancel = true;
        }

        if (cancel && (focusView!= null)) {
            focusView.requestFocus();
        } else {
            String date = editTextDate.getText().toString();
            String time = editTextTime.getText().toString();

            //bodyAsyncTask editRecord = new bodyAsyncTask(this, bodyRecordConstant.EDIT_BODY_RECORD,bodyRecordConstant.GET_HBA1C_TYPE_CODE, bodyRecordConstant.VIEW_MODE_GRAPH, ctx);
            //String user_id = Integer.toString(user.userid);
            //String record_type = bodyRecordConstant.GET_HBA1C_TYPE_CODE;
            //editRecord.execute(user_id, record_type, date, time, orHbA1c, recordId );

            bodyRecord bodyRecord = new bodyRecord(getApplicationContext());
            BodyRecord a = new BodyRecord();
            bodyRecord.update(a.new bodyRecord(Integer.parseInt(recordId), Integer.parseInt(bodyRecordConstant.GET_HBA1C_TYPE_CODE), (user.userid), date, time, 0, 0, 0, 0, 0, 0, 0, Double.parseDouble(orHbA1c), 0, 0, 0, 0, 0, 0, 0, ""));
            Intent intent = new Intent();
            intent.setClass(this, MainActivity.class);
            intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_OTHER_INDEX);
            this.startActivity(intent);
            this.finish();
        }
    }


    private void validTotalC() {

        // Reset errors.
         editTextOrTotalCValue.setError(null);
        editTextOrHdlCValue.setError(null);
        editTextOrLdlCValue.setError(null);
        editTextOrTriValue.setError(null);

        // Store values of input.
        String orTotalC = editTextOrTotalCValue.getText().toString();
        String orHdlC = editTextOrHdlCValue.getText().toString();
        String orLdlC = editTextOrLdlCValue.getText().toString();
        String orTri = editTextOrTriValue.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid input, if the user entered one.
        if (!TextUtils.isEmpty(orTotalC)) {
            if(!isValidTotalC(orTotalC)) {
                editTextOrTotalCValue.setError(getString(R.string.or_totalC_error));
                focusView = editTextOrTotalCValue;
                cancel = true;
            }
        }

        if (!TextUtils.isEmpty(orHdlC)) {
            if(!isValidTotalC(orHdlC)) {
                editTextOrHdlCValue.setError(getString(R.string.or_HDL_C_error));
                focusView = editTextOrHdlCValue;
                cancel = true;
            }
        }

        if (!TextUtils.isEmpty(orLdlC)) {
            if(!isValidTotalC(orLdlC)) {
                editTextOrLdlCValue.setError(getString(R.string.or_LDL_C_error));
                focusView = editTextOrLdlCValue;
                cancel = true;
            }
        }

        if (!TextUtils.isEmpty(orTri)) {
            if(!isValidTotalC(orTri)) {
                editTextOrTriValue.setError(getString(R.string.or_tri_error));
                focusView = editTextOrTriValue;
                cancel = true;
            }
        }

        if (TextUtils.isEmpty(orTotalC) ||  TextUtils.isEmpty(orHdlC) || TextUtils.isEmpty(orLdlC) || TextUtils.isEmpty(orTri) ) {
            if (TextUtils.isEmpty(orTotalC)) {
                editTextOrTotalCValue.setError(getString(R.string.or_C_error));
                focusView = editTextOrTotalCValue;
            }
            if (TextUtils.isEmpty(orHdlC)) {
                editTextOrHdlCValue.setError(getString(R.string.or_C_error));
                focusView = editTextOrHdlCValue;
            }
            if (TextUtils.isEmpty(orLdlC)) {
                editTextOrLdlCValue.setError(getString(R.string.or_C_error));
                focusView = editTextOrLdlCValue;
            }
            if (TextUtils.isEmpty(orTri)) {
                editTextOrTriValue.setError(getString(R.string.or_C_error));
                focusView = editTextOrTriValue;
            }
            cancel = true;
        }

        if (cancel && (focusView!= null)) {
            focusView.requestFocus();
        } else {

            String date = editTextDate.getText().toString();
            String time = editTextTime.getText().toString();

            //bodyAsyncTask addRecord = new bodyAsyncTask(this, bodyRecordConstant.EDIT_BODY_RECORD, bodyRecordConstant.GET_TRIGLYCERIDERS_TYPE_CODE, bodyRecordConstant.VIEW_MODE_GRAPH, ctx);
            //String user_id = Integer.toString(user.userid);
            //String record_type = bodyRecordConstant.GET_TRIGLYCERIDERS_TYPE_CODE;
            //addRecord.execute(user_id, record_type, date, time, orTotalC, orHdlC, orLdlC, orTri, recordId);

            bodyRecord bodyRecord = new bodyRecord(getApplicationContext());
            BodyRecord a = new BodyRecord();
            bodyRecord.update(a.new bodyRecord(Integer.parseInt(recordId), Integer.parseInt(bodyRecordConstant.GET_TRIGLYCERIDERS_TYPE_CODE), (user.userid), date, time, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, Double.parseDouble(orTotalC), Double.parseDouble(orLdlC), Double.parseDouble(orHdlC), Double.parseDouble(orTri), ""));
            Intent intent = new Intent();
            intent.setClass(this, MainActivity.class);
            intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_OTHER_INDEX);
            this.startActivity(intent);
            this.finish();
        }

    }

    private void validRemark() {

        // Reset errors.
        editTextOrRemarkCValue.setError(null);

        // Store values of input.
        String orRemark = editTextOrRemarkCValue.getText().toString();

        boolean cancel = false;
        View focusView = null;


        // Check for a valid input, if the user entered one.

        if(TextUtils.isEmpty(orRemark)) {
            cancel = true;
        }

        if (cancel && (focusView!= null)) {
            focusView.requestFocus();
        } else {

                String date = editTextDate.getText().toString();
                String time = editTextTime.getText().toString();

                //bodyAsyncTask addRecord = new bodyAsyncTask(this, bodyRecordConstant.EDIT_BODY_RECORD,bodyRecordConstant.GET_REMARK_TYPE_CODE, bodyRecordConstant.VIEW_MODE_GRAPH, ctx);
                //String user_id = Integer.toString(user.userid);
                //String record_type = bodyRecordConstant.GET_REMARK_TYPE_CODE;
                //addRecord.execute(user_id, record_type, date, time, orRemark, recordId);

            BodyRecord a = new BodyRecord();
            bodyRecord.update(a.new bodyRecord(Integer.parseInt(recordId), Integer.parseInt(bodyRecordConstant.GET_REMARK_TYPE_CODE), (user.userid), date, time, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, orRemark));
            Intent intent = new Intent();
            intent.setClass(this, MainActivity.class);
            intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_OTHER_INDEX);
            this.startActivity(intent);
            this.finish();

        }
    }

    //For spinner control - manual
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }
    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        //
    }
    //For spinner control - auto


    private boolean isValidHbA1c(String or){
        double orValue = Double.parseDouble(or);

        if (orValue < bodyRecordConstant.GET_HBA1C_LOWER_LIMIT || orValue >= bodyRecordConstant.GET_HBA1C_UPPER_LIMIT){
            return false;
        }
           return true;
    }


    private boolean isValidTotalC(String or){
        double orValue = Double.parseDouble(or);

        if (orValue < bodyRecordConstant.GET_TOTALC_LOWER_LIMIT || orValue >= bodyRecordConstant.GET_TOTALC_UPPER_LIMIT){
            return false;
        }
        return true;
    }



    @Override
    public void onBackPressed() {
        handleBackAction();
    }


    private void handleBackAction(){
        Intent intent = new Intent();
        intent.setClass(this, MainActivity.class);
        intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_OTHER_INDEX);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        finish();
    }

}
