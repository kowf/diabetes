package com.dmc.myapplication.otherBodyIndex;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dmc.myapplication.R;
import com.dmc.myapplication.localDB.RealmFunction;
import com.dmc.myapplication.pojo.BloodTestRecord;
import com.dmc.myapplication.util.DateUtil;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tsunmingtso on 28/12/2017.
 */

public class Page_BloodLipidChart extends Fragment {
    BarChart barChart_TC, barChart_HDL, barChart_LDL, barChart_TG;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.page_bloodlipidchart, container, false);

        barChart_TC = (BarChart) view.findViewById(R.id.barChart_TC);
        barChart_HDL = (BarChart) view.findViewById(R.id.barChart_HDL);
        barChart_LDL = (BarChart) view.findViewById(R.id.barChart_LDL);
        barChart_TG = (BarChart) view.findViewById(R.id.barChart_TG);

        RealmFunction realmFunction = new RealmFunction(getContext());
        DateUtil dateUtil = new DateUtil();

        /* BarChart - TC */
        List<BloodTestRecord> list_TC = realmFunction.getFirstFiveTC();

        List<BarEntry> barEntries_TC = new ArrayList<>();
        int index_TC = 0;
        for (int i = list_TC.size() - 1; i >= 0; i--) {
            barEntries_TC.add(new BarEntry((float) list_TC.get(i).getTOTAL_C(), index_TC++));
        }

        BarDataSet barDataSet_TC = new BarDataSet(barEntries_TC, "總膽固醇");
        barDataSet_TC.setValueTextSize(20f); // set value textSize

        Legend legend_TC = barChart_TC.getLegend();
        legend_TC.setTextSize(20f); // set legend textSize

        List<String> labels_TC = new ArrayList<>();
        for (int i = list_TC.size() - 1; i >= 0; i--) {
            labels_TC.add(dateUtil.dateToString_English(list_TC.get(i).getDATE()));
        }

        BarData barData_TC = new BarData(labels_TC, barDataSet_TC);
        barChart_TC.setData(barData_TC);
        barChart_TC.setDescription("mmol/L");
        barChart_TC.setDescriptionTextSize(50f); // set description textSize

        /* BarChart - HDL */
        List<BloodTestRecord> list_HDL = realmFunction.getFirstFiveHDL();

        List<BarEntry> barEntries_HDL = new ArrayList<>();
        int index_HDL = 0;
        for (int i = list_HDL.size() - 1; i >= 0; i--) {
            barEntries_HDL.add(new BarEntry((float) list_HDL.get(i).getHDL_C(), index_HDL++));
        }

        BarDataSet barDataSet_HDL = new BarDataSet(barEntries_HDL, "高密度膽固醇");
        barDataSet_HDL.setValueTextSize(20f); // set value textSize

        Legend legend_HDL = barChart_HDL.getLegend();
        legend_HDL.setTextSize(20f); // set legend textSize

        List<String> labels_HDL = new ArrayList<>();
        for (int i = list_HDL.size() - 1; i >= 0; i--) {
            labels_HDL.add(dateUtil.dateToString_English(list_HDL.get(i).getDATE()));
        }

        BarData barData_HDL = new BarData(labels_HDL, barDataSet_HDL);
        barChart_HDL.setData(barData_HDL);
        barChart_HDL.setDescription("mmol/L");
        barChart_HDL.setDescriptionTextSize(50f); // set description textSize

        /* BarChart - LDL */
        List<BloodTestRecord> list_LDL = realmFunction.getFirstFiveLDL();

        List<BarEntry> barEntries_LDL = new ArrayList<>();
        int index_LDL = 0;
        for (int i = list_LDL.size() - 1; i >= 0; i--) {
            barEntries_LDL.add(new BarEntry((float) list_LDL.get(i).getLDL_C(), index_LDL++));
        }

        BarDataSet barDataSet_LDL = new BarDataSet(barEntries_LDL, "低密度膽固醇");
        barDataSet_LDL.setValueTextSize(20f); // set value textSize

        Legend legend_LDL = barChart_LDL.getLegend();
        legend_LDL.setTextSize(20f); // set legend textSize

        List<String> labels_LDL = new ArrayList<>();
        for (int i = list_LDL.size() - 1; i >= 0; i--) {
            labels_LDL.add(dateUtil.dateToString_English(list_LDL.get(i).getDATE()));
        }

        BarData barData_LDL = new BarData(labels_LDL, barDataSet_LDL);
        barChart_LDL.setData(barData_LDL);
        barChart_LDL.setDescription("mmol/L");
        barChart_LDL.setDescriptionTextSize(50f); // set description textSize

        /* BarChart - TG */
        List<BloodTestRecord> list_TG = realmFunction.getFirstFiveTG();

        List<BarEntry> barEntries_TG = new ArrayList<>();
        int index_TG = 0;
        for (int i = list_TG.size() - 1; i >= 0; i--) {
            barEntries_TG.add(new BarEntry((float) list_TG.get(i).getTRIGLYCERIDES(), index_TG++));
        }

        BarDataSet barDataSet_TG = new BarDataSet(barEntries_TG, "三酸甘油脂");
        barDataSet_TG.setValueTextSize(20f); // set value textSize

        Legend legend_TG = barChart_TG.getLegend();
        legend_TG.setTextSize(20f); // set legend textSize

        List<String> labels_TG = new ArrayList<>();
        for (int i = list_TG.size() - 1; i >= 0; i--) {
            labels_TG.add(dateUtil.dateToString_English(list_TG.get(i).getDATE()));
        }

        BarData barData_TG = new BarData(labels_TG, barDataSet_TG);
        barChart_TG.setData(barData_TG);
        barChart_TG.setDescription("mmol/L");
        barChart_TG.setDescriptionTextSize(50f); // set description textSize

        return view;
    }
}
