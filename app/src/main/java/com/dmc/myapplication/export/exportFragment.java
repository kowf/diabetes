package com.dmc.myapplication.export;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.dmc.myapplication.R;
import com.dmc.myapplication.login.User;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.systemConstant;
import com.dmc.myapplication.tool.exportTool;

import java.io.File;

/**
 * Created by KwokSinMan on 24/2/2016.
 */
public class exportFragment extends Fragment{

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.export_main, container, false);
        final Button generalButton = (Button) v.findViewById(R.id.generalExport);
        generalButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {

                exportGeneralAsyncTask exportGeneralFile = new exportGeneralAsyncTask(getActivity(), exportConstant.EXPORT_GENERAL);
                exportGeneralFile.execute();

            }
        });

        Button bodyIndexButton = (Button) v.findViewById(R.id.bodyIndexExport);
        bodyIndexButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                exportBodyIndexAsyncTask connect = new exportBodyIndexAsyncTask(getActivity(), exportConstant.EXPORT_BODY_INDEX);
                connect.execute();
            }
        });

        return v;
    }
}
