package com.dmc.myapplication.localDB;

import android.content.Context;

import com.dmc.myapplication.pojo.BloodTestRecord;
import com.dmc.myapplication.pojo.BodyIndexRecord;
import com.dmc.myapplication.pojo.User;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by tsunmingtso on 21/12/2017.
 */

public class RealmFunction {
    private String TAG = this.getClass().getSimpleName();

    private Realm realm;

    public RealmFunction(Context context) {
        Realm.init(context);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        realm = Realm.getInstance(config);
    }

    /* Body Index Record */
    public void deleteAllBodyIndexRecords() {
        realm.beginTransaction();
        RealmResults<BodyIndexRecord> results = realm.where(BodyIndexRecord.class).findAll();
        results.deleteAllFromRealm();
        realm.commitTransaction();
    }

    public void saveBodyIndexRecordsFromJSON(JSONArray json) {
        realm.beginTransaction();
        realm.createAllFromJson(BodyIndexRecord.class, json);
        realm.commitTransaction();
    }

    public void saveBodyIndexRecord(int userID, int recordType, Date insertDate, double height, double weight, double waistLine, double BMI, double fat) {
        realm.beginTransaction();
        BodyIndexRecord bodyIndexRecord = new BodyIndexRecord();
        bodyIndexRecord.setUSER_ID(userID);
        bodyIndexRecord.setRECORD_TYPE(recordType);
        bodyIndexRecord.setDATE(insertDate);
        bodyIndexRecord.setHEIGHT(height);
        bodyIndexRecord.setWEIGHT(weight);
        bodyIndexRecord.setWAIST(waistLine);
        bodyIndexRecord.setBMI(BMI);
        bodyIndexRecord.setFAT(fat);
        realm.insert(bodyIndexRecord);
        realm.commitTransaction();
    }

    public List<BodyIndexRecord> getFirstFiveWeight() {
        RealmResults<BodyIndexRecord> results = realm.where(BodyIndexRecord.class)
                .notEqualTo("WEIGHT", 0.0)
                .findAllSorted("DATE", Sort.DESCENDING);
        if (results.size() > 5) {
            return results.subList(0, 5);
        } else {
            return results;
        }
    }

    public List<BodyIndexRecord> getFirstFiveBMI() {
        RealmResults<BodyIndexRecord> results = realm.where(BodyIndexRecord.class)
                .notEqualTo("BMI", 0.0)
                .findAllSorted("DATE", Sort.DESCENDING);
        if (results.size() > 5) {
            return results.subList(0, 5);
        } else {
            return results;
        }
    }

    public List<BodyIndexRecord> getFirstFiveWaist() {
        RealmResults<BodyIndexRecord> results = realm.where(BodyIndexRecord.class)
                .notEqualTo("WAIST", 0.0)
                .findAllSorted("DATE", Sort.DESCENDING);
        if (results.size() > 5) {
            return results.subList(0, 5);
        } else {
            return results;
        }
    }

    public List<BodyIndexRecord> getFirstFiveFat() {
        RealmResults<BodyIndexRecord> results = realm.where(BodyIndexRecord.class)
                .notEqualTo("FAT", 0.0)
                .findAllSorted("DATE", Sort.DESCENDING);
        if (results.size() > 5) {
            return results.subList(0, 5);
        } else {
            return results;
        }
    }

    public List<BodyIndexRecord> getAllBodyIndexRecordsByDate() {
        return realm.where(BodyIndexRecord.class).findAll().sort("DATE");
    }

    /* Blood Test Record */
    public void deleteAllBloodTestRecords() {
        realm.beginTransaction();
        RealmResults<BloodTestRecord> results = realm.where(BloodTestRecord.class).findAll();
        results.deleteAllFromRealm();
        realm.commitTransaction();
    }

    public void saveBloodTestRecordsFromJSON(JSONArray json) {
        realm.beginTransaction();
        realm.createAllFromJson(BloodTestRecord.class, json);
        realm.commitTransaction();
    }

    public void saveBloodTestRecord(int userID, int recordType, Date insertDate, double fastingBloodSugar, double hba1c, double totalC, double hdlC, double ldlC, double triglycerides) {
        realm.beginTransaction();
        BloodTestRecord bloodTestRecord = new BloodTestRecord();
        bloodTestRecord.setUSER_ID(userID);
        bloodTestRecord.setRECORD_TYPE(recordType);
        bloodTestRecord.setDATE(insertDate);
        bloodTestRecord.setFASTING_BLOOD_SUGAR(fastingBloodSugar);
        bloodTestRecord.setHbA1c(hba1c);
        bloodTestRecord.setTOTAL_C(totalC);
        bloodTestRecord.setHDL_C(hdlC);
        bloodTestRecord.setLDL_C(ldlC);
        bloodTestRecord.setTRIGLYCERIDES(triglycerides);
        realm.insert(bloodTestRecord);
        realm.commitTransaction();
    }

    public List<BloodTestRecord> getFirstFiveFBG() {
        RealmResults<BloodTestRecord> results = realm.where(BloodTestRecord.class)
                .notEqualTo("FASTING_BLOOD_SUGAR", 0.0)
                .findAllSorted("DATE", Sort.DESCENDING);
        if (results.size() > 5) {
            return results.subList(0, 5);
        } else {
            return results;
        }
    }

    public List<BloodTestRecord> getFirstFiveHbA1c() {
        RealmResults<BloodTestRecord> results = realm.where(BloodTestRecord.class)
                .notEqualTo("HbA1c", 0.0)
                .findAllSorted("DATE", Sort.DESCENDING);
        if (results.size() > 5) {
            return results.subList(0, 5);
        } else {
            return results;
        }
    }

    public List<BloodTestRecord> getFirstFiveTC() {
        RealmResults<BloodTestRecord> results = realm.where(BloodTestRecord.class)
                .notEqualTo("TOTAL_C", 0.0)
                .findAllSorted("DATE", Sort.DESCENDING);
        if (results.size() > 5) {
            return results.subList(0, 5);
        } else {
            return results;
        }
    }

    public List<BloodTestRecord> getFirstFiveHDL() {
        RealmResults<BloodTestRecord> results = realm.where(BloodTestRecord.class)
                .notEqualTo("HDL_C", 0.0)
                .findAllSorted("DATE", Sort.DESCENDING);
        if (results.size() > 5) {
            return results.subList(0, 5);
        } else {
            return results;
        }
    }

    public List<BloodTestRecord> getFirstFiveLDL() {
        RealmResults<BloodTestRecord> results = realm.where(BloodTestRecord.class)
                .notEqualTo("LDL_C", 0.0)
                .findAllSorted("DATE", Sort.DESCENDING);
        if (results.size() > 5) {
            return results.subList(0, 5);
        } else {
            return results;
        }
    }

    public List<BloodTestRecord> getFirstFiveTG() {
        RealmResults<BloodTestRecord> results = realm.where(BloodTestRecord.class)
                .notEqualTo("TRIGLYCERIDES", 0.0)
                .findAllSorted("DATE", Sort.DESCENDING);
        if (results.size() > 5) {
            return results.subList(0, 5);
        } else {
            return results;
        }
    }

    /* User Info */
    public void saveUserInfoFromJSON(JSONObject json) {
        realm.beginTransaction();
        realm.createObjectFromJson(User.class, json);
        realm.commitTransaction();
    }

    public User getUserInfo() {
        return realm.where(User.class).findFirst();
    }
}
