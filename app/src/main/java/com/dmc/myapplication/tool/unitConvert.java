package com.dmc.myapplication.tool;

/**
 * Created by Po on 28/3/2016.
 */
public class unitConvert {

    //unit converter - cm to inch
    public static double CmToInches(double cm) {
        double inch = cm * 0.3937 + 0.2;
        //System.out.println("inch: "+inch);
        return inch;
    }

    //unit converter - inch to cm
    public static double InchToCm(double inch) {
        double cm = inch / 0.3937 ;
        //System.out.println("cm: "+cm);
        return cm;
    }

    //unit converter - kg to lb
    public static double KgToLb(double kg) {
        double lb = kg * 2.2046226218 ;
        //System.out.println("lb: "+lb);
        return lb;
    }

    //unit converter - lb to kg
    public static double LbToKg(double lb) {
        double kg = lb / 2.2046226218  ;
        //System.out.println("kg: "+kg);
        return kg;
    }


}
