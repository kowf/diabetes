package com.dmc.myapplication.tool;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.dmc.myapplication.Models.foodAssessment;
import com.dmc.myapplication.Models.foodRecord;

/**
 * Created by lamivan on 14/11/2017.
 */

public class foodAssessmentDB extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "foodAssessDB.db";

    public static final int VERSION = 1;
    private static SQLiteDatabase database;

    public foodAssessmentDB(Context context) {
      super(context, DATABASE_NAME, null, VERSION);
    }

    public static SQLiteDatabase getDatabase(Context context) {
      if (database == null || !database.isOpen()) {
        database = new foodAssessmentDB(context).getWritableDatabase();
      }

      return database;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
      //
      db.execSQL(foodAssessment.CREATE_TABLE);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
      //
      db.execSQL("DROP TABLE IF EXISTS " + foodAssessment.TABLE_NAME);
      onCreate(db);
    }
  }


