package com.dmc.myapplication.tool;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.Base64;
import android.widget.ImageView;

import com.dmc.myapplication.R;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by KwokSinMan on 11/3/2016.
 */
public class imageTool {

    public static void setPic(ImageView drugImage,String mCurrentPhotoPath ){
        try {
            if (mCurrentPhotoPath != null && !mCurrentPhotoPath.isEmpty()) {
                Bitmap bm;
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(mCurrentPhotoPath, options);
                final int REQUIRED_SIZE = 200;
                int scale = 1;
                while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                        && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                    scale *= 2;
                options.inSampleSize = scale;
                options.inJustDecodeBounds = false;
                drugImage.setImageBitmap(imageTool.rotateImage90(BitmapFactory.decodeFile(mCurrentPhotoPath, options)));
            } else {
                drugImage.setImageResource(R.drawable.ic_local_see_black_48dp);
            }
        }catch (Exception e){
            e.printStackTrace();
            drugImage.setImageResource(R.drawable.ic_local_see_black_48dp);
        }
    }

    public static Bitmap decreaseImageSie(String imgPath){
        Bitmap reducedImage = null;
        if (imgPath != null && !imgPath.isEmpty()) {
            BitmapFactory.Options options = null;
            options = new BitmapFactory.Options();
            options.inSampleSize = 4;
            reducedImage = BitmapFactory.decodeFile(imgPath,options);


           // ByteArrayOutputStream stream = new ByteArrayOutputStream();
           // bitmap.compress(Bitmap.CompressFormat.JPEG, 10, stream);
            //byte[] decreasedImage =  stream.toByteArray();
            //bitmap = BitmapFactory.decodeByteArray(decreasedImage, 0, decreasedImage.length);
            //System.out.println("Candy =" + decreasedImage.length);

            // Encode Image to String
            //reducedImage =  Base64.encodeToString(decreasedImage, Base64.DEFAULT);
        }
        return reducedImage;
    }

    public static Bitmap rotateImage90(Bitmap bitmap)
    {
        Matrix mtx = new Matrix();
        mtx.postRotate(90);
        // Rotating Bitmap
        Bitmap rotatedBMP = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), mtx, true);
        if (rotatedBMP != bitmap)
            bitmap.recycle();

        return rotatedBMP;
    }

    public static String getImageName(){
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return "JPEG_" + timeStamp + "_";
    }



}
