package com.dmc.myapplication.tool;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.dmc.myapplication.Models.goal;

/**
 * Created by lamivan on 1/1/2018.
 */

public class goalDetailDB extends SQLiteOpenHelper {
  public static final String DATABASE_NAME = "goalDetailDB.db";

  public static final int VERSION = 1;
  private static SQLiteDatabase database;

  public goalDetailDB(Context context) {
    super(context, DATABASE_NAME, null, VERSION);
  }

  public static SQLiteDatabase getDatabase(Context context) {
    if (database == null || !database.isOpen()) {
      database = new goalDetailDB(context).getWritableDatabase();
    }

    return database;
  }

  @Override
  public void onCreate(SQLiteDatabase db) {
    db.execSQL(goal.CREATE_TABLE);

  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    //
    db.execSQL("DROP TABLE IF EXISTS " + goal.TABLE_NAME);
    onCreate(db);
  }
}
