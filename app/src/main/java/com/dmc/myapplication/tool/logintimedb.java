package com.dmc.myapplication.tool;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.dmc.myapplication.Models.logintime;
import com.dmc.myapplication.Models.rewards;

/**
 * Created by lamivan on 26/12/2017.
 */

public class logintimedb extends SQLiteOpenHelper {
  public static final String DATABASE_NAME = "login.db";

  public static final int VERSION = 1;
  private static SQLiteDatabase database;

  public logintimedb(Context context) {
    super(context, DATABASE_NAME, null, VERSION);
  }

  public static SQLiteDatabase getDatabase(Context context) {
    if (database == null || !database.isOpen()) {
      database = new logintimedb(context).getWritableDatabase();
    }

    return database;
  }

  @Override
  public void onCreate(SQLiteDatabase db) {

    db.execSQL(logintime.CREATE_TABLE);
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    //
    db.execSQL("DROP TABLE IF EXISTS " + logintime.TABLE_NAME);
    onCreate(db);
  }
}

