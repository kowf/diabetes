package com.dmc.myapplication.tool;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.dmc.myapplication.Models.INJECTION_RECORD;
import com.dmc.myapplication.Models.foodRecord;

/**
 * Created by januslin on 11/10/2016.
 */
public class INJECTION_RECORDDB extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "INJECTIONDB.db";

    public static final int VERSION = 1;
    private static SQLiteDatabase database;

    public INJECTION_RECORDDB(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    public static SQLiteDatabase getDatabase(Context context) {
        if (database == null || !database.isOpen()) {
            database = new INJECTION_RECORDDB(context).getWritableDatabase();
        }

        return database;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //
        db.execSQL(INJECTION_RECORD.CREATE_TABLE);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //
        db.execSQL("DROP TABLE IF EXISTS " + INJECTION_RECORD.TABLE_NAME);
        onCreate(db);
    }
}
