package com.dmc.myapplication.tool;

/**
 * Created by KwokSinMan on 13/3/2016.
 */
public class timeObject {
    private int hour;
    private int min;

    public timeObject(){

    }

    public timeObject(String time){
        String [] temp = time.split(":");
        this.hour = Integer.parseInt(temp[0]);
        this.min = Integer.parseInt(temp[1]);
    }
    public int getHour() {
        return hour;
    }
    public int getMin() {
        return min;
    }

    public String getTimeString(int inputedMin){
        if (0<=inputedMin && inputedMin <=9)
            return "0" + String.valueOf(inputedMin);
        else return String.valueOf(inputedMin);
    }

    public String getUITimeFormal(){
        return getTimeString(this.hour) +":"+getTimeString(this.min);
    }
}
