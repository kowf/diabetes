package com.dmc.myapplication.reminder;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

import com.dmc.myapplication.systemConstant;

public class MyDBHelper extends SQLiteOpenHelper {


    private static SQLiteDatabase database;


    public MyDBHelper(Context context, String name, CursorFactory factory,
                      int version) {
        super(context, name, factory, version);
    }

    public static SQLiteDatabase getDatabase(Context context) {
        if (database == null || !database.isOpen()) {
            database = new MyDBHelper(context, systemConstant.DATABASE_NAME,
                    null, systemConstant.DATABASE_VERSION).getWritableDatabase();
        }

        return database;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // 建立應用程式需要的表格
        // 待會再回來完成它
        //System.out.println("Candy reminderDAO.CREATE_TABLE ="+reminderDAO.CREATE_TABLE);
        //System.out.println("Candy broadcastNumberDAO.CREATE_TABLE ="+broadcastNumberDAO.CREATE_TABLE);
        db.execSQL(reminderDAO.CREATE_TABLE);
        db.execSQL(broadcastNumberDAO.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // 刪除原有的表格
        db.execSQL("DROP TABLE IF EXISTS " + reminderDAO.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + broadcastNumberDAO.TABLE_NAME);
        // 呼叫onCreate建立新版的表格
        onCreate(db);
    }

}