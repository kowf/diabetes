package com.dmc.myapplication.reminder;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by KwokSinMan on 17/3/2016.
 */
public class broadcastNumberDAO {
    // 表格名稱
    public static final String TABLE_NAME = "BROADCAST_NUMBER ";
    // 編號表格欄位名稱，固定不變
    public static final String KEY_ID = "_broadcast_id";
    public static final String NAME_COLUMN = "reminder_id";
    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    NAME_COLUMN + " INTEGER NOT NULL) ";

    private SQLiteDatabase db;
    public broadcastNumberDAO(Context context) {
        db = MyDBHelper.getDatabase(context);
    }

    public void close() {
        db.close();
    }

    public long insert(long reminderId) {
        ContentValues cv = new ContentValues();
        cv.put(NAME_COLUMN, reminderId);
        return db.insert(TABLE_NAME, null, cv);
    }

    // 取得資料數量
    public int getCount() {
        int result = 0;
        Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM " + TABLE_NAME, null);

        if (cursor.moveToNext()) {
            result = cursor.getInt(0);
        }
        return result;
    }

    public List<broadcastNumber> getAll() {
        List<broadcastNumber> result = new ArrayList<>();
        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null, null);

        while (cursor.moveToNext()) {
            result.add(getRecord(cursor));
        }

        cursor.close();
        return result;
    }

    // 取得指定編號的資料物件
    public broadcastNumber get(long id) {
        broadcastNumber broadcastNumber = null;
        String where = KEY_ID + "=" + id;
        Cursor result = db.query(TABLE_NAME, null, where, null, null, null, null, null);

        if (result.moveToFirst()) {
            broadcastNumber = getRecord(result);
        }
        result.close();
        return broadcastNumber;
    }

    public broadcastNumber getRecord(Cursor cursor) {
        // 準備回傳結果用的物件
        broadcastNumber result = new broadcastNumber();
        result.setBroadcastId(cursor.getLong(0));
        result.setReminderId(cursor.getLong(1));
        // 回傳結果
        return result;
    }




}
