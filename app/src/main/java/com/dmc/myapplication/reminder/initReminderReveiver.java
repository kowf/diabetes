package com.dmc.myapplication.reminder;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.List;

public class initReminderReveiver extends BroadcastReceiver {
    public initReminderReveiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        reminderDAO reminderDAO = new reminderDAO(context.getApplicationContext());
        List<reminder> reminderList = reminderDAO.getAll();
        for(int i=0; i<reminderList.size(); i++){
            reminder reminder = reminderList.get(i);
            int frequencyId = reminder.getManyTime();
            reminderBiz biz = new reminderBiz();
            if(frequencyId == reminderConstant.GET_FREQUENCY_ONE){
                biz.setReminder(context.getApplicationContext(),reminder.getTime1(),reminder.getId(),Long.parseLong(reminder.getBroadcastNumber1()));
            }
            if(frequencyId == reminderConstant.GET_FREQUENCY_TWO){
                biz.setReminder(context.getApplicationContext(),reminder.getTime1(),reminder.getId(),Long.parseLong(reminder.getBroadcastNumber1()));
                biz.setReminder(context.getApplicationContext(),reminder.getTime2(),reminder.getId(),Long.parseLong(reminder.getBroadcastNumber2()));
            }
            if(frequencyId == reminderConstant.GET_FREQUENCY_THREE){
                biz.setReminder(context.getApplicationContext(),reminder.getTime1(),reminder.getId(),Long.parseLong(reminder.getBroadcastNumber1()));
                biz.setReminder(context.getApplicationContext(),reminder.getTime2(),reminder.getId(),Long.parseLong(reminder.getBroadcastNumber2()));
                biz.setReminder(context.getApplicationContext(),reminder.getTime3(),reminder.getId(),Long.parseLong(reminder.getBroadcastNumber3()));
            }
            if(frequencyId == reminderConstant.GET_FREQUENCY_FOUR){
                biz.setReminder(context.getApplicationContext(),reminder.getTime1(),reminder.getId(),Long.parseLong(reminder.getBroadcastNumber1()));
                biz.setReminder(context.getApplicationContext(),reminder.getTime2(),reminder.getId(),Long.parseLong(reminder.getBroadcastNumber2()));
                biz.setReminder(context.getApplicationContext(),reminder.getTime3(),reminder.getId(),Long.parseLong(reminder.getBroadcastNumber3()));
                biz.setReminder(context.getApplicationContext(),reminder.getTime4(),reminder.getId(),Long.parseLong(reminder.getBroadcastNumber4()));
            }
        }
    }
}
