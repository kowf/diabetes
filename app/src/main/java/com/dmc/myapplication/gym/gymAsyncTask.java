package com.dmc.myapplication.gym;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.dmc.myapplication.dmAppAsyncTask;
import com.dmc.myapplication.navFood.navFoodBiz;
import com.dmc.myapplication.navFood.navFoodConstant;
import com.dmc.myapplication.noInternetConnectionDialog;
import com.dmc.myapplication.serverNoResponseDialog;

import java.net.SocketTimeoutException;
import java.net.URLEncoder;

/**
 * Created by KwokSinMan on 1/2/2016.
 */
public class gymAsyncTask  extends AsyncTask<String,Void,String> {

    private ProgressDialog progressBar;
    private String model;
    private Activity activity;

    public gymAsyncTask(Activity activity, String model ) {
        this.activity = activity;
        this.model = model;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressBar = new ProgressDialog(activity);
        progressBar.setCancelable(false);
        progressBar.setTitle("載入中...");
        progressBar.setMessage("請稍候 !!");
        progressBar.show();
    }

    @Override
    protected String doInBackground(String... arg0) {
        String data="";
        try {
            if (model.equals(gymConstant.GET_GYM_RECORD)){
                String userId = (String)arg0[0];
                String gymDate = (String)arg0[1];
                data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(userId, "UTF-8");
                data += "&" + URLEncoder.encode("gym_date", "UTF-8") + "=" + URLEncoder.encode(gymDate, "UTF-8");
            }
            if (model.equals(gymConstant.GET_GYM_CATE_DDL)) {

            }
            if(model.equals(gymConstant.GET_GYM_All_LIST)){
                String gymCateId = (String)arg0[0];
                data  = URLEncoder.encode("gym_cate_id", "UTF-8") + "=" + URLEncoder.encode(gymCateId, "UTF-8");
            }
            if(model.equals(gymConstant.GET_GYM_SEARCH_RESULT)){
                String searchKeyword=(String)arg0[0];
                data = URLEncoder.encode("search_keyword", "UTF-8") + "=" + URLEncoder.encode(searchKeyword, "UTF-8");
            }
            if (model.equals(gymConstant.GET_GYM_COMMON_LIST)){
                String userId=(String)arg0[0];
                data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(userId, "UTF-8");
            }
            if (model.equals(gymConstant.SAVE_FREE_TEXT_GYM_RECORD)){
                String userId = (String)arg0[0];
                String gymDate = (String)arg0[1];
                String gymTime = (String)arg0[2];
                String gymValue = (String)arg0[3];
                String gymName = (String)arg0[4];

                data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(userId, "UTF-8");
                data += "&" + URLEncoder.encode("gym_date", "UTF-8") + "=" + URLEncoder.encode(gymDate, "UTF-8");
                data += "&" + URLEncoder.encode("gym_time", "UTF-8") + "=" + URLEncoder.encode(gymTime, "UTF-8");
                data += "&" + URLEncoder.encode("gym_value", "UTF-8") + "=" + URLEncoder.encode(gymValue, "UTF-8");
                data += "&" + URLEncoder.encode("gym_name", "UTF-8") + "=" + URLEncoder.encode(gymName, "UTF-8");
            }
            if (model.equals(gymConstant.SAVE_GYM_RECORD)){
                String userId = (String)arg0[0];
                String gymDate = (String)arg0[1];
                String gymTime = (String)arg0[2];
                String gymValue = (String)arg0[3];
                String gymId = (String)arg0[4];

                data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(userId, "UTF-8");
                data += "&" + URLEncoder.encode("gym_date", "UTF-8") + "=" + URLEncoder.encode(gymDate, "UTF-8");
                data += "&" + URLEncoder.encode("gym_time", "UTF-8") + "=" + URLEncoder.encode(gymTime, "UTF-8");
                data += "&" + URLEncoder.encode("gym_value", "UTF-8") + "=" + URLEncoder.encode(gymValue, "UTF-8");
                data += "&" + URLEncoder.encode("gym_id", "UTF-8") + "=" + URLEncoder.encode(gymId, "UTF-8");
            }
            if(model.equals(gymConstant.GET_GYM_DETAIL)){
                String userId = (String)arg0[0];
                String gymRecordId = (String)arg0[1];
                String isFreeText = (String)arg0[2];

                data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(userId, "UTF-8");
                data += "&" + URLEncoder.encode("gym_record_id", "UTF-8") + "=" + URLEncoder.encode(gymRecordId, "UTF-8");
                data += "&" + URLEncoder.encode("is_free_text", "UTF-8") + "=" + URLEncoder.encode(isFreeText, "UTF-8");
            }
            if (model.equals(gymConstant.DELETE_GYM_RECORD)){
                String userId = (String)arg0[0];
                String gymRecordId = (String)arg0[1];
                data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(userId, "UTF-8");
                data += "&" + URLEncoder.encode("gym_record_id", "UTF-8") + "=" + URLEncoder.encode(gymRecordId, "UTF-8");
            }
            if(model.equals(gymConstant.SAVE_EDIT_GYM_RECORD)){
                String userId = (String)arg0[0];
                String gymRecordId = (String)arg0[1];
                String gymDate = (String)arg0[2];
                String gymTime = (String)arg0[3];
                String gymValue = (String)arg0[4];
                data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(userId, "UTF-8");
                data += "&" + URLEncoder.encode("gym_record_id", "UTF-8") + "=" + URLEncoder.encode(gymRecordId, "UTF-8");
                data += "&" + URLEncoder.encode("gym_date", "UTF-8") + "=" + URLEncoder.encode(gymDate, "UTF-8");
                data += "&" + URLEncoder.encode("gym_time", "UTF-8") + "=" + URLEncoder.encode(gymTime, "UTF-8");
                data += "&" + URLEncoder.encode("gym_value", "UTF-8") + "=" + URLEncoder.encode(gymValue, "UTF-8");
            }

            //get result  from server
            dmAppAsyncTask dmasyncTask = new dmAppAsyncTask(model);
            return dmasyncTask.getDataFromServer(data);

        } catch (SocketTimeoutException e){
            e.printStackTrace();
            return dmAppAsyncTask.SERVER_SLEEP;
        } catch (Exception e) {
            e.printStackTrace();
            return dmAppAsyncTask.NO_INTERNET_CONNECTION;
        }
    }

    @Override
    protected void onPostExecute(String result){
        if (result!=null) {
            if(result.equals(dmAppAsyncTask.NO_INTERNET_CONNECTION)){
                progressBar.dismiss();
                noInternetConnectionDialog dialog = new noInternetConnectionDialog();
                dialog.show(this.activity.getFragmentManager(), "NoInternetConnectionDialog");
            }else if(result.equals(dmAppAsyncTask.SERVER_SLEEP)){
                progressBar.dismiss();
                serverNoResponseDialog dialog = new serverNoResponseDialog();
                dialog.show(this.activity.getFragmentManager(), "NoServerResponseDialog");
            }else {
                gymBiz gymBiz = new gymBiz();
                if (model.equals(gymConstant.GET_GYM_RECORD)) {
                    gymBiz.setGymRecord(this.activity, result);
                }
                if (model.equals(gymConstant.GET_GYM_CATE_DDL)) {
                    gymBiz.setGymCateDDL(this.activity, result);
                }
                if (model.equals(gymConstant.GET_GYM_All_LIST)) {
                    gymBiz.setGymAllList(this.activity, result);
                }
                if (model.equals(gymConstant.GET_GYM_SEARCH_RESULT)) {
                    gymBiz.setGymSearchResult(this.activity, result);
                }
                if (model.equals(gymConstant.GET_GYM_COMMON_LIST)) {
                    gymBiz.setGymCommonList(this.activity, result);
                }
                if (model.equals(gymConstant.SAVE_FREE_TEXT_GYM_RECORD)) {
                    gymBiz.afterSaveGymRecord(this.activity, gymConstant.IS_DELETE_ACTION_N);
                }
                if (model.equals(gymConstant.SAVE_GYM_RECORD)) {
                    gymBiz.afterSaveGymRecord(this.activity, gymConstant.IS_DELETE_ACTION_N);
                }
                if (model.equals(gymConstant.GET_GYM_DETAIL)) {
                    gymBiz.getGymDetail(this.activity, result);
                }
                if (model.equals(gymConstant.DELETE_GYM_RECORD)) {
                    gymBiz.afterSaveGymRecord(this.activity, gymConstant.IS_DELETE_ACTION_Y);
                }
                if (model.equals(gymConstant.SAVE_EDIT_GYM_RECORD)) {
                    gymBiz.afterSaveGymRecord(this.activity, gymConstant.IS_DELETE_ACTION_N);
                }
                progressBar.dismiss();
            }
        }
    }

}
