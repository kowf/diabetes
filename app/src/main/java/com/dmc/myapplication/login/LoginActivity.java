package com.dmc.myapplication.login;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.LoaderManager.LoaderCallbacks;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.Models.bodyRecord;
import com.dmc.myapplication.Models.exercise_type;
import com.dmc.myapplication.Models.exercise_type_record;
import com.dmc.myapplication.Models.food;
import com.dmc.myapplication.Models.foodAssessment;
import com.dmc.myapplication.Models.foodCategories;
import com.dmc.myapplication.Models.foodRecord;
import com.dmc.myapplication.Models.foodSubCategories;
import com.dmc.myapplication.Models.logintime;
import com.dmc.myapplication.Models.rewards;
import com.dmc.myapplication.Models.stepRecord;
import com.dmc.myapplication.Models.sync_data_version;
import com.dmc.myapplication.Models.xiaomiDeviceSetting;
import com.dmc.myapplication.R;
import com.dmc.myapplication.bodyRecord.BodyRecord;
import com.dmc.myapplication.localDB.RealmFunction;
import com.dmc.myapplication.navFood.navFoodConstant;
import com.dmc.myapplication.navFood.navfoodAsyncTask;
import com.dmc.myapplication.network.RetrofitFunction;
import com.dmc.myapplication.network.webService;
import com.dmc.myapplication.webHelper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * java for login screen and checking control
 */
public class LoginActivity extends AppCompatActivity implements LoaderCallbacks<Cursor> {

    UserLocalStore userLocalStore;
    private static Handler handler;

    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    //private UserLoginTask mAuthTask = null;
    private String mAuthTask = null;

    // UI references.
    private EditText mUserIDView;
    private View mProgressView;
    private View mLoginFormView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setHandlerFunction();

        // Set up the login form.
        mUserIDView = (EditText) findViewById(R.id.userid);
        mUserIDView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mSignInButton = (Button) findViewById(R.id.sign_in_button);
        //Button tvRegisterLink = (Button) findViewById(R.id.tvRegisterLink);

        //tvRegisterLink.setVisibility(View.GONE);

        mSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                String userID = mUserIDView.getText().toString();
                if (!userID.isEmpty() && userID.length() == 6) {
                    RetrofitFunction retrofitFunction = new RetrofitFunction(LoginActivity.this, webService.serverURL);
                    retrofitFunction.login(Integer.valueOf(userID));
                }
                attemptLogin();
            }
        });

        final TextView phoneNumInputLengthTrack = (TextView) findViewById(R.id.phoneNumInputLengthTrack);
        mUserIDView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                phoneNumInputLengthTrack.setText(String.valueOf((mUserIDView.length())) + "/6");
            }
        });
        /*tvRegisterLink.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, terms.class));
                //finish();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);*/
        mProgressView = findViewById(R.id.login_progress);

        userLocalStore = new UserLocalStore(this);

    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mUserIDView.setError(null);

        // Store values at the time of the login attempt.
        String user_id = mUserIDView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(user_id) || !isPasswordValid(user_id)) {
            mUserIDView.setError(getString(R.string.error_invalid_password));
            focusView = mUserIDView;
            cancel = true;
        }


        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            //showProgress(true);
            //mAuthTask = new UserLoginTask(user_id);
            //mAuthTask.execute((Void) null);
            mAuthTask = "yes";
            User user = new User(Integer.parseInt(user_id));
            authenticate(user);
        }
    }


    private boolean isPasswordValid(String password) {
        return password.matches("^([0-9]{6,6})$");
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }


    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }


    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

    }


    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }


/*
    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }
*/

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                startActivity(new Intent(this, welcomeActivity.class));
                finish();
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    private void authenticate(User user) {
        ServerRequests serverRequests = new ServerRequests(this);
        serverRequests.fetchUserDataInBackground(user, new GetUserCallback() {
            @Override
            public void done(User returnedUser) {
                mAuthTask = null;
                if (returnedUser == null) {

                    mUserIDView.setError(getString(R.string.error_incorrect_password));
                    mUserIDView.requestFocus();

                } else {
                    logUserIn(returnedUser);
                }
            }
        });


    }

    private void logUserIn(User returnedUser) {

        userLocalStore.storeUserData(returnedUser);
        userLocalStore.setUserLoggedIn(true);


        sync_data_version sdvDB = new sync_data_version(this);
        sdvDB.insert(sdvDB.new sync_data_version_class("EXERCISE_TYPE", 0));
        sdvDB.insert(sdvDB.new sync_data_version_class("FOOD", 0));
        sdvDB.insert(sdvDB.new sync_data_version_class("FOOD_CATEGORIES", 0));
        sdvDB.insert(sdvDB.new sync_data_version_class("FOOD_SUB_CATEGORIES", 0));

        xiaomiDeviceSetting xiaomiDeviceSettingDB = new xiaomiDeviceSetting(this);
        xiaomiDeviceSettingDB.insert(xiaomiDeviceSettingDB.new xiaomiDeviceSetting_class("enabled", "0"));
        xiaomiDeviceSettingDB.insert(xiaomiDeviceSettingDB.new xiaomiDeviceSetting_class("connectionState", "0"));

        Bundle params = new Bundle();
        params.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, false);
        params.putBoolean(ContentResolver.SYNC_EXTRAS_DO_NOT_RETRY, false);
        params.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, false);


        //Load Past Data from Server
        Thread thread = new Thread(importOldData);
        thread.start();

        try {
            thread.join();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Downloading Data
        ProgressDialog dialog = ProgressDialog.show(getThis, "",
                "下載資料中，請稍後...", true);


        //Process
        Thread thread2 = new Thread(downloadBasicData);
        thread2.start();

        try {
            thread2.join();
        } catch (Exception e) {
            e.printStackTrace();
        }


        Account newAccount = new Account(String.valueOf("更新程式"), "com.dmc.myapplication.myacc");
        AccountManager accountManager = (AccountManager) getThis.getSystemService(ACCOUNT_SERVICE);
        accountManager.addAccountExplicitly(newAccount, null, null);
        //ContentResolver.addPeriodicSync(newAccount, "com.dmc.myapplication.provider", params, 5);
        //ContentResolver.setSyncAutomatically(newAccount, "com.dmc.myapplication.provider", true);
        //ContentResolver.requestSync(newAccount,"com.dmc.myapplication.provider",params);
        ContentResolver.setIsSyncable(newAccount, "com.dmc.myapplication.provider", 1);
        //ContentResolver.setIsSyncable(newAccount, "com.dmc.myapplication.provider2", 1);
        ContentResolver.setSyncAutomatically(newAccount, "com.dmc.myapplication.provider", true);
        //ContentResolver.setSyncAutomatically(newAccount, "com.dmc.myapplication.provider2", true);

        Bundle bundle = new Bundle();
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_FORCE, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);

        ContentResolver.addPeriodicSync(
                newAccount,
                "com.dmc.myapplication.provider",
                Bundle.EMPTY,
                (60 * 60 * 12));

            /*ContentResolver.addPeriodicSync(
                    newAccount,
                    "com.dmc.myapplication.provider2",
                    Bundle.EMPTY,
                    30);*/

        dialog.dismiss();

        startActivity(new Intent(getThis, MainActivity.class));
        finish();


    }

    Context getThis = this;

    private Runnable downloadBasicData = new Runnable() {
        @Override
        public void run() {


            ////////////////////////////////


            try {

                System.out.println("System Update Sync Working!");
                checkLatestVersion();


                System.out.println("Sync Working!");

                List<String> serverRequestList = getServerRequestDataTOSendForBodyrecord();
                if (serverRequestList != null && serverRequestList.size() > 0) {
                    sendRequestedDataBodyRecord(serverRequestList);
                }

                serverRequestList = getServerRequestDataTOSendForExercise();
                if (serverRequestList != null && serverRequestList.size() > 0) {
                    sendRequestedDataExercise(serverRequestList);
                }

                serverRequestList = getServerRequestDataTOSendForFood();
                if (serverRequestList != null && serverRequestList.size() > 0) {
                    sendRequestedDataFood(serverRequestList);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


            ////////////////////////////////

        }
    };

    private Runnable importOldData = new Runnable() {
        @Override
        public void run() {
            UserLocalStore userLocalStore = new UserLocalStore(getThis);
            User user = userLocalStore.getLoggedInUser();

            String data = "userId=" + user.userid + "&event=bodyrecord&method=getOldRecord&data=";
            System.out.println(data);
            String pastBodyRecords = webHelper.sendRequest("POST", data);
            Gson gson = new Gson();
            List<HashMap<String, String>> pastBodyRecordsList = gson.fromJson(pastBodyRecords, new TypeToken<List<HashMap<String, String>>>() {
            }.getType());
            System.out.println("size=" + pastBodyRecordsList.size());
            bodyRecord brdb = new bodyRecord(getThis);
            logintime logintime = new logintime(getApplicationContext());
            List<HashMap<String, String>> result = new ArrayList<>();
            foodAssessment foodAssessment = new foodAssessment(getThis);
            foodAssessment.foodAssess_class foodAssess_class = foodAssessment.new foodAssess_class();

            logintime.insert(logintime.new loginrecord_class(0, user.userid, 0));
            rewards rewards = new rewards(getApplicationContext());
            BodyRecord br = new BodyRecord();

            if (pastBodyRecordsList != null) {
                System.out.println("size=" + pastBodyRecordsList.size());


                for (int x = 0; x < pastBodyRecordsList.size(); x++) {
                    BodyRecord.bodyRecord b = br.new bodyRecord(0, Integer.parseInt(pastBodyRecordsList.get(x).get("RECORD_TYPE")), Integer.parseInt(pastBodyRecordsList.get(x).get("USER_ID")),
                            pastBodyRecordsList.get(x).get("DATE"), pastBodyRecordsList.get(x).get("TIME"), Double.parseDouble(pastBodyRecordsList.get(x).get("HEIGHT"))
                            , Double.parseDouble(pastBodyRecordsList.get(x).get("WEIGHT")), Double.parseDouble(pastBodyRecordsList.get(x).get("WAIST")), Double.parseDouble(pastBodyRecordsList.get(x).get("BMI"))
                            , Integer.parseInt(pastBodyRecordsList.get(x).get("BP_H")), Integer.parseInt(pastBodyRecordsList.get(x).get("BP_L")), Integer.parseInt(pastBodyRecordsList.get(x).get("HEART_RATE"))
                            , Double.parseDouble(pastBodyRecordsList.get(x).get("HbA1c")), Integer.parseInt(pastBodyRecordsList.get(x).get("PERIOD")), Integer.parseInt(pastBodyRecordsList.get(x).get("TYPE_GLUCOSE"))
                            , Double.parseDouble(pastBodyRecordsList.get(x).get("GLUCOSE"))
                            , Double.parseDouble(pastBodyRecordsList.get(x).get("TOTAL_C")), Double.parseDouble(pastBodyRecordsList.get(x).get("LDL_C")), Double.parseDouble(pastBodyRecordsList.get(x).get("HDL_C"))
                            , Double.parseDouble(pastBodyRecordsList.get(x).get("TRIGLYCERIDES")), pastBodyRecordsList.get(x).get("REMARKS"));
                    b.create_datetime = pastBodyRecordsList.get(x).get("create_datetime");
                    b.edit_datetime = pastBodyRecordsList.get(x).get("edit_datetime");

                    brdb.insert(b);
                }
            }

            data = "userId=" + user.userid + "&event=exercise&method=getOldRecord&data=";

            pastBodyRecords = webHelper.sendRequest("POST", data);
            pastBodyRecordsList = gson.fromJson(pastBodyRecords, new TypeToken<List<HashMap<String, String>>>() {
            }.getType());
            exercise_type_record etrdb = new exercise_type_record(getThis);
            if (pastBodyRecordsList != null) {

                for (int x = 0; x < pastBodyRecordsList.size(); x++) {
                    exercise_type_record.exercise_type_record_class obj = etrdb.new exercise_type_record_class(0, Integer.parseInt(pastBodyRecordsList.get(x).get("USER_ID")), pastBodyRecordsList.get(x).get("EXERCISE_DATE"), pastBodyRecordsList.get(x).get("EXERCISE_TIME"), Integer.parseInt(pastBodyRecordsList.get(x).get("EXERCISE_TYPE_ID")), Integer.parseInt(pastBodyRecordsList.get(x).get("EXERCISE_PERIOD")));
                    obj.create_datetime = pastBodyRecordsList.get(x).get("create_datetime");
                    obj.edit_datetime = pastBodyRecordsList.get(x).get("edit_datetime");

                    etrdb.insert(obj);
                }
            }

            //navfoodAsyncTask asyncTask = new navfoodAsyncTask(LoginActivity.this, navFoodConstant.GET_FOOD_RECORD);
            //asyncTask.execute(String.valueOf(user.userid));

            // data = "userId=" + user.userid + "&event=food&method=getOldRecord&data=";

            // pastBodyRecords = webHelper.sendRequest("POST", data);
            // pastBodyRecordsList = gson.fromJson(pastBodyRecords, new TypeToken<List<HashMap<String, String>>>() {
            /// }.getType());


            data = "userId=" + user.userid + "&event=step&method=getOldRecord&data=";

            pastBodyRecords = webHelper.sendRequest("POST", data);
            pastBodyRecordsList = gson.fromJson(pastBodyRecords, new TypeToken<List<HashMap<String, String>>>() {
            }.getType());
            stepRecord stepdb = new stepRecord(getThis);
            if (pastBodyRecordsList != null) {

                for (int x = 0; x < pastBodyRecordsList.size(); x++) {
                    stepRecord.stepRecord_class obj = stepdb.new stepRecord_class(0, Integer.parseInt(pastBodyRecordsList.get(x).get("USER_ID")), pastBodyRecordsList.get(x).get("DATE"), Integer.parseInt(pastBodyRecordsList.get(x).get("STEP_COUNT")));
                    obj.create_datetime = pastBodyRecordsList.get(x).get("create_datetime");
                    obj.edit_datetime = pastBodyRecordsList.get(x).get("edit_datetime");
                    stepdb.insert(obj);
                }
            }

            // data = "userId="+user.userid+"&event=foodAssessment&method=getOldRecord&data=";
            // pastBodyRecords = webHelper.sendRequest("POST", data);
            //  pastBodyRecordsList = gson.fromJson(pastBodyRecords, new TypeToken<List<HashMap<String, String>>>(){}.getType());
            //  SimpleDateFormat ss = new SimpleDateFormat("yyyyMMddhhmmssSSS");
            //  String currentTimestamp = ss.format(new Date());

            // if (pastBodyRecordsList != null) {

            //   for (int x = 0; x < pastBodyRecordsList.size(); x++) {
            //     foodAssessment.foodAssess_class obj = foodAssessment.new foodAssess_class(0, user.userid, pastBodyRecordsList.get(x).get("FOOD_ASSESS_DATE"), Float.parseFloat(pastBodyRecordsList.get(x).get("FOOD_RATING")),
            //           pastBodyRecordsList.get(x).get("FOOD_QUESTION_2"), pastBodyRecordsList.get(x).get("FOOD_QUESTION_3a"), pastBodyRecordsList.get(x).get("FOOD_QUESTION_3b1"), pastBodyRecordsList.get(x).get("FOOD_QUESTION_3b2"), pastBodyRecordsList.get(x).get("FOOD_QUESTION_3b3"));
            //   obj.create_datetime = pastBodyRecordsList.get(x).get("create_datetime");
            //   obj.edit_datetime = pastBodyRecordsList.get(x).get("edit_datetime");
            //   foodAssessment.insert(obj, currentTimestamp);
            // }
            // }

            ConnectivityManager cm =
                    (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null &&
                    activeNetwork.isConnectedOrConnecting();
            if (isConnected) {
                System.out.println("select data from rewards");
                navfoodAsyncTask asyncTask = new navfoodAsyncTask(LoginActivity.this, navFoodConstant.SELECT_REWARDS_DATA);
                asyncTask.execute(String.valueOf(user.userid));

            }


        }
    };

    private List<String> getServerRequestDataTOSendForBodyrecord() {

        //Prepare data
        bodyRecord bodyRecordDB = new bodyRecord(getBaseContext());
        List<HashMap<String, String>> datetimeList = bodyRecordDB.getAllCreatedatetimeEditdatetime();
        if (datetimeList.size() <= 0) {
            bodyRecordDB.close();
            return null;
        }
        String datetimeListJson = new Gson().toJson(datetimeList);
        datetimeListJson = URLEncoder.encode(datetimeListJson);
        //System.out.println(datetimeListJson);

        UserLocalStore userLocalStore = new UserLocalStore(getBaseContext());
        User user = userLocalStore.getLoggedInUser();

        // post请求的参数
        String data = "userId=" + user.userid + "&event=bodyrecord&method=compareData&data=" + datetimeListJson;
        String result = webHelper.sendRequest("POST", data);
        Gson gson = new Gson();
        return gson.fromJson(result, new TypeToken<List<String>>() {
        }.getType());
    }

    /**
     * @return
     */
    private List<String> getServerRequestDataTOSendForExercise() {

        //Prepare data
        exercise_type_record exerciseRecordDB = new exercise_type_record(getBaseContext());
        List<HashMap<String, String>> datetimeList = exerciseRecordDB.getAllCreatedatetimeEditdatetime();
        if (datetimeList.size() <= 0) {
            return null;
        }
        String datetimeListJson = new Gson().toJson(datetimeList);
        datetimeListJson = URLEncoder.encode(datetimeListJson);
        //System.out.println(datetimeListJson);

        UserLocalStore userLocalStore = new UserLocalStore(getBaseContext());
        User user = userLocalStore.getLoggedInUser();

        // post请求的参数
        String data = "userId=" + user.userid + "&event=exercise&method=compareData&data=" + datetimeListJson;
        String result = webHelper.sendRequest("POST", data);
        Gson gson = new Gson();
        return gson.fromJson(result, new TypeToken<List<String>>() {
        }.getType());
    }

    private List<String> getServerRequestDataTOSendForFood() {

        //Prepare data
        foodRecord foodRecordDB = new foodRecord(getBaseContext());
        List<HashMap<String, String>> datetimeList = foodRecordDB.getAllCreatedatetimeEditdatetime();
        if (datetimeList.size() <= 0) {
            return null;
        }
        String datetimeListJson = new Gson().toJson(datetimeList);
        datetimeListJson = URLEncoder.encode(datetimeListJson);
        //System.out.println(datetimeListJson);

        UserLocalStore userLocalStore = new UserLocalStore(getBaseContext());
        User user = userLocalStore.getLoggedInUser();

        // post请求的参数
        String data = "userId=" + user.userid + "&event=food&method=compareData&data=" + datetimeListJson;

        String result = webHelper.sendRequest("POST", data);

        Gson gson = new Gson();

        return gson.fromJson(result, new TypeToken<List<String>>() {
        }.getType());

    }

    private void sendRequestedDataBodyRecord(List<String> requestList) {
        bodyRecord bodyRecordDB = new bodyRecord(getBaseContext());
        List<HashMap<String, String>> BRList = bodyRecordDB.getRecordHashMapByCreateDatetime(requestList);
        String BRListString = new Gson().toJson(BRList);
        BRListString = URLEncoder.encode(BRListString);

        UserLocalStore userLocalStore = new UserLocalStore(getBaseContext());
        User user = userLocalStore.getLoggedInUser();

        //Send
        String data = "userId=" + user.userid + "&event=bodyrecord&method=updateRecords&data=" + BRListString;
        String result = webHelper.sendRequest("POST", data);

    }

    private void sendRequestedDataExercise(List<String> requestList) {
        exercise_type_record exercDB = new exercise_type_record(getBaseContext());
        List<HashMap<String, String>> BRList = exercDB.getRecordHashMapByCreateDatetime(requestList);
        String BRListString = new Gson().toJson(BRList);
        BRListString = URLEncoder.encode(BRListString);

        UserLocalStore userLocalStore = new UserLocalStore(getBaseContext());
        User user = userLocalStore.getLoggedInUser();

        //Send
        String data = "userId=" + user.userid + "&event=exercise&method=updateRecords&data=" + BRListString;
        String result = webHelper.sendRequest("POST", data);

    }

    private void sendRequestedDataFood(List<String> requestList) {
        foodRecord exercDB = new foodRecord(getBaseContext());
        List<HashMap<String, String>> BRList = exercDB.getRecordHashMapByCreateDatetime(requestList);
        String BRListString = new Gson().toJson(BRList);
        BRListString = URLEncoder.encode(BRListString);

        UserLocalStore userLocalStore = new UserLocalStore(getBaseContext());
        User user = userLocalStore.getLoggedInUser();

        //Send
        String data = "userId=" + user.userid + "&event=food&method=updateRecords&data=" + BRListString;
        String result = webHelper.sendRequest("POST", data);

    }

    private void checkLatestVersion() {
        UserLocalStore userLocalStore = new UserLocalStore(getBaseContext());
        String responce = webHelper.sendRequest("POST", "userId=" + userLocalStore.getLoggedInUser().userid + "&event=syncdataversion&method=getLatestVersion&data=");
        if (responce != null) {
            Gson gson = new Gson();
            HashMap<String, Integer> latestVersion = gson.fromJson(responce, new TypeToken<HashMap<String, Integer>>() {
            }.getType());
            sync_data_version sdvDB = new sync_data_version(getBaseContext());
            System.out.println("Debug: latestVersion.size = " + latestVersion.size());
            for (int x = 0; x < latestVersion.size(); x++) {
                System.out.println("Debug: latestVersion.keySet().toArray()[x].toString() = " + latestVersion.keySet().toArray()[x].toString());
                System.out.println("Debug: sdvDB.get(latestVersion.keySet().toArray()[x].toString()).getVERSION() = " + sdvDB.get(latestVersion.keySet().toArray()[x].toString()).getVERSION());
                System.out.println("Debug: latestVersion.get(latestVersion.keySet().toArray()[x].toString()) = " + latestVersion.get(latestVersion.keySet().toArray()[x].toString()));

                if (sdvDB.get(latestVersion.keySet().toArray()[x].toString()).getVERSION() != latestVersion.get(latestVersion.keySet().toArray()[x].toString())) {
                    System.out.println("Not match!!");

                    switch (latestVersion.keySet().toArray()[x].toString()) {
                        case "EXERCISE_TYPE":
                            System.out.println("Run EXERCISE_TYPE!");
                            updateExerciseTypeTable("EXERCISE_TYPE", latestVersion.get(latestVersion.keySet().toArray()[x].toString()));
                            break;

                        case "FOOD":
                            System.out.println("Run FOOD!");
                            updateFoodTable("FOOD", latestVersion.get(latestVersion.keySet().toArray()[x].toString()));
                            break;

                        case "FOOD_CATEGORIES":
                            System.out.println("Run FOOD_CATEGORIES!");
                            updateFoodCateTypeTable("FOOD_CATEGORIES", latestVersion.get(latestVersion.keySet().toArray()[x].toString()));
                            break;

                        case "FOOD_SUB_CATEGORIES":
                            System.out.println("Run FOOD_SUB_CATEGORIES!");
                            updateFoodSubCateTypeTable("FOOD_SUB_CATEGORIES", latestVersion.get(latestVersion.keySet().toArray()[x].toString()));
                            break;

                        default:
                            break;
                    }
                }
            }
        }
    }

    private void updateExerciseTypeTable(String TABLE, Integer VERSION) {
        System.out.println("Start update ExerciseTypeTable to version " + VERSION);
        exercise_type etDB = new exercise_type(getBaseContext());
        etDB.deleteAll();

        UserLocalStore userLocalStore = new UserLocalStore(getBaseContext());
        String responce = webHelper.sendRequest("POST", "userId=" + userLocalStore.getLoggedInUser().userid + "&event=syncdataversion&method=getLatestExerciseType&data=");
        Gson gson = new Gson();
        List<HashMap<String, String>> newData = gson.fromJson(responce, new TypeToken<List<HashMap<String, String>>>() {
        }.getType());
        for (int x = 0; x < newData.size(); x++) {
            etDB.insert(etDB.new exercise_type_class(Integer.parseInt(newData.get(x).get("EXERCISE_TYPE_ID")), newData.get(x).get("EXERCISE_TYPE_NAME")));
        }

        //UpdateVersion
        updateLocalVersion(TABLE, VERSION);
    }

    private void updateFoodCateTypeTable(String TABLE, Integer VERSION) {
        System.out.println("Start update FoodCateTypeTable to version " + VERSION);
        foodCategories etDB = new foodCategories(getBaseContext());
        etDB.deleteAll();

        UserLocalStore userLocalStore = new UserLocalStore(getBaseContext());
        String responce = webHelper.sendRequest("POST", "userId=" + userLocalStore.getLoggedInUser().userid + "&event=syncdataversion&method=getLatestFoodCategories&data=");
        Gson gson = new Gson();
        List<HashMap<String, String>> newData = gson.fromJson(responce, new TypeToken<List<HashMap<String, String>>>() {
        }.getType());
        for (int x = 0; x < newData.size(); x++) {
            etDB.insert(etDB.new foodCategories_class(Integer.parseInt(newData.get(x).get("FOOD_CATE_ID")), newData.get(x).get("FOOD_CATE_NAME")));
        }

        etDB.close();
        //UpdateVersion
        updateLocalVersion(TABLE, VERSION);
    }

    private void updateFoodSubCateTypeTable(String TABLE, Integer VERSION) {
        System.out.println("Start update FoodSubCateTypeTable to version " + VERSION);
        foodSubCategories etDB = new foodSubCategories(getBaseContext());
        etDB.deleteAll();

        UserLocalStore userLocalStore = new UserLocalStore(getBaseContext());
        String responce = webHelper.sendRequest("POST", "userId=" + userLocalStore.getLoggedInUser().userid + "&event=syncdataversion&method=getLatestFoodSubCategories&data=");
        Gson gson = new Gson();
        List<HashMap<String, String>> newData = gson.fromJson(responce, new TypeToken<List<HashMap<String, String>>>() {
        }.getType());
        for (int x = 0; x < newData.size(); x++) {
            etDB.insert(etDB.new foodSubCategories_class(Integer.parseInt(newData.get(x).get("FOOD_SUB_CATE_ID")), newData.get(x).get("FOOD_SUB_CATE_NAME"), Integer.parseInt(newData.get(x).get("FOOD_CATE_ID"))));
        }

        etDB.close();
        //UpdateVersion
        updateLocalVersion(TABLE, VERSION);
    }

    private void updateFoodTable(String TABLE, Integer VERSION) {
        System.out.println("Start update FoodTable to version " + VERSION);
        food etDB = new food(getBaseContext());
        etDB.deleteAll();

        UserLocalStore userLocalStore = new UserLocalStore(getBaseContext());
        String responce = webHelper.sendRequest("POST", "userId=" + userLocalStore.getLoggedInUser().userid + "&event=syncdataversion&method=getLatestFood&data=");
        Gson gson = new Gson();
        List<HashMap<String, String>> newData = gson.fromJson(responce, new TypeToken<List<HashMap<String, String>>>() {
        }.getType());
        for (int x = 0; x < newData.size(); x++) {
            etDB.insert(etDB.new food_class(Integer.parseInt(newData.get(x).get("FOOD_ID")), newData.get(x).get("FOOD_NAME"), (newData.get(x).get("FOOD_UNIT")), Double.parseDouble(newData.get(x).get("FOOD_GRAM")), Double.parseDouble(newData.get(x).get("FOOD_CALORIE")), Double.parseDouble(newData.get(x).get("FOOD_CARBOHYDRATE")), Double.parseDouble(newData.get(x).get("FOOD_PROTEIN")), Double.parseDouble(newData.get(x).get("FOOD_FAT")), Double.parseDouble(newData.get(x).get("FOOD_SUGAR")), Double.parseDouble(newData.get(x).get("FOOD_FIBER")), Double.parseDouble(newData.get(x).get("FOOD_CHOLESTEROL")), Double.parseDouble(newData.get(x).get("FOOD_SODIUM")), Integer.parseInt(newData.get(x).get("FOOD_SUB_CATE_ID")), (newData.get(x).get("PHOTO_PATH"))));
        }

        etDB.close();
        //UpdateVersion
        updateLocalVersion(TABLE, VERSION);
    }

    private void updateLocalVersion(String TABLE, Integer VERSION) {
        sync_data_version sdvDB = new sync_data_version(getBaseContext());
        sdvDB.update(sdvDB.new sync_data_version_class(TABLE, VERSION));
        //sdvDB.close();
    }

    private void setHandlerFunction() {
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 0:
                        RealmFunction realmFunction = new RealmFunction(LoginActivity.this);
                        int userID = realmFunction.getUserInfo().getUser_id();

                        RetrofitFunction retrofitFunction = new RetrofitFunction(LoginActivity.this, webService.serverURL);
                        retrofitFunction.getBodyIndexRecords(userID, 1);
                        retrofitFunction.getBloodTestRecords(userID, 2);
                        break;
                }
            }
        };
    }

    public static void sendLoginSuccessMessage() {
        Message message = new Message();
        message.what = 0;
        handler.sendMessage(message);
    }

}

