package com.dmc.myapplication.navFood;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.DialogFragment;
import android.os.Bundle;

import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.dmc.myapplication.Models.foodAssessment;
import com.dmc.myapplication.R;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.network.RetrofitFunction;
import com.dmc.myapplication.network.webService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by KwokSinMan on 9/12/2015.
 */

public class navFoodViewDatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    List<HashMap<String, String>> map = new ArrayList<HashMap<String, String>>();
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String displayDate = ((TextView) getActivity().findViewById(R.id.navFoodViewDate)).getText().toString();
        String [] displayDateArray = displayDate.split("/");
        int yy = Integer.parseInt(displayDateArray[2]);
        int mm = Integer.parseInt(displayDateArray[1]) - 1; // because Jan =0
        int dd = Integer.parseInt(displayDateArray[0]);
        DatePickerDialog dialog =  new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, this, yy,mm,dd);
        dialog.getDatePicker().setMaxDate( Calendar.getInstance().getTimeInMillis());
        return dialog;
    }

    // On the date picker , to confirm the following action
    public void onDateSet(DatePicker view, int yy, int mm, int dd) {
        // set display date
        mm = mm+1; // because Jan =0
        String displayAsDate = dd + "/" + mm + "/" + yy;
        ((TextView) getActivity().findViewById(R.id.navFoodViewDate)).setText(displayAsDate);

        //get food record from DB
        UserLocalStore userLocalStore = new UserLocalStore(getActivity());

        int userId = userLocalStore.getLoggedInUser().userid;
        SimpleDateFormat ss = new SimpleDateFormat("yyyy-MM-dd");
        Date d = null;

//        try {
  //          d = ss.parse(new navFoodTime().getDBDateFormal(displayAsDate));
    //    } catch (ParseException e) {
      //      e.printStackTrace();
      //  }
        RetrofitFunction function = new RetrofitFunction(getActivity(), webService.serverURL);
        function.getFoodRecord(userId, new navFoodTime().getDBDateFormal(displayAsDate));

        foodAssessment foodAssessment = new foodAssessment(getActivity());
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");

        try {
            List<String> strings =  new ArrayList<String>();
            d = s.parse(new navFoodTime().getDBDateFormal(displayAsDate));
            ss = new SimpleDateFormat("yyyyMMddhhmmssSSS");
            String currentTimestamp = ss.format(d);
            System.out.println(currentTimestamp.substring(0,8));
            strings.add(currentTimestamp.substring(0,8));

            map = foodAssessment.getRecordHashMapByCreateDatetime(strings);
            navstoreMapClass mapClass = new navstoreMapClass();
            mapClass.setMap(map);
            System.out.println("RETRIEVED: " +  mapClass.getMap().get(0).get("FOOD_EMPTY"));
            if (map.get(0).get("FOOD_EMPTY").equals("1")){
                System.out.println("rating his: " + map.get(0).get("FOOD_RATING"));
                System.out.println("Q2 his: " + map.get(0).get("FOOD_Q2"));
                System.out.println("Q3a his: " + map.get(0).get("FOOD_Q3"));
                System.out.println("Q3b1 his: " + map.get(0).get("FOOD_Q3B1"));
                System.out.println("Q3b2 his: " + map.get(0).get("FOOD_Q3B2"));
                System.out.println("Q3b3 his: " + map.get(0).get("FOOD_Q3B3"));
                ((Button) getActivity().findViewById(R.id.do_assessment)).setEnabled(true);

                ((Button) getActivity().findViewById(R.id.do_assessment)).setText(getString(R.string.title_activity_nav_food_test_review));
            } else {
                String now = ss.format(new Date());
                ConnectivityManager cm =
                        (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null &&
                        activeNetwork.isConnectedOrConnecting();
                if (isConnected) {
                    navfoodAsyncTask asyncTask = new navfoodAsyncTask(getActivity(), navFoodConstant.GET_FOOD_ASSESSMENT);
                    asyncTask.execute(String.valueOf(userId), new navFoodTime().getDBDateFormal(displayAsDate));
                } else {
                     // TODO: only if current date is today, allow user do assessment
                      if (currentTimestamp.substring(0, 8).equals(now.substring(0, 8))) {
                          ((Button) getActivity().findViewById(R.id.do_assessment)).setEnabled(true);
                          ((Button) getActivity().findViewById(R.id.do_assessment)).setText(getString(R.string.title_activity_nav_food_test));
                        } else {
                        ((Button) getActivity().findViewById(R.id.do_assessment)).setEnabled(false);
                     }
                }
             }
         } catch (ParseException e) {
           e.printStackTrace();
         }
    }


}
