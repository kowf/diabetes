package com.dmc.myapplication.navFood;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Base64;

import com.dmc.myapplication.systemConstant;

import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.util.ArrayList;


import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.client.HttpClient;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;


/**
 * Created by KwokSinMan on 12/3/2016.
 */
public class navFoodUploadImageAsyncTask extends AsyncTask<Void,Void,Void> {
    Bitmap image;
    String name;

    public navFoodUploadImageAsyncTask(Bitmap image, String name){
        this.image = image;
        this.name = name;
    }

    @Override
    protected Void doInBackground(Void... params){
        //System.out.println("Candy has run");
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, systemConstant.IMAGE_COMPRESS, stream);
        String reducedImage = Base64.encodeToString(stream.toByteArray(), Base64.DEFAULT);

        //System.out.println("reducedImage="+reducedImage);


        ArrayList<NameValuePair> dataToSend = new ArrayList<>();
        dataToSend.add(new BasicNameValuePair("image",reducedImage));
        dataToSend.add(new BasicNameValuePair("name", name));

        HttpClient client;
        if(!systemConstant.SERVER_ADDRESS.contains("https")){ // run http
            HttpParams httpRequestParams = getHttpRequestParams();
            client = new DefaultHttpClient(httpRequestParams);
        }else{ // run https
            client = trustAllHosts();
        }

        HttpPost post = new HttpPost(navFoodConstant.GET_ADD_FOOD_IMAGE);

        try{
            post.setEntity(new UrlEncodedFormEntity(dataToSend));
            client.execute(post);

        }catch (Exception e ){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid){
        super.onPostExecute(aVoid);
        //System.out.println("Image upload Done.");
    }

    private HttpParams getHttpRequestParams(){
        HttpParams httpRequestParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpRequestParams, systemConstant.CONNECTION_TIMEOUT);
        HttpConnectionParams.setSoTimeout(httpRequestParams,systemConstant.CONNECTION_TIMEOUT);
        return httpRequestParams;
    }

    private HttpClient trustAllHosts() {
        // Create a trust manager that does not validate certificate chains
        try {

            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);
            SSLSocketFactory sf = new MySSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);
            HttpConnectionParams.setConnectionTimeout(params, systemConstant.CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(params, systemConstant.CONNECTION_TIMEOUT);

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));

            ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);
            return new DefaultHttpClient(ccm, params);

        }catch (Exception e){
            e.printStackTrace();
            return new DefaultHttpClient();
        }
    }

    private class MySSLSocketFactory extends SSLSocketFactory {
        SSLContext sslContext = SSLContext.getInstance("TLS");

        public MySSLSocketFactory(KeyStore truststore) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException {
            super(truststore);

            TrustManager tm = new X509TrustManager() {
                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            };

            sslContext.init(null, new TrustManager[] { tm }, null);
        }

        @Override
        public Socket createSocket(Socket socket, String host, int port, boolean autoClose) throws IOException, UnknownHostException {
            return sslContext.getSocketFactory().createSocket(socket, host, port, autoClose);
        }

        @Override
        public Socket createSocket() throws IOException {
            return sslContext.getSocketFactory().createSocket();
        }
    }
}
