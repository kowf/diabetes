package com.dmc.myapplication.navFood;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.CheckedTextView;
import android.widget.Toast;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.R;
import com.dmc.myapplication.systemConstant;

import java.util.ArrayList;
/**
 * Created by lamivan on 17/12/2017.
 */

public class navFoodTestActivity extends AppCompatActivity {
  String question2answer = "";
  String question2his = "";
  String question3ahis = "";
  String question3b1his = "";
  String question3b2his = "";
  String question3b3his = "";
  String question2hisother = "";
  String[] question2hiss;
  String[] question2hissother;
  ArrayList<String> question2hist = new ArrayList<>();
  float ratings = 0;
  float ratinghistory = 0;
   ArrayList<Integer> selectedOption = new ArrayList<>();
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_nav_food_test);
    //ActionBar actionBar = getSupportActionBar();
    //actionBar.setDisplayHomeAsUpEnabled(true);
    //actionBar.setHomeButtonEnabled(true);

    final String getFoodDate = getIntent().getExtras().getString(navFoodConstant.GET_FOOD_DATE);
    final TextView date = (TextView)findViewById(R.id.date);
    final boolean review = getIntent().getExtras().getBoolean("test");
    System.out.println("read only? :" + !review);

    if (!review) {
      ratinghistory = getIntent().getFloatExtra("rating", 0);
      question2his = getIntent().getStringExtra("Q2");
      question3ahis = getIntent().getStringExtra("Q3a");
      question3b1his = getIntent().getStringExtra("Q3b1");
      question3b2his = getIntent().getStringExtra("Q3b2");
      question3b3his = getIntent().getStringExtra("Q3b3");
    }
    date.setText(new navFoodTime().getUIDateFormal(getFoodDate));
    RatingBar ratingBar = (RatingBar) findViewById(R.id.ratingBar);
    if (review) {
      ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
        @Override
        public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
          System.out.println("rating: " + rating);
          ratings = rating;
        }
      });
    } else {
      ratingBar.setEnabled(false);
      ratingBar.setIsIndicator(true);
      ratingBar.setRating(ratinghistory);
      if (question2his.contains(",")) {
        question2hiss = question2his.split(",");
        for (String question2his1 : question2hiss) {
          if (question2his1.contains(":")) {
            question2hissother = question2his1.split(":");
            question2hist.add(question2hissother[0]);
            question2hisother = question2hissother[1];
          } else {
            question2hist.add(question2his1);
          }
        }
        System.out.println(question2hist.size());
      }

    }
      final CheckedTextView option1 = (CheckedTextView) findViewById(R.id.checked2a);

      final CheckedTextView option2 = (CheckedTextView) findViewById(R.id.checked2b);

      final CheckedTextView option3 = (CheckedTextView) findViewById(R.id.checked2c);

      final CheckedTextView option4 = (CheckedTextView) findViewById(R.id.checked2d);

      final CheckedTextView option5 = (CheckedTextView) findViewById(R.id.checked2e);

      final CheckedTextView option6 = (CheckedTextView) findViewById(R.id.checked2f);

      final CheckedTextView option7 = (CheckedTextView) findViewById(R.id.checked2g);

      final CheckedTextView option8 = (CheckedTextView) findViewById(R.id.checked2h);

      final CheckedTextView option9 = (CheckedTextView) findViewById(R.id.checked2i);

      final CheckedTextView option11 = (CheckedTextView) findViewById(R.id.checked2k);

      final CheckedTextView option12 = (CheckedTextView) findViewById(R.id.checked2l);
     final EditText editText = (EditText) findViewById(R.id.others);

    final CheckedTextView[] checkbox = {option1, option2, option3, option4, option5, option6
            , option7, option8, option9, option11, option12};
    editText.setText("");
    if (review) {
      for (int i = 0; i < checkbox.length; i++) {
        final int k = i;
        checkbox[i].setChecked(false);
        checkbox[i].setCheckMarkDrawable(null);
        checkbox[i].setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            if (!checkbox[k].isChecked()) {
              checkbox[k].setChecked(true);
              if (k == 10) {
                editText.setEnabled(true);
                editText.setHint("請輸入原因，十五個字內");
              }
              checkbox[k].setCheckMarkDrawable(R.drawable.checked);
            } else {
              checkbox[k].setChecked(false);
              if (k == 10) {
                editText.setEnabled(false);
                editText.setHint("");
              }
              checkbox[k].setCheckMarkDrawable(null);
            }
          }
        });
      }
    } else {
      for (CheckedTextView aCheckbox : checkbox) {
        aCheckbox.setEnabled(false);
        aCheckbox.setChecked(false);
        aCheckbox.setCheckMarkDrawable(null);
      }
      editText.setEnabled(false);
      editText.setVisibility(View.VISIBLE);
      editText.setText(question2hisother);
        if (question2his.contains(",")) {
          for (int s = 0; s < question2hist.size(); s++) {
            System.out.println(Integer.parseInt(question2hist.get(s))-1);
            checkbox[Integer.parseInt(question2hist.get(s))-1].setChecked(true);
            checkbox[Integer.parseInt(question2hist.get(s))-1].setCheckMarkDrawable(R.drawable.checked);
          }

      } else {
          checkbox[Integer.parseInt(question2hist.get(0))-1].setChecked(true);
          checkbox[Integer.parseInt(question2hist.get(0))-1].setCheckMarkDrawable(R.drawable.checked);
        }
    }
    Button continues = (Button) findViewById(R.id.continu);
    continues.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent = new Intent(navFoodTestActivity.this, navFoodTest2Activity.class);
        if (review) {
          selectedOption = new ArrayList<Integer>();
          for (int i = 0; i < checkbox.length; i++) {
            if (checkbox[i].isChecked()) {
              int k = 0;
              k = i + 1;
              selectedOption.add(k);
            }
          }
          question2answer = "";
          for (int i = 0; i < selectedOption.size(); i++) {
            if (i != selectedOption.size() - 1) {
              question2answer += selectedOption.get(i).toString() + ",";
            } else {
              question2answer += selectedOption.get(i).toString();
            }
          }
          if (!editText.getText().toString().equals("") && editText.isEnabled()) {
            question2answer += ":" + editText.getText().toString();
          }
          System.out.println(question2answer);
          intent.putExtra("rating", ratings);
          intent.putExtra("question2result", question2answer);
        } else {
          intent.putExtra("Q3", question3ahis);
          intent.putExtra("Q3b1", question3b1his);
          intent.putExtra("Q3b2", question3b2his);
          intent.putExtra("Q3b3", question3b3his);
        }

        intent.putExtra("review", review);
        intent.putExtra(navFoodConstant.GET_FOOD_DATE, getFoodDate );
        if ((review && editText.getText().length() < 15) || !review) {
          startActivity(intent);
          finish();
        } else {
          Toast.makeText(getApplicationContext(), "其他原因須於15字之內", Toast.LENGTH_LONG).show();
        }
      }
    });
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        handleBackAction();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }@Override
  public void onBackPressed() {
    handleBackAction();
  }

  private void handleBackAction(){
    String getFoodDate = getIntent().getExtras().getString(navFoodConstant.GET_FOOD_DATE);
    Intent intent = new Intent();
    intent.setClass(this, MainActivity.class);
    intent.putExtra(navFoodConstant.GET_FOOD_DATE, getFoodDate);
    intent.putExtra(systemConstant.REDIRECT_PAGE ,systemConstant.DISPLAY_NAVFOOD);
    startActivity(intent);
    overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
    finish();
  }
}
