package com.dmc.myapplication.navFood;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;


import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.R;

import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.systemConstant;

public class navFoodPieChartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navfood_pie_chart);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String foodDate = getIntent().getExtras().getString(navFoodConstant.GET_FOOD_DATE);
     /*
        navFoodTime foodDateTool = new navFoodTime();
        TextView foodDateDisplay = (TextView) findViewById(R.id.navFoodPieChartDate);
        foodDateDisplay.setText("日期: " +foodDateTool.getUIDateFormal(foodDate));
        */
        UserLocalStore userLocalStore = new UserLocalStore(this);
        String userId = Integer.toString(userLocalStore.getLoggedInUser().userid);
        navfoodAsyncTask connect = new navfoodAsyncTask(this, navFoodConstant.GET_FOOD_PIE_CHART_DATA);
        connect.execute(userId,foodDate);

        // SET return <- into left hand side
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public void onBackPressed() {
        handleBackAction();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() ==  android.R.id.home){
            handleBackAction();
        }
        return true;
    }

    private void handleBackAction(){
        String foodDate = getIntent().getExtras().getString(navFoodConstant.GET_FOOD_DATE);
        Intent intent = new Intent();
        intent.setClass(this, MainActivity.class);
        intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_NAVFOOD);
        intent.putExtra(navFoodConstant.GET_FOOD_DATE,foodDate);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        finish();
    }

}
