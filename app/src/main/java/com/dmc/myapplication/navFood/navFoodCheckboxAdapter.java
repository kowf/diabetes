package com.dmc.myapplication.navFood;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.dmc.myapplication.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lamivan on 13/11/2017.
 */

public class navFoodCheckboxAdapter extends ArrayAdapter<navFoodTextCheckboxState> {
  private Context mContext;
  private ArrayList<navFoodTextCheckboxState> listState;
  private navFoodCheckboxAdapter myAdapter;
  private boolean isFromView = false;

  public navFoodCheckboxAdapter(Context context, int resource, List<navFoodTextCheckboxState> objects) {
    super(context, resource, objects);
    this.mContext = context;
    this.listState = (ArrayList<navFoodTextCheckboxState>) objects;
    this.myAdapter = this;
  }

  @Override
  public View getDropDownView(int position, View convertView,
                              ViewGroup parent) {
    return getCustomView(position, convertView, parent);
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    return getCustomView(position, convertView, parent);
  }

  public View getCustomView(final int position, View convertView,
                            ViewGroup parent) {

    final ViewHolder holder;
    if (convertView == null) {
      LayoutInflater layoutInflator = LayoutInflater.from(mContext);
      convertView = layoutInflator.inflate(R.layout.spinner_item, null);
      holder = new ViewHolder();
      holder.mTextView = (TextView) convertView
              .findViewById(R.id.text);
      holder.mCheckBox = (CheckBox) convertView
              .findViewById(R.id.checkbox);
      convertView.setTag(holder);
    } else {
      holder = (ViewHolder) convertView.getTag();
    }

    holder.mTextView.setText(listState.get(position).getTitle());

    // To check weather checked event fire from getview() or user input
    isFromView = true;
    holder.mCheckBox.setChecked(listState.get(position).isSelected());
    isFromView = false;

    if ((position == 0)) {
      holder.mCheckBox.setVisibility(View.INVISIBLE);
    } else {
      holder.mCheckBox.setVisibility(View.VISIBLE);
    }
    holder.mCheckBox.setTag(position);
    holder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int getPosition = (Integer) buttonView.getTag();
        listState.get(getPosition).setSelected(true);

      }
    });
    return convertView;
  }

  private class ViewHolder {
    private TextView mTextView;
    private CheckBox mCheckBox;
  }
}

