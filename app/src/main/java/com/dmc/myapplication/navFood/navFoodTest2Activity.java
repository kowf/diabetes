package com.dmc.myapplication.navFood;

import android.content.ContentResolver;

import com.dmc.myapplication.Models.rewards;
import com.dmc.myapplication.network.RetrofitFunction;
import com.dmc.myapplication.network.webService;
import com.dmc.myapplication.otherBodyIndex.otherBodyMeasure_add;
import com.dmc.myapplication.tool.dateTool;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.Models.foodAssessment;
import com.dmc.myapplication.R;
import com.dmc.myapplication.login.User;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.sync.SyncAdapter;
import com.dmc.myapplication.systemConstant;
import com.dmc.myapplication.webHelper;

import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

/**
 * Created by lamivan on 17/12/2017.
 */

public class navFoodTest2Activity extends AppCompatActivity {
  String question3aanswer = "";
  String question3b1answer = "";
  String question3b2answer = "";
  String question3b3answer = "";
  String question3ahis = "";
  String question3b1his = "";
  String question3b2his = "";
  String question3b3his = "";
  ArrayList<String> question3ahist = new ArrayList<>();
  String question3ahisother = "";
  String[] question3ahiss;
  String[] question3b1hiss;
  String[] question3b2hiss;
  String[] question3b3hiss;
  String[] question3b1hisscomma;
  String[] question3ahissother;
  private static Handler handler;

  ArrayList<Integer> selectedOption = new ArrayList<>();
  final ArrayList<EditText> question3b1 = new ArrayList<>();
  final ArrayList<EditText> question3b2 = new ArrayList<>();
  final ArrayList<EditText> question3b3 = new ArrayList<>();
  private static int copyButtonFreq = 0;
  private static int copyButtonFreq2 = 0;
  private static int copyButtonFreq3 = 0;
  LinearLayout layout1, layout2, layout3, layout4, layout5, layout6, layout7,
    layout8, layout9, layout10, layout11, layout12, layout13, layout14,
    layout15, layout16, layout17, layout18, layout19;
  private static int viewsCount = 0;
  private static int viewsCount2 = 999;
  private ArrayList<CheckedTextView> checkedTextViews = new ArrayList<>();
  private ArrayList<LinearLayout> inearLayout = new ArrayList<>();
  private ArrayList<LinearLayout.LayoutParams> arams = new ArrayList<>();
  private List<View> allViews = new ArrayList<View>();
  LinearLayout.LayoutParams params, params2, params3, params4, params5,
   params6, params7, params8, params9, params10, params11, params12, params13, params14,
          params15, params16, params17, params18, params19;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.activity_nav_food_test_test);
    setHandlerFunction();

    final boolean review = getIntent().getExtras().getBoolean("review");
    System.out.println("read only? :" + !review);
    final String date = getIntent().getStringExtra(navFoodConstant.GET_FOOD_DATE);
    System.out.println(date);
    final CheckedTextView option1 = (CheckedTextView) findViewById(R.id.checked3a);

    final CheckedTextView option2 = (CheckedTextView) findViewById(R.id.checked3b);

    final CheckedTextView option3 = (CheckedTextView) findViewById(R.id.checked3c);

    final CheckedTextView option4 = (CheckedTextView) findViewById(R.id.checked3d);

    final CheckedTextView option5 = (CheckedTextView) findViewById(R.id.checked3e);

    final CheckedTextView option6 = (CheckedTextView) findViewById(R.id.checked3f);

    final CheckedTextView option7 = (CheckedTextView) findViewById(R.id.checked3g);

    final CheckedTextView option8 = (CheckedTextView) findViewById(R.id.checked3h);

    final CheckedTextView option9 = (CheckedTextView) findViewById(R.id.checked3i);

    final CheckedTextView option10 = (CheckedTextView) findViewById(R.id.checked3j);

    final CheckedTextView option11 = (CheckedTextView) findViewById(R.id.checked3k);
    final EditText editText = (EditText) findViewById(R.id.others);

    // LinearLayout acts as parent Layout , we will add child Views to this Layout dynamically
    layout1 = (LinearLayout) findViewById(R.id.layout1);

    // To set Margin for the child Views
    params = new LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT, WRAP_CONTENT);

    params.setMargins(17, 11, 5, 5);
    // TODO: QUESTION3B1 Container
    layout4 = new LinearLayout(this);
    layout4.setOrientation(LinearLayout.VERTICAL);
    params4 = new LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT, WRAP_CONTENT);

    params4.setMargins(17, 11, 5, 5);
    // TODO: QUESTION3B1 #1
    layout2 = new LinearLayout(this);

    params2 = new LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT, WRAP_CONTENT);

    params2.setMargins(17, 11, 5, 5);

    // TODO: QUESTION3B1 #2
    layout5 = new LinearLayout(this);

    params5 = new LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT, WRAP_CONTENT);

    params5.setMargins(17, 11, 5, 5);

    // TODO: QUESTION3B1 #3
    layout7 = new LinearLayout(this);

    params7 = new LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT, WRAP_CONTENT);

    params7.setMargins(17, 11, 5, 5);

    // TODO: QUESTION3B1 #4
    layout8 = new LinearLayout(this);

    params8 = new LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT, WRAP_CONTENT);

    params8.setMargins(17, 11, 5, 5);

    // TODO: QUESTION3B1 #5
    layout9 = new LinearLayout(this);

    params9 = new LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT, WRAP_CONTENT);

    params9.setMargins(17, 11, 5, 5);

    // TODO: QUESTION3B2 Container
    layout3 = new LinearLayout(this);
    layout3.setOrientation(LinearLayout.VERTICAL);
    params3 = new LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT, WRAP_CONTENT);

    params3.setMargins(17, 11, 5, 5);

    // TODO: QUESTION3B2 #1
    layout10 = new LinearLayout(this);

    params10 = new LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT, WRAP_CONTENT);

    params10.setMargins(17, 11, 5, 5);

    // TODO: QUESTION3B2 #2
    layout11 = new LinearLayout(this);

    params11 = new LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT, WRAP_CONTENT);

    params11.setMargins(17, 11, 5, 5);

    // TODO: QUESTION3B2 #3
    layout12 = new LinearLayout(this);

    params12 = new LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT, WRAP_CONTENT);

    params12.setMargins(17, 11, 5, 5);

    // TODO: QUESTION3B2 #4
    layout13 = new LinearLayout(this);

    params13 = new LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT, WRAP_CONTENT);

    params13.setMargins(17, 11, 5, 5);

    // TODO: QUESTION3B2 #5
    layout14 = new LinearLayout(this);

    params14 = new LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT, WRAP_CONTENT);

    params14.setMargins(17, 11, 5, 5);

    // TODO: QUESTION3B3 Container
    layout6 = new LinearLayout(this);
    layout6.setOrientation(LinearLayout.VERTICAL);
    params6 = new LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT, WRAP_CONTENT);

    params6.setMargins(17, 11, 5, 5);

    // TODO: QUESTION3B3 #1
    layout15 = new LinearLayout(this);

    params15 = new LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT, WRAP_CONTENT);

    params15.setMargins(17, 11, 5, 5);

    // TODO: QUESTION3B3 #2
    layout16 = new LinearLayout(this);

    params16 = new LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT, WRAP_CONTENT);

    params16.setMargins(17, 11, 5, 5);

    // TODO: QUESTION3B3 #3
    layout17 = new LinearLayout(this);

    params17 = new LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT, WRAP_CONTENT);

    params17.setMargins(17, 11, 5, 5);

    // TODO: QUESTION3B3 #4
    layout18 = new LinearLayout(this);

    params18 = new LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT, WRAP_CONTENT);

    params18.setMargins(17, 11, 5, 5);

    // TODO: QUESTION3B3 #5
    layout19 = new LinearLayout(this);

    params19 = new LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT, WRAP_CONTENT);

    params19.setMargins(17, 11, 5, 5);
    inearLayout.add(layout1);
    inearLayout.add(layout2);
    inearLayout.add(layout3);
    inearLayout.add(layout4);
    inearLayout.add(layout5);
    inearLayout.add(layout6);
    inearLayout.add(layout7);
    inearLayout.add(layout8);
    inearLayout.add(layout9);
    inearLayout.add(layout10);
    inearLayout.add(layout11);
    inearLayout.add(layout12);
    inearLayout.add(layout13);
    inearLayout.add(layout14);
    inearLayout.add(layout15);
    inearLayout.add(layout16);
    inearLayout.add(layout17);
    inearLayout.add(layout18);
    inearLayout.add(layout19);
    arams.add(params);
    arams.add(params2);
    arams.add(params3);
    arams.add(params4);
    arams.add(params5);
    arams.add(params6);
    arams.add(params7);
    arams.add(params8);
    arams.add(params9);
    arams.add(params10);
    arams.add(params11);
    arams.add(params12);
    arams.add(params13);
    arams.add(params14);
    arams.add(params15);
    arams.add(params16);
    arams.add(params17);
    arams.add(params18);
    arams.add(params19);
    // create checkbox dynamically , added to LinearLayout
    // createTextView(getString(R.string.question3a), 1);

    final CheckedTextView[] checkbox = {option1, option2, option3, option4, option5, option6
            , option7, option8, option9, option10, option11};
    if (!review) {
      question3ahis = getIntent().getStringExtra("Q3");
      question3b1his = getIntent().getStringExtra("Q3b1");
      question3b2his = getIntent().getStringExtra("Q3b2");
      question3b3his = getIntent().getStringExtra("Q3b3");
      if (question3ahis.contains(",")) {
        question3ahiss = question3ahis.split(",");
        for (String question2his1 : question3ahiss) {
          if (question2his1.contains(":")) {
            question3ahissother = question2his1.split(":");
            question3ahist.add(question3ahissother[0]);
            question3ahisother = question3ahissother[1];
          } else {
            question3ahist.add(question2his1);
          }
        }
        System.out.println(question3ahist.size());
      }
      for (CheckedTextView aCheckbox : checkbox) {
        aCheckbox.setEnabled(false);
        aCheckbox.setChecked(false);
        aCheckbox.setCheckMarkDrawable(null);
      }
      editText.setEnabled(false);
      editText.setVisibility(View.VISIBLE);
      editText.setText(question3ahisother);
      if (question3ahis.contains(",")) {
        for (int s = 0; s < question3ahist.size(); s++) {
          System.out.println(Integer.parseInt(question3ahist.get(s))-1);
          checkbox[Integer.parseInt(question3ahist.get(s))-1].setChecked(true);
          checkbox[Integer.parseInt(question3ahist.get(s))-1].setCheckMarkDrawable(R.drawable.checked);
        }
      } else {
        checkbox[Integer.parseInt(question3ahist.get(0))-1].setChecked(true);
        checkbox[Integer.parseInt(question3ahist.get(0))-1].setCheckMarkDrawable(R.drawable.checked);
      }
    } else {

      for (int i = 0; i < checkbox.length; i++) {
        final int k = i;
        checkbox[i].setChecked(false);
        checkbox[i].setCheckMarkDrawable(null);
        checkbox[i].setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            if (!checkbox[k].isChecked()) {
              checkbox[k].setChecked(true);
              if (k == 10) {
                editText.setEnabled(true);
                editText.setHint("請輸入原因，十五個字內");
              }
              checkbox[k].setCheckMarkDrawable(R.drawable.checked);
            } else {
              checkbox[k].setChecked(false);
              if (k == 10) {
                editText.setEnabled(false);
                editText.setHint("");
              }
              checkbox[k].setCheckMarkDrawable(null);
            }
          }
        });
      }
    }

      createTextView(getString(R.string.question3b), 1);
      layout1.addView(layout4, params4);
      createTextView(getString(R.string.option31), 4);
      layout4.addView(layout2, params2);
      layout4.addView(layout5, params5);
      layout4.addView(layout7, params7);
      layout4.addView(layout8, params8);
      layout4.addView(layout9, params9);
      EditText editText3b1a1 = createEditText("新食物1", 2);
      EditText editText3b1b1 = createEditText("舊食物1", 2);
      question3b1.add(editText3b1a1);
      question3b1.add(editText3b1b1);
      if (review) {
        copyButton();
      } else {

        if (!question3b1his.contains(",")) {
          question3b1hiss = question3b1his.split(":");
          editText3b1a1.setEnabled(false);
          editText3b1b1.setEnabled(false);
          if (question3b1hiss.length != 0) {
            editText3b1a1.setText(question3b1hiss[0]);
            editText3b1b1.setText(question3b1hiss[1]);
          }
        } else {
          question3b1hisscomma = question3b1his.split(",");
          switch (question3b1hisscomma.length){
            case 2:
              question3b1hiss = question3b1hisscomma[0].split(":");
              editText3b1a1.setEnabled(false);
              editText3b1b1.setEnabled(false);
              if (question3b1hiss.length != 0) {
                editText3b1a1.setText(question3b1hiss[0]);
                editText3b1b1.setText(question3b1hiss[1]);
              }
              EditText editText3b1a2 = createEditText("新食物2",5);
              EditText editText3b1b2 = createEditText("舊食物2",5);
              question3b1.add(editText3b1a2);
              question3b1.add(editText3b1b2);
              question3b1hiss = question3b1hisscomma[1].split(":");
              editText3b1a2.setEnabled(false);
              editText3b1b2.setEnabled(false);
              if (question3b1hiss.length != 0) {
                editText3b1a2.setText(question3b1hiss[0]);
                editText3b1b2.setText(question3b1hiss[1]);
              }
              break;
            case 3:
              question3b1hiss = question3b1hisscomma[0].split(":");
              editText3b1a1.setEnabled(false);
              editText3b1b1.setEnabled(false);
              if (question3b1hiss.length != 0) {
                editText3b1a1.setText(question3b1hiss[0]);
                editText3b1b1.setText(question3b1hiss[1]);
              }
              editText3b1a2 = createEditText("新食物2",5);
              editText3b1b2 = createEditText("舊食物2",5);
              question3b1.add(editText3b1a2);
              question3b1.add(editText3b1b2);
              question3b1hiss = question3b1hisscomma[1].split(":");
              editText3b1a2.setEnabled(false);
              editText3b1b2.setEnabled(false);
              if (question3b1hiss.length != 0) {
                editText3b1a2.setText(question3b1hiss[0]);
                editText3b1b2.setText(question3b1hiss[1]);
              }
              EditText editText3b1a3 = createEditText("新食物3",7);
              EditText editText3b1b3 = createEditText("舊食物3",7);
              question3b1.add(editText3b1a3);
              question3b1.add(editText3b1b3);
              question3b1hiss = question3b1hisscomma[2].split(":");
              editText3b1a3.setEnabled(false);
              editText3b1b3.setEnabled(false);
              if (question3b1hiss.length != 0) {
                editText3b1a3.setText(question3b1hiss[0]);
                editText3b1b3.setText(question3b1hiss[1]);
              }
              break;
            case 4:
              question3b1hiss = question3b1hisscomma[0].split(":");
              editText3b1a1.setEnabled(false);
              editText3b1b1.setEnabled(false);
              if (question3b1hiss.length != 0) {
                editText3b1a1.setText(question3b1hiss[0]);
                editText3b1b1.setText(question3b1hiss[1]);
              }
              editText3b1a2 = createEditText("新食物2",5);
              editText3b1b2 = createEditText("舊食物2",5);
              question3b1.add(editText3b1a2);
              question3b1.add(editText3b1b2);
              question3b1hiss = question3b1hisscomma[1].split(":");
              editText3b1a2.setEnabled(false);
              editText3b1b2.setEnabled(false);
              if (question3b1hiss.length != 0) {
                editText3b1a2.setText(question3b1hiss[0]);
                editText3b1b2.setText(question3b1hiss[1]);
              }
              editText3b1a3 = createEditText("新食物3",7);
              editText3b1b3 = createEditText("舊食物3",7);
              question3b1.add(editText3b1a3);
              question3b1.add(editText3b1b3);
              question3b1hiss = question3b1hisscomma[2].split(":");
              editText3b1a3.setEnabled(false);
              editText3b1b3.setEnabled(false);
              if (question3b1hiss.length != 0) {
                editText3b1a3.setText(question3b1hiss[0]);
                editText3b1b3.setText(question3b1hiss[1]);
              }
              EditText editText3b1a4 = createEditText("新食物4",8);
              EditText editText3b1b4 = createEditText("舊食物4",8);
              question3b1.add(editText3b1a4);
              question3b1.add(editText3b1b4);
              question3b1hiss = question3b1hisscomma[3].split(":");
              editText3b1a4.setEnabled(false);
              editText3b1b4.setEnabled(false);
              if (question3b1hiss.length != 0) {
                editText3b1a4.setText(question3b1hiss[0]);
                editText3b1b4.setText(question3b1hiss[1]);
              }
              break;
            case 5:
              question3b1hiss = question3b1hisscomma[0].split(":");
              editText3b1a1.setEnabled(false);
              editText3b1b1.setEnabled(false);
              if (question3b1hiss.length != 0) {
                editText3b1a1.setText(question3b1hiss[0]);
                editText3b1b1.setText(question3b1hiss[1]);
              }
              editText3b1a2 = createEditText("新食物2",5);
              editText3b1b2 = createEditText("舊食物2",5);
              question3b1.add(editText3b1a2);
              question3b1.add(editText3b1b2);
              question3b1hiss = question3b1hisscomma[1].split(":");
              editText3b1a2.setEnabled(false);
              editText3b1b2.setEnabled(false);
              if (question3b1hiss.length != 0) {
                editText3b1a2.setText(question3b1hiss[0]);
                editText3b1b2.setText(question3b1hiss[1]);
              }
              editText3b1a3 = createEditText("新食物3",7);
              editText3b1b3 = createEditText("舊食物3",7);
              question3b1.add(editText3b1a3);
              question3b1.add(editText3b1b3);
              question3b1hiss = question3b1hisscomma[2].split(":");
              editText3b1a3.setEnabled(false);
              editText3b1b3.setEnabled(false);
              if (question3b1hiss.length != 0) {
                editText3b1a3.setText(question3b1hiss[0]);
                editText3b1b3.setText(question3b1hiss[1]);
              }
              editText3b1a4 = createEditText("新食物4",8);
              editText3b1b4 = createEditText("舊食物4",8);
              question3b1.add(editText3b1a4);
              question3b1.add(editText3b1b4);
              question3b1hiss = question3b1hisscomma[3].split(":");
              editText3b1a4.setEnabled(false);
              editText3b1b4.setEnabled(false);
              if (question3b1hiss.length != 0) {
                editText3b1a4.setText(question3b1hiss[0]);
                editText3b1b4.setText(question3b1hiss[1]);
              }
              EditText editText3b1a5 = createEditText("新食物5",9);
              EditText editText3b1b5 = createEditText("舊食物5",9);
              question3b1hiss = question3b1hisscomma[4].split(":");
              question3b1.add(editText3b1a5);
              question3b1.add(editText3b1b5);
              editText3b1a5.setEnabled(false);
              editText3b1b5.setEnabled(false);
              if (question3b1hiss.length != 0) {
                editText3b1a5.setText(question3b1hiss[0]);
                editText3b1b5.setText(question3b1hiss[1]);
              }
              break;
          }
        }
      }
      layout1.addView(layout3, params3);
      createTextView(getString(R.string.option32), 3);
      layout3.addView(layout10, params10);
      layout3.addView(layout11, params11);
      layout3.addView(layout12, params12);
      layout3.addView(layout13, params13);
      layout3.addView(layout14, params14);
      EditText editText3b2a = createEditText("多吃1", 10);
      question3b2.add(editText3b2a);
      if (review) {
        copyButton2();
      } else {
        if (!question3b2his.contains(",")) {
          editText3b2a.setEnabled(false);
          editText3b2a.setText(question3b2his);
        } else {
          question3b2hiss = question3b2his.split(",");
          switch (question3b2hiss.length){
            case 2:
              editText3b2a.setEnabled(false);
              editText3b2a.setText(question3b2hiss[0]);
              EditText editText3b2b = createEditText("多吃2",11);
              question3b2.add(editText3b2b);
              editText3b2b.setEnabled(false);
              editText3b2b.setText(question3b2hiss[1]);
              break;
            case 3:
              editText3b2a.setEnabled(false);
              editText3b2a.setText(question3b2hiss[0]);
              editText3b2b = createEditText("多吃2",11);
              question3b2.add(editText3b2b);
              editText3b2b.setEnabled(false);
              editText3b2b.setText(question3b2hiss[1]);
              EditText editText3b2c = createEditText("多吃3",12);
              question3b2.add(editText3b2c);
              editText3b2c.setEnabled(false);
              editText3b2c.setText(question3b1hiss[2]);
              break;
            case 4:
              editText3b2a.setEnabled(false);
              editText3b2a.setText(question3b2hiss[0]);
              editText3b2b = createEditText("多吃2",11);
              question3b2.add(editText3b2b);
              editText3b2b.setEnabled(false);
              editText3b2b.setText(question3b2hiss[1]);
              editText3b2c = createEditText("多吃3",12);
              question3b2.add(editText3b2c);
              editText3b2c.setEnabled(false);
              editText3b2c.setText(question3b2hiss[2]);
              EditText editText3b2d = createEditText("多吃4",13);
              question3b2.add(editText3b2d);
              editText3b2d.setEnabled(false);
              editText3b2d.setText(question3b2hiss[3]);
              break;
            case 5:
              editText3b2a.setEnabled(false);
              editText3b2a.setText(question3b2hiss[0]);
              editText3b2b = createEditText("多吃2",11);
              question3b2.add(editText3b2b);
              editText3b2b.setEnabled(false);
              editText3b2b.setText(question3b2hiss[1]);
              editText3b2c = createEditText("多吃3",12);
              question3b2.add(editText3b2c);
              editText3b2c.setEnabled(false);
              editText3b2c.setText(question3b2hiss[2]);
              editText3b2d = createEditText("多吃4",13);
              question3b2.add(editText3b2d);
              editText3b2d.setEnabled(false);
              editText3b2d.setText(question3b2hiss[3]);
              EditText editText3b2e = createEditText("多吃5",14);
              question3b2.add(editText3b2e);
              editText3b2e.setEnabled(false);
              editText3b2e.setText(question3b2hiss[4]);
              break;
          }
        }
      }
      layout1.addView(layout6, params6);
      createTextView(getString(R.string.option33), 6);
      layout6.addView(layout15, params15);
      layout6.addView(layout16, params16);
      layout6.addView(layout17, params17);
      layout6.addView(layout18, params18);
      layout6.addView(layout19, params19);
      EditText editText3b3a = createEditText("少吃1", 15);
      question3b3.add(editText3b3a);
    if (review) {
      copyButton3();
    } else {
      if (!question3b3his.contains(",")) {
        editText3b3a.setEnabled(false);
        editText3b3a.setText(question3b3his);
      } else {
        question3b3hiss = question3b3his.split(",");
        switch (question3b3hiss.length){
          case 2:
            editText3b3a.setEnabled(false);
            editText3b3a.setText(question3b3hiss[0]);
            EditText editText3b3b = createEditText("少吃2",16);
            question3b3.add(editText3b3b);
            editText3b3b.setEnabled(false);
            editText3b3b.setText(question3b3hiss[1]);
            break;
          case 3:
            editText3b3a.setEnabled(false);
            editText3b3a.setText(question3b3hiss[0]);
            editText3b3b = createEditText("少吃2",16);
            question3b3.add(editText3b3b);
            editText3b3b.setEnabled(false);
            editText3b3b.setText(question3b3hiss[1]);
            EditText editText3b3c = createEditText("少吃3",17);
            question3b3.add(editText3b3c);
            editText3b3c.setEnabled(false);
            editText3b3c.setText(question3b3hiss[2]);
            break;
          case 4:
            editText3b3a.setEnabled(false);
            editText3b3a.setText(question3b3hiss[0]);
            editText3b3b = createEditText("少吃2",16);
            question3b3.add(editText3b3b);
            editText3b3b.setEnabled(false);
            editText3b3b.setText(question3b3hiss[1]);
            editText3b3c = createEditText("少吃3",17);
            question3b3.add(editText3b3c);
            editText3b3c.setEnabled(false);
            editText3b3c.setText(question3b3hiss[2]);
            EditText editText3b3d = createEditText("少吃4",18);
            question3b3.add(editText3b3d);
            editText3b3d.setEnabled(false);
            editText3b3d.setText(question3b3hiss[3]);
            break;
          case 5:
            editText3b3a.setEnabled(false);
            editText3b3a.setText(question3b3hiss[0]);
            editText3b3b = createEditText("少吃2",16);
            question3b3.add(editText3b3b);
            editText3b3b.setEnabled(false);
            editText3b3b.setText(question3b3hiss[1]);
            editText3b3c = createEditText("少吃3",17);
            question3b3.add(editText3b3c);
            editText3b3c.setEnabled(false);
            editText3b3c.setText(question3b3hiss[2]);
            editText3b3d = createEditText("少吃4",18);
            question3b3.add(editText3b3d);
            editText3b3d.setEnabled(false);
            editText3b3d.setText(question3b3hiss[3]);
            EditText editText3b3e = createEditText("少吃5",19);
            question3b3.add(editText3b3e);
            editText3b3e.setEnabled(false);
            editText3b3e.setText(question3b3hiss[4]);
            break;
        }
      }
    }
      Button save = saveButton();
      save.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          if (review) {
            selectedOption = new ArrayList<Integer>();
            for (int i = 0; i < checkbox.length; i++) {
              if (checkbox[i].isChecked()) {
                int k = 0;
                k = i + 1;
                selectedOption.add(k);
              }
            }
            question3aanswer = "";
            for (int i = 0; i < selectedOption.size(); i++) {
              if (i != selectedOption.size() - 1) {
                question3aanswer += selectedOption.get(i).toString() + ",";
              } else {
                question3aanswer += selectedOption.get(i).toString();
              }
            }
            if (!editText.getText().toString().equals("") && editText.isEnabled()) {
              question3aanswer += ":" + editText.getText().toString();
            }
            final String q2a = getIntent().getStringExtra("question2result");
            final float rating = getIntent().getFloatExtra("rating", 0);
            System.out.println("response q1: " + rating);
            System.out.println("response q2: " + q2a);
            System.out.println("response q3: " + question3aanswer);
            question3b1answer = "";
            if (question3b1.size() == 0){
              question3b1answer = "NAN";
            } else {
              for (int i = 0; i < question3b1.size(); i += 2) {
                if (i != question3b1.size() - 2) {
                  question3b1answer += question3b1.get(i).getText().toString() + ":" + question3b1.get(i + 1).getText().toString() + ",";
                } else {
                  question3b1answer += question3b1.get(i).getText().toString() + ":" + question3b1.get(i + 1).getText().toString();
                }
              }
            }
            System.out.println("number of response q3b1: " + question3b1.size());
            System.out.println("string represent of q3b1: " + question3b1answer);
            question3b2answer = "";
            if (question3b2.size() == 0){
              question3b2answer = "NAN";
            } else {
              for (int i = 0; i < question3b2.size(); i++) {
                if (i != question3b2.size() - 1) {
                  question3b2answer += question3b2.get(i).getText().toString() + ",";
                } else {
                  question3b2answer += question3b2.get(i).getText().toString();
                }
              }
            }
            System.out.println("number of response q3b2: " + question3b2.size());
            System.out.println("string represent of q3b2: " + question3b2answer);
            question3b3answer = "";
            if (question3b3.size() == 0){
              question3b3answer = "NAN";
            } else {
              for (int i = 0; i < question3b3.size(); i++) {
                if (i != question3b3.size() - 1) {
                  question3b3answer += question3b3.get(i).getText().toString() + ",";
                } else {
                  question3b3answer += question3b3.get(i).getText().toString();
                }
              }
            }
            System.out.println("number of response q3b3: " + question3b3.size());
            System.out.println("string represent of q3b3: " + question3b3answer);

            UserLocalStore userLocalStore = new UserLocalStore(v.getContext());
            final String userId = Integer.toString(userLocalStore.getLoggedInUser().userid);

            SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
            Date d = null;
            try {
              d = s.parse(date);
            } catch (ParseException e) {
              e.printStackTrace();
            }
            SimpleDateFormat ss = new SimpleDateFormat("yyyyMMddhhmmssSSS");
            String currentTimestamp = ss.format(d);

            foodAssessment foodAssessment = new foodAssessment(getApplicationContext());
            //  SimpleDateFormat s = new SimpleDateFormat("yyyyMMddhhmmssSSS");
            //  String currentTimestamp = s.format(new Date());
            //  System.out.println(currentTimestamp.substring(0,8));
            //  List<String> strings =  new ArrayList<String>();
            //  strings.add(currentTimestamp.substring(0,8));
            //  List<HashMap<String, String>> map =  foodAssessment.getRecordHashMapByCreateDatetime(strings);
            //  System.out.println(map.isEmpty());
            //  if (!map.isEmpty()) {
            foodAssessment.insert(foodAssessment.new foodAssess_class(0, Integer.parseInt(userId), date, rating, q2a, question3aanswer, question3b1answer, question3b2answer, question3b3answer), currentTimestamp.substring(0,8));
            Bundle bundle = new Bundle();
            bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
            bundle.putBoolean(ContentResolver.SYNC_EXTRAS_FORCE, true);
            bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);


            ContentResolver.requestSync(null, "com.dmc.myapplication.provider", bundle);
            ConnectivityManager cm =
                    (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null &&
                    activeNetwork.isConnectedOrConnecting();
            //final List<HashMap<String, String>> results = new ArrayList<>();
            List<String> time = new ArrayList<>();
            time.add(currentTimestamp.substring(0, 8));
            final List<HashMap<String, String>> results = foodAssessment.getRecordHashMapByCreateDatetime(time);

            RetrofitFunction retrofitFunction = new RetrofitFunction(navFoodTest2Activity.this, webService.serverURL);
            retrofitFunction.insertFoodAssessment(Integer.valueOf(userId), d, rating, q2a, question3aanswer, question3b1answer, question3b2answer, question3b3answer, currentTimestamp);


            if (isConnected) {

            //  Thread thread = new Thread(new Runnable() {
                //@Override
               // public void run() {
              //    String data = "";
                //  data = "userId="+userId+"&event=foodAssessment&method=updateRecords&data=" + results;
                //  webHelper.sendRequest("POST", data);
              //  }
              //});
             // thread.start();

             // try{
               // thread.join();
             // }catch (Exception e){
               // e.printStackTrace();
              //}

              //  navfoodAsyncTask asyncTask = new navfoodAsyncTask(navFoodTest2Activity.this, navFoodConstant.SAVE_FOOD_ASSESSMENT);
            //  asyncTask.execute(userId, date, String.valueOf(rating), q2a, question3aanswer, question3b1answer, question3b2answer, question3b3answer, currentTimestamp);
            }
            rewards rewards = new rewards(getApplicationContext());
            List<HashMap<String, String>> result = new ArrayList<>();

            result = rewards.getAllCreatedatetimeEditdatetime();
            System.out.println("current rewards: " + result);

            if (isConnected) {
              navfoodAsyncTask asyncTask = new navfoodAsyncTask(navFoodTest2Activity.this, navFoodConstant.SAVE_REWARDS_DATA);
              asyncTask.execute(userId, String.valueOf(Integer.valueOf(result.get(0).get("FOOD_REWARDS")) + 30), currentTimestamp);
            }
          }
          //}
          Intent intent = new Intent();
          intent.setClass(getApplicationContext(), MainActivity.class);
          intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_NAVFOOD);
          intent.putExtra(navFoodConstant.GET_FOOD_DATE, date);
          intent.putExtra("finished", true);
          startActivity(intent);
          finish();

        }
      });


  }
  private void setHandlerFunction() {
    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
    handler = new Handler() {
      @Override
      public void handleMessage(Message msg) {
        switch (msg.what) {
          case 0:
            builder.setMessage("資料已成功儲存")
                    .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                      @Override
                      public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        handleBackAction();
                      }
                    })
                    .show();
            break;
          case 1:
            builder.setMessage("請稍後再試")
                    .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                      @Override
                      public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                      }
                    })
                    .show();
            break;
        }
      }
    };
  }
  private void handleBackAction() {
    Intent intent = new Intent();
    intent.setClass(this, MainActivity.class);
    intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_NAVFOOD);
    startActivity(intent);
    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    finish();
  }
  public void sendSuccessMessage() {
    Message message = new Message();
    message.what = 0;
    handler.sendMessage(message);
  }

  public void sendFailMessage() {
    Message message = new Message();
    message.what = 1;
    handler.sendMessage(message);
  }
  // create a button to show/save data , entered in the Form
  private void copyButton() {
    Button copyButton = new Button(this);
    copyButton.setHeight(WRAP_CONTENT);
    copyButton.setText("＋1");
    copyButton.setWidth(0);
    arams.get(1).weight = 1;
    copyButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        copyButtonFreq++;
        switch (copyButtonFreq) {
          case 1:
            EditText editText3b1a2 = createEditText("新食物2",5);
            EditText editText3b1b2 = createEditText("舊食物2",5);
            question3b1.add(editText3b1a2);
            question3b1.add(editText3b1b2);
            break;
          case 2:
            EditText editText3b1a3 = createEditText("新食物3",7);
            EditText editText3b1b3 = createEditText("舊食物3",7);
            question3b1.add(editText3b1a3);
            question3b1.add(editText3b1b3);
            break;
          case 3:
            EditText editText3b1a4 = createEditText("新食物4",8);
            EditText editText3b1b4 = createEditText("舊食物4",8);
            question3b1.add(editText3b1a4);
            question3b1.add(editText3b1b4);
            break;
          case 4:
            EditText editText3b1a5 = createEditText("新食物5",9);
            EditText editText3b1b5 = createEditText("舊食物5",9);
            question3b1.add(editText3b1a5);
            question3b1.add(editText3b1b5);
            break;
          default:
            Toast.makeText(navFoodTest2Activity.this,"+1 已達最大值", Toast.LENGTH_LONG).show();
              break;
        }

      }
    });
    inearLayout.get(1).addView(copyButton,arams.get(1));
  }
  // create a button to show/save data , entered in the Form
  private void copyButton2() {
    Button copyButton = new Button(this);
    copyButton.setHeight(WRAP_CONTENT);
    copyButton.setText("+1");
    copyButton.setWidth(0);
    arams.get(9).weight = 1;
    copyButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        copyButtonFreq2++;
        switch (copyButtonFreq2) {
          case 1:
            EditText editText3b2b = createEditText("多吃2",11);
            question3b2.add(editText3b2b);
            break;
          case 2:
            EditText editText3b2c = createEditText("多吃3",12);
            question3b2.add(editText3b2c);
            break;
          case 3:
            EditText editText3b2d = createEditText("多吃4",13);
            question3b2.add(editText3b2d);
            break;
          case 4:
            EditText editText3b2e = createEditText("多吃5",14);
            question3b2.add(editText3b2e);
            break;
          default:
            Toast.makeText(navFoodTest2Activity.this,"+1 已達最大值", Toast.LENGTH_LONG).show();
            break;
        }
      }
    });
    inearLayout.get(9).addView(copyButton,arams.get(9));
  }
  // create a button to show/save data , entered in the Form
  private void copyButton3() {
    Button copyButton = new Button(this);
    copyButton.setHeight(WRAP_CONTENT);
    copyButton.setText("+1");
    copyButton.setWidth(0);
    arams.get(14).weight = 1;
    copyButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        copyButtonFreq3++;
        switch (copyButtonFreq3) {
          case 1:
            EditText editText3c3b = createEditText("少吃2",16);
            question3b3.add(editText3c3b);
            break;
          case 2:
            EditText editText3c3c = createEditText("少吃3",17);
            question3b3.add(editText3c3c);
            break;
          case 3:
            EditText editText3c3d = createEditText("少吃4",18);
            question3b3.add(editText3c3d);
            break;
          case 4:
            EditText editText3c3e = createEditText("少吃5",19);
            question3b3.add(editText3c3e);
            break;
          default:
            Toast.makeText(navFoodTest2Activity.this,"+1 已達最大值", Toast.LENGTH_LONG).show();
            break;
        }
      }
    });
    inearLayout.get(14).addView(copyButton,arams.get(14));
  }
  // create a button to show/save data , entered in the Form
  private Button saveButton() {
    Button saveButton = new Button(this);
    saveButton.setHeight(WRAP_CONTENT);
    saveButton.setText("Save");
    layout1.addView(saveButton,params);
    return saveButton;
  }

  // Access the value of the EditText

  private View.OnClickListener submitListener = new View.OnClickListener() {
    public void onClick(View view) {
      StringBuilder stringBuilder = new StringBuilder();
      for (View singView : allViews) {

        String className = Utils.getClassName(singView.getClass());

        if (className.equalsIgnoreCase("EditText")) {
          EditText editText = (EditText) singView;
          stringBuilder.append(" "+editText.getText().toString());
        }
        else if (className.equalsIgnoreCase("CheckedTextView")) {

          CheckedTextView checkBox = (CheckedTextView) singView;
          stringBuilder.append(" "+checkBox.isChecked());
        }

      }
      Log.i("All Data ", stringBuilder.toString());

      Utils.showAlertDialog(view.getContext(), "Data", stringBuilder.toString());
    }
  };

  public void createEditText1(String hint, int onto) {
    EditText editText = new EditText(this);
    editText.setId(viewsCount2++);
    System.out.println(editText.getId());
    editText.setEnabled(false);
    editText.setHint(hint);
    editText.setEms(5);
    arams.get(onto-1).weight = 1;
    allViews.add(editText);
    inearLayout.get(onto-1).addView(editText,arams.get(onto-1));

  }
  public EditText createEditText(String hint, int onto) {
    EditText editText = new EditText(this);
    editText.setId(viewsCount++);
    editText.setHint(hint);
    editText.setEms(5);
    arams.get(onto-1).weight = 1;
    allViews.add(editText);
    inearLayout.get(onto-1).addView(editText,arams.get(onto-1));
    return editText;
  }
  public void createTextView(String label, int onto) {

    final TextView checkBox = new TextView(this);
    checkBox.setText(label);

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      checkBox.setTextAppearance(android.R.style.TextAppearance_Large);
    } else {
      checkBox.setTextSize(20f);
      checkBox.setTextColor(Color.BLACK);
    }
    allViews.add(checkBox);
    inearLayout.get(onto-1).addView(checkBox,arams.get(onto-1));
  }



}
