package com.dmc.myapplication.navFood;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.menu.ActionMenuItemView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.Models.food;
import com.dmc.myapplication.Models.foodAssessment;
import com.dmc.myapplication.Models.foodRecord;
import com.dmc.myapplication.R;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.network.RetrofitFunction;
import com.dmc.myapplication.network.webService;
import com.dmc.myapplication.systemConstant;
import com.dmc.myapplication.tool.exportTool;
import com.dmc.myapplication.tool.imageTool;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


/**
 * Created by KwokSinMan on 30/11/2015.
 */
public class navFoodFragment extends Fragment {
    List<HashMap<String, String>> map = new ArrayList<HashMap<String, String>>();
    private static Handler handler;
    String getFoodDate; TextView navFoodDate;
    @Override
    public View onCreateView (final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

        View v =  inflater.inflate(R.layout.navfood_main, container, false);
        System.out.println("== current activity" + getActivity().toString());
        getFoodDate = this.getArguments().getString(navFoodConstant.GET_FOOD_DATE);

        setHandlerFunction();
        // set food  Date
        boolean finished = this.getArguments().getBoolean("finished", false);
        navFoodDate = (TextView) v.findViewById(R.id.navFoodViewDate);
        navFoodDate.setText(new navFoodTime().getUIDateFormal(getFoodDate));

        // set data picker
        ImageView setfooddate = (ImageView) v.findViewById(R.id.setNavFoodViewDate);
        setfooddate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new navFoodViewDatePickerFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        //set Pie Chart onClickListener
        Button navFoodPieChart = (Button) v.findViewById(R.id.navFoodChart);
        /*navFoodPieChart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), navFoodPieChartActivity.class);
                intent.putExtra(navFoodConstant.GET_FOOD_DATE,  new navFoodTime().getDBDateFormal((String) ((TextView) getActivity().findViewById(R.id.navFoodViewDate)).getText()));
                getActivity().startActivity(intent);
                getActivity().finish();
            }
        });
        */
        navFoodPieChart.setVisibility(View.GONE);

        final Button navDoAssessment = (Button) v.findViewById(R.id.do_assessment);
        foodAssessment foodAssessment = new foodAssessment(v.getContext());
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");

         try {
             List<String> strings =  new ArrayList<String>();
             Date d = s.parse(new navFoodTime().getDBDateFormal(navFoodDate.getText().toString()));
             SimpleDateFormat ss = new SimpleDateFormat("yyyyMMddhhmmssSSS");
             String currentTimestamp = ss.format(d);
             System.out.println(currentTimestamp.substring(0,8));
             strings.add(currentTimestamp.substring(0,8));

             map = foodAssessment.getRecordHashMapByCreateDatetime(strings);
             navstoreMapClass.setMap(map);
             //TODO: if local storage is not empty, query local storage
            if (map.get(0).get("FOOD_EMPTY").equals("1")){
                System.out.println("rating his: " + map.get(0).get("FOOD_RATING"));
                System.out.println("Q2 his: " + map.get(0).get("FOOD_Q2"));
                System.out.println("Q3a his: " + map.get(0).get("FOOD_Q3"));
                System.out.println("Q3b1 his: " + map.get(0).get("FOOD_Q3B1"));
                System.out.println("Q3b2 his: " + map.get(0).get("FOOD_Q3B2"));
                System.out.println("Q3b3 his: " + map.get(0).get("FOOD_Q3B3"));
                navDoAssessment.setText(getString(R.string.title_activity_nav_food_test_review));
            } else {
                // TODO: local storage is empty, try to get data from server
                String now = ss.format(new Date());
                ConnectivityManager cm =
                        (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null &&
                        activeNetwork.isConnectedOrConnecting();
                UserLocalStore userLocalStore = new UserLocalStore(this.getActivity());
                String userId = Integer.toString(userLocalStore.getLoggedInUser().userid);

                if (isConnected) {
                    navfoodAsyncTask asyncTask = new navfoodAsyncTask(getActivity(), navFoodConstant.GET_FOOD_ASSESSMENT);
                    asyncTask.execute(userId, getFoodDate );
                } else {
                    // TODO: only if current date is today and no data available from server,
                    // allow user do assessment
                    if (currentTimestamp.substring(0, 8).equals(now.substring(0, 8)) || !finished) {
                        navDoAssessment.setEnabled(true);
                        navDoAssessment.setText(getString(R.string.title_activity_nav_food_test));
                    } else {
                        navDoAssessment.setEnabled(false);
                    }
                }
             }
         } catch (ParseException e) {
             e.printStackTrace();
        }


        navDoAssessment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), navFoodTestActivity.class);
                intent.putExtra(navFoodConstant.GET_FOOD_DATE,  new navFoodTime().getDBDateFormal((String) ((TextView) getActivity().findViewById(R.id.navFoodViewDate)).getText()));
                if (navDoAssessment.getText() == getString(R.string.title_activity_nav_food_test_review) && navDoAssessment.isEnabled()) {
                    intent.putExtra("test", false);

                    intent.putExtra("rating", Float.valueOf(navstoreMapClass.getMap().get(0).get("FOOD_RATING")));
                    intent.putExtra("Q2", navstoreMapClass.getMap().get(0).get("FOOD_Q2"));
                    intent.putExtra("Q3a", navstoreMapClass.getMap().get(0).get("FOOD_Q3"));
                    intent.putExtra("Q3b1", navstoreMapClass.getMap().get(0).get("FOOD_Q3B1"));
                    intent.putExtra("Q3b2", navstoreMapClass.getMap().get(0).get("FOOD_Q3B2"));
                    intent.putExtra("Q3b3", navstoreMapClass.getMap().get(0).get("FOOD_Q3B3"));
                } else {
                    intent.putExtra("test", true);
                }
                getActivity().startActivity(intent);
                getActivity().finish();
            }
        });

        //set Add record event
/*
        Button navfoodAdd = (Button) v.findViewById(R.id.navFoodAdd);
        navfoodAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), navFoodAddActivity.class);
                intent.putExtra(navFoodConstant.GET_FOOD_DATE,  new navFoodTime().getDBDateFormal((String) ((TextView) getActivity().findViewById(R.id.navFoodViewDate)).getText()));
                startActivity(intent);
            }
        });
*/
        //set edit food record event
        ExpandableListView expListView = (ExpandableListView) v.findViewById(R.id.lvExp);
        expListView.setOnChildClickListener(new editFoodOnChildClickListener(this.getActivity()));

        // get Data from DB to display total CARBOHYDRATE and food result
        UserLocalStore userLocalStore = new UserLocalStore(this.getActivity());

        int userId = userLocalStore.getLoggedInUser().userid;
      //  SimpleDateFormat ss = new SimpleDateFormat("yyyy-MM-dd");
      //  Date d = null;

      //  try {
        //    d = ss.parse(new navFoodTime().getDBDateFormal(navFoodDate.getText().toString()));
       // } catch (ParseException e) {
         //   e.printStackTrace();
       // }
        RetrofitFunction function = new RetrofitFunction(getActivity(), webService.serverURL);
        function.getFoodRecord(userId, new navFoodTime().getDBDateFormal(navFoodDate.getText().toString()));
      //  navFoodBiz biz = new navFoodBiz();
      //  biz.setUpListView(this.getActivity(), getFoodDate);



        return v;
    }
    private void setHandlerFunction() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 0:
            //            builder.setMessage("資料已成功儲存")
          //                      .setPositiveButton("確定", new DialogInterface.OnClickListener() {
              //                      @Override
                //                    public void onClick(DialogInterface dialogInterface, int i) {
                  //                      dialogInterface.dismiss();
                                            navfoodAsyncTask connect = new navfoodAsyncTask(getActivity(), "ABC");
                                            connect.execute(new navFoodTime().getDBDateFormal(navFoodDate.getText().toString()));
                    //                }
                     //           })
                       //         .show();
                        break;
                    case 1:
                        builder.setMessage("請稍後再試")
                                .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                     dialogInterface.dismiss();
                                    }
                                })
                                .show();
                        break;
                }
            }
        };
    }
    public static void setFoodRecord(Context context,String result) {
        Gson gson = new Gson();
        System.out.println("== food record" + result);
        List<HashMap<String, String>> pastBodyRecordsList = gson.fromJson(result, new TypeToken<List<HashMap<String, String>>>() {
        }.getType());
        com.dmc.myapplication.Models.foodRecord foodRdb = new com.dmc.myapplication.Models.foodRecord(context);
        foodRdb.delete();
        if (pastBodyRecordsList != null) {
            for (int x = 0; x < pastBodyRecordsList.size(); x++) {
                String[] dateStr = pastBodyRecordsList.get(x).get("FOOD_DATE").split("-");
                Integer year = Integer.parseInt(dateStr[0]);
                Integer Month = Integer.parseInt(dateStr[1]);
                Integer Day = Integer.parseInt(dateStr[2]);
                String newDateStr = year.toString() + "-" + Month.toString() + "-" + Day.toString();

                String foodTime, foodSession, foodPlace, foodName, foodUnit, photoPath;
                int foodID, foodQuantity;
                double foodCalorie, foodCarbohydrate, foodProtein, foodFat;

                if (pastBodyRecordsList.get(x).get("FOOD_TIME") == null) {
                    foodTime = "";
                } else {
                    foodTime = pastBodyRecordsList.get(x).get("FOOD_TIME");
                }

                if (pastBodyRecordsList.get(x).get("FOOD_SESSION") == null) {
                    foodSession = "";
                } else {
                    foodSession = pastBodyRecordsList.get(x).get("FOOD_SESSION");
                }

                if (pastBodyRecordsList.get(x).get("FOOD_PLACE") == null) {
                    foodPlace = "";
                } else {
                    foodPlace = pastBodyRecordsList.get(x).get("FOOD_PLACE");
                }

                if (pastBodyRecordsList.get(x).get("FOOD_ID") == null) {
                    foodID = 0;
                } else {
                    foodID = Integer.parseInt(pastBodyRecordsList.get(x).get("FOOD_ID"));
                }

                if (pastBodyRecordsList.get(x).get("FOOD_QUANTITY") == null) {
                    foodQuantity = 0;
                } else {
                    foodQuantity = Integer.parseInt(pastBodyRecordsList.get(x).get("FOOD_QUANTITY"));
                }

                if (pastBodyRecordsList.get(x).get("FOOD_CALORIE") == null) {
                    foodCalorie = 0;
                } else {
                    foodCalorie = Double.parseDouble(pastBodyRecordsList.get(x).get("FOOD_CALORIE"));
                }

                if (pastBodyRecordsList.get(x).get("FOOD_CARBOHYDRATE") == null) {
                    foodCarbohydrate = 0;
                } else {
                    foodCarbohydrate = Double.parseDouble(pastBodyRecordsList.get(x).get("FOOD_CARBOHYDRATE"));
                }

                if (pastBodyRecordsList.get(x).get("FOOD_PROTEIN") == null) {
                    foodProtein = 0;
                } else {
                    foodProtein = Double.parseDouble(pastBodyRecordsList.get(x).get("FOOD_PROTEIN"));
                }

                if (pastBodyRecordsList.get(x).get("FOOD_FAT") == null) {
                    foodFat = 0;
                } else {
                    foodFat = Double.parseDouble(pastBodyRecordsList.get(x).get("FOOD_FAT"));
                }

                if (pastBodyRecordsList.get(x).get("FOOD_NAME") == null) {
                    foodName = "";
                } else {
                    foodName = pastBodyRecordsList.get(x).get("FOOD_NAME");
                }

                if (pastBodyRecordsList.get(x).get("FOOD_UNIT") == null) {
                    foodUnit = "";
                } else {
                    foodUnit = pastBodyRecordsList.get(x).get("FOOD_UNIT");
                }

                if (pastBodyRecordsList.get(x).get("PHOTO_PATH") == null) {
                    photoPath = "";
                } else {
                    photoPath = pastBodyRecordsList.get(x).get("PHOTO_PATH");
                }
                System.out.println("== food record id is: " + Integer.valueOf(pastBodyRecordsList.get(x).get("FOOD_RECORD_ID")));
                com.dmc.myapplication.Models.foodRecord.foodRecord_class obj = foodRdb.new foodRecord_class(Integer.valueOf(pastBodyRecordsList.get(x).get("FOOD_RECORD_ID")),
                        Integer.parseInt(pastBodyRecordsList.get(x).get("USER_ID")),
                        newDateStr,
                        foodTime,
                        foodSession,
                        foodPlace,
                        foodID,
                        foodQuantity,
                        foodCalorie,
                        foodCarbohydrate,
                        foodProtein,
                        foodFat,
                        foodName,
                        foodUnit,
                        photoPath);

                obj.create_datetime = pastBodyRecordsList.get(x).get("create_datetime");
                obj.edit_datetime = pastBodyRecordsList.get(x).get("edit_datetime");

                foodRdb.insert(obj);


            }
        }
    }
    public static void sendRetrieveSuccessMessage() {
        Message message = new Message();
        message.what = 0;
        handler.sendMessage(message);
    }

    public static void sendRetrieveFailedMessage() {
        Message message = new Message();
        message.what = 1;
        handler.sendMessage(message);
    }

    private class editFoodOnChildClickListener implements ExpandableListView.OnChildClickListener {
        private Activity activity;
        public editFoodOnChildClickListener(Activity activity){this.activity = activity;}

        @Override
        public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
            foodRecord foodDB = new foodRecord(getContext());
            int foodRecordId = ((navFoodBiz.foodRecord) parent.getExpandableListAdapter().getChild(groupPosition, childPosition)).getFoodRecordId();
            String isFreeText = ((navFoodBiz.foodRecord) parent.getExpandableListAdapter().getChild(groupPosition, childPosition)).getIsFreeTextInput();
            Boolean isSpeedMode = foodDB.get(foodRecordId).isSpeedRecordMode();
            foodDB.close();
            if (isSpeedMode){
                Intent intent = new Intent();
                intent.setClass(activity, navFoodEasyEditActivity.class);
                intent.putExtra("food_record_id", foodRecordId);
                intent.putExtra(navFoodConstant.GET_FOOD_DATE,  new navFoodTime().getDBDateFormal((String) ((TextView) getActivity().findViewById(R.id.navFoodViewDate)).getText()));
                activity.startActivity(intent);
                activity.overridePendingTransition(R.anim.slide1,R.anim.slide2);
                activity.finish();

            }else{
                //System.out.println("Candy testing foodRecordId=" + foodRecordId);
                //System.out.println("Candy testing isFreeText=" + isFreeText);
                Intent intent = new Intent();
                intent.setClass(activity, navFoodEditDetailActivity.class);
                intent.putExtra("food_record_id", foodRecordId);
                intent.putExtra("is_free_text",isFreeText);
                intent.putExtra(navFoodConstant.GET_FOOD_DATE,  new navFoodTime().getDBDateFormal(((TextView) getActivity().findViewById(R.id.navFoodViewDate)).getText().toString()));
                activity.startActivity(intent);
                activity.overridePendingTransition(R.anim.slide1,R.anim.slide2);
                activity.finish();
            }
            return false;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setHasOptionsMenu(true);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu,MenuInflater inflater){
        inflater.inflate(R.menu.food_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent();
        switch (item.getItemId()){
            case R.id.food_menuItem_add:
                intent.setClass(getActivity(), navFoodAddActivity.class);
                intent.putExtra(navFoodConstant.GET_FOOD_DATE,  new navFoodTime().getDBDateFormal((String) ((TextView) getActivity().findViewById(R.id.navFoodViewDate)).getText()));
                getActivity().startActivity(intent);
                getActivity().finish();
                break;

            case R.id.food_menuItem_speed_add:
                intent.setClass(getActivity(), navFoodEasyAddActivity.class);
                intent.putExtra(navFoodConstant.GET_FOOD_DATE,  new navFoodTime().getDBDateFormal((String) ((TextView) getActivity().findViewById(R.id.navFoodViewDate)).getText()));
                getActivity().startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide1,R.anim.slide2);
                getActivity().finish();
                break;

        }
        return true;
    }




}

