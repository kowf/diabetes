package com.dmc.myapplication.navFood;

import android.app.AlertDialog;
import android.content.Context;

/**
 * Created by lamivan on 17/12/2017.
 */

public class Utils {
  public static void showAlertDialog(Context context, String title,
                                     String message) {
    AlertDialog.Builder builder = new AlertDialog.Builder(context);
    if (title != null)
      builder.setTitle(title);
    builder.setMessage(message);
    builder.setNegativeButton("OK", null);
    builder.show();
  }

  /**
   *
   * @param c
   * @return
   */

  public static String getClassName(Class c) {
    String className = c.getName();
    int firstChar;
    firstChar = className.lastIndexOf('.') + 1;
    if (firstChar > 0) {
      className = className.substring(firstChar);
    }
    return className;
  }
}
