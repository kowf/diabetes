package com.dmc.myapplication.gymNew;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.dmc.myapplication.R;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.tool.dateTool;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;


/**
 * Created by KwokSinMan on 13/3/2016.
 */
public class gymNewFragment extends Fragment {
    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.gym_new_main, container, false);
        String getGymDate = this.getArguments().getString(gymNewConstant.GET_GYM_DATE);
        gymNewConstant.getGymDate = getGymDate;
        // set gym Date
        TextView gymViewDate = (TextView) v.findViewById(R.id.gymNewViewDate);
        gymViewDate.setText(dateTool.getUIDateFormal(getGymDate));

        // set data picker
        ImageView setGymDate = (ImageView) v.findViewById(R.id.setGymNewViewDate);
        setGymDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new gymViewDatePickerFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        //
        ExpandableListView expListView = (ExpandableListView) v.findViewById(R.id.gymNewRecord);
        expListView.setOnChildClickListener(new editGymOnChildClickListener(this.getActivity()));


        // get Data from DB to display total gym min and gym result
        gymNewAsyncTask connect = new gymNewAsyncTask(this.getActivity(), gymNewConstant.GET_GYM_RECORD);
        UserLocalStore userLocalStore = new UserLocalStore(this.getActivity());
        String userId = Integer.toString(userLocalStore.getLoggedInUser().userid);
        connect.execute(userId, getGymDate);

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu,MenuInflater inflater){
        inflater.inflate(R.menu.gym_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() ==  R.id.gym_menuItem_add){
            Intent intent = new Intent();
            intent.setClass(getActivity(), gymNewAddActivity.class);
            intent.putExtra(gymNewConstant.GET_GYM_DATE, dateTool.getDBDateFormal((String) ((TextView) getActivity().findViewById(R.id.gymNewViewDate)).getText()));
            getActivity().startActivity(intent);
            getActivity().finish();
        }
        return true;
    }

    private class editGymOnChildClickListener implements ExpandableListView.OnChildClickListener {
        private Activity activity;
        public editGymOnChildClickListener(Activity activity){this.activity = activity;}
        @Override
        public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
            String gymRecordId = Integer.toString(((gymNewRecord) parent.getExpandableListAdapter().getChild(groupPosition, childPosition)).getGymNewRecordId());
            //System.out.println("Candy gymRecordId="+gymRecordId);
            Intent intent = new Intent();
            intent.setClass(activity, gymNewEditActivity.class);
            intent.putExtra("gym_new_record_id", gymRecordId);
            intent.putExtra(gymNewConstant.GET_GYM_DATE, dateTool.getDBDateFormal((String) ((TextView) getActivity().findViewById(R.id.gymNewViewDate)).getText()));
            activity.startActivity(intent);
            activity.finish();
            return false;
        }
    }

    public static class gymViewDatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            String displayDate = (String) ((TextView) getActivity().findViewById(R.id.gymNewViewDate)).getText();
            String[] displayDateArray = displayDate.split("/");
            int yy = Integer.parseInt(displayDateArray[2]);
            int mm = Integer.parseInt(displayDateArray[1]) - 1; // because Jan =0
            int dd = Integer.parseInt(displayDateArray[0]);
            DatePickerDialog dialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, this, yy, mm, dd);
            dialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
            return dialog;
        }
        // On the date picker , to confirm the following action
        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            // set display date
            mm = mm + 1; // because Jan =0
            String displayAsDate = dd + "/" + mm + "/" + yy;
            ((TextView) getActivity().findViewById(R.id.gymNewViewDate)).setText(displayAsDate);

            String displayAsDate2 = yy + "-" + mm + "-" + dd;

            gymNewConstant.getGymDate = displayAsDate2;
            //get gym record from DB
            gymNewAsyncTask connect = new gymNewAsyncTask(this.getActivity(), gymNewConstant.GET_GYM_RECORD);
            UserLocalStore userLocalStore = new UserLocalStore(this.getActivity());
            String userId = Integer.toString(userLocalStore.getLoggedInUser().userid);
            connect.execute(userId, dateTool.getDBDateFormal(displayAsDate));
        }
    }

}
