package com.dmc.myapplication.gymNew;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.dmc.myapplication.dmAppAsyncTask;
import com.dmc.myapplication.noInternetConnectionDialog;
import com.dmc.myapplication.serverNoResponseDialog;

import java.net.SocketTimeoutException;
import java.net.URLEncoder;

/**
 * Created by KwokSinMan on 13/3/2016.
 */
public class gymNewAsyncTask extends AsyncTask<String,Void,String> {
    private ProgressDialog progressBar;
    private String model;
    private Activity activity;

    public gymNewAsyncTask(Activity activity, String model ) {
        this.activity = activity;
        this.model = model;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressBar = new ProgressDialog(activity);
        progressBar.setCancelable(false);
        progressBar.setTitle("載入中...");
        progressBar.setMessage("請稍候 !!");
        progressBar.show();
    }

    @Override
    protected String doInBackground(String... arg0) {
        String data="";
        try {
            if (model.equals(gymNewConstant.GET_GYM_RECORD)){
                String userId = (String)arg0[0];
                String gymDate = (String)arg0[1];
                data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(userId, "UTF-8");
                data += "&" + URLEncoder.encode("gym_date", "UTF-8") + "=" + URLEncoder.encode(gymDate, "UTF-8");
            }
            if(model.equals(gymNewConstant.GET_GYM_TYPE_DDL)){

            }
            if(model.equals(gymNewConstant.GET_ADD_RECORD)){
                String userId = (String)arg0[0];
                String gymDate = (String)arg0[1];
                String gymTime = (String)arg0[2];
                String gymType = (String)arg0[3];
                String gymValue = (String)arg0[4];
                data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(userId, "UTF-8");
                data += "&" + URLEncoder.encode("gym_date", "UTF-8") + "=" + URLEncoder.encode(gymDate, "UTF-8");
                data += "&" + URLEncoder.encode("gym_time", "UTF-8") + "=" + URLEncoder.encode(gymTime, "UTF-8");
                data += "&" + URLEncoder.encode("gym_type", "UTF-8") + "=" + URLEncoder.encode(gymType, "UTF-8");
                data += "&" + URLEncoder.encode("gym_value", "UTF-8") + "=" + URLEncoder.encode(gymValue, "UTF-8");
            }
            if(model.equals(gymNewConstant.GET_GYM_RECORD_DETAIL)){
                String userId = (String)arg0[0];
                String gymRecordId = (String)arg0[1];
                data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(userId, "UTF-8");
                data += "&" + URLEncoder.encode("record_id", "UTF-8") + "=" + URLEncoder.encode(gymRecordId, "UTF-8");
            }
            if(model.equals(gymNewConstant.DELETE_GYM_RECORD)){
                String userId = (String)arg0[0];
                String gymRecordId = (String)arg0[1];
                data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(userId, "UTF-8");
                data += "&" + URLEncoder.encode("record_id", "UTF-8") + "=" + URLEncoder.encode(gymRecordId, "UTF-8");
            }
            if(model.equals(gymNewConstant.EDIT_GYP_RECORD)){
                String userId = (String)arg0[0];
                String gymRecordId = (String)arg0[1];
                String gymDate = (String)arg0[2];
                String gymTime = (String)arg0[3];
                String gymType = (String)arg0[4];
                String gymValue = (String)arg0[5];
                data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(userId, "UTF-8");
                data += "&" + URLEncoder.encode("record_id", "UTF-8") + "=" + URLEncoder.encode(gymRecordId, "UTF-8");
                data += "&" + URLEncoder.encode("gym_date", "UTF-8") + "=" + URLEncoder.encode(gymDate, "UTF-8");
                data += "&" + URLEncoder.encode("gym_time", "UTF-8") + "=" + URLEncoder.encode(gymTime, "UTF-8");
                data += "&" + URLEncoder.encode("gym_type", "UTF-8") + "=" + URLEncoder.encode(gymType, "UTF-8");
                data += "&" + URLEncoder.encode("gym_value", "UTF-8") + "=" + URLEncoder.encode(gymValue, "UTF-8");
            }

            //get result  from server
            //dmAppAsyncTask dmasyncTask = new dmAppAsyncTask(model);
            //return dmasyncTask.getDataFromServer(data);
            return "";
       // } //catch (SocketTimeoutException e){
           // e.printStackTrace();
           // return dmAppAsyncTask.SERVER_SLEEP;
        } catch (Exception e) {
            e.printStackTrace();
            //return dmAppAsyncTask.NO_INTERNET_CONNECTION;
        }
        return "";
    }

    @Override
    protected void onPostExecute(String result){
        if (result!=null) {
            if(result.equals(dmAppAsyncTask.NO_INTERNET_CONNECTION)){
                progressBar.dismiss();
                noInternetConnectionDialog dialog = new noInternetConnectionDialog();
                dialog.show(this.activity.getFragmentManager(), "NoInternetConnectionDialog");
            }else if(result.equals(dmAppAsyncTask.SERVER_SLEEP)){
                progressBar.dismiss();
                serverNoResponseDialog dialog = new serverNoResponseDialog();
                dialog.show(this.activity.getFragmentManager(), "NoServerResponseDialog");
            }else {
                gymNewBiz gymNewBiz = new gymNewBiz();
                if (model.equals(gymNewConstant.GET_GYM_RECORD)) {
                    gymNewBiz.setGymRecord(this.activity, result);
                }
                if(model.equals(gymNewConstant.GET_GYM_TYPE_DDL)){
                    gymNewBiz.setGymTypeDDL(this.activity, result);
                }
                if(model.equals(gymNewConstant.GET_ADD_RECORD)){
                    gymNewBiz.afterSaveGymRecord(this.activity, false);
                }
                if(model.equals(gymNewConstant.GET_GYM_RECORD_DETAIL)){
                    gymNewBiz.setGymRecordDetail(this.activity, result);
                }
                if(model.equals(gymNewConstant.DELETE_GYM_RECORD)){
                    gymNewBiz.afterSaveGymRecord(this.activity, true);
                }
                if(model.equals(gymNewConstant.EDIT_GYP_RECORD)){
                    gymNewBiz.afterSaveGymRecord(this.activity, false);
                }

                progressBar.dismiss();
            }
        }
    }
}
