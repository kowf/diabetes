package com.dmc.myapplication;

import android.os.AsyncTask;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class AsyncMicrosoftRequest extends AsyncTask<String, Void, String> {
    public interface AsyncResponse {
        void processFinish(String output);
    }

    public AsyncResponse delegate = null;

    public AsyncMicrosoftRequest(AsyncResponse delegate){
        this.delegate = delegate;
    }
   private static String getStringFromInputStream(InputStream is)
           throws IOException {
       ByteArrayOutputStream os = new ByteArrayOutputStream();
       // 模板代码 必须熟练
       byte[] buffer = new byte[1024];
       int len = -1;
       // 一定要写len=is.read(buffer)
       // 如果while((is.read(buffer))!=-1)则无法将数据写入buffer中
       while ((len = is.read(buffer)) != -1) {
           os.write(buffer, 0, len);
       }
       is.close();
       String state = os.toString();// 把流中的数据转换成字符串,采用的编码是utf-8(模拟器默认编码)
       os.close();
       return state;
   }
   @Override
   protected String doInBackground(String... params) {
       System.out.println(params[1]);
       File file = new File(params[1]);
       if (params[0].contains("https")) {
           HttpsURLConnection conn = null;
           TrustManager[] trustAllCerts = new TrustManager[]{
                   new X509TrustManager() {
                       public X509Certificate[] getAcceptedIssuers() {
                           return new X509Certificate[0];
                       }

                       public void checkClientTrusted(
                               X509Certificate[] certs, String authType) {
                       }

                       public void checkServerTrusted(
                               X509Certificate[] certs, String authType) {
                       }
                   }
           };

           // Install the all-trusting trust manager
           try {
               SSLContext sc = SSLContext.getInstance("SSL");
               sc.init(null, trustAllCerts, new java.security.SecureRandom());
               HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                   @Override
                   public boolean verify(String var1, SSLSession var2) {
                       Log.i("RestUtilImpl", "Approving certificate for " + var1);
                       return true;
                   }
               });
               HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
           } catch (GeneralSecurityException e) {
           }
           try {
               FileInputStream fileInputStream = new FileInputStream(file);

               // 创建一个URL对象
               URL mURL = new URL(params[0]);
               System.out.println(params[0]);
               System.out.println(params[1]);
               // 调用URL的openConnection()方法,获取HttpURLConnection对象
               conn = (HttpsURLConnection) mURL.openConnection();

               conn.setRequestMethod("POST");// 设置请求方法为post
               //conn.setReadTimeout(5000);// 设置读取超时为5秒
               //conn.setConnectTimeout(10000);// 设置连接网络超时为10秒
               conn.setDoOutput(true);// 设置此方法,允许向服务器输出内容

               conn.addRequestProperty("Prediction-Key", "b84f4e98cba446d88aa4283c1d0bcc2b");
               conn.addRequestProperty("Content-Type", "application/octet-stream");
               // post请求的参数
               //String data = "userId=999999&event=bodyrecord&method=compareData&data="+datetimeListJson;
               // 获得一个输出流,向服务器写数据,默认情况下,系统不允许向服务器输出内容
               OutputStream out = conn.getOutputStream();// 获得一个输出流,向服务器写数据
               int bytesAvailable, bytesRead, bufferSize;
               byte[] buffer;
               int maxBufferSize = 1024 * 1024;
               bytesAvailable = fileInputStream.available();
               bufferSize = Math.min(bytesAvailable, maxBufferSize);
               buffer = new byte[bufferSize];
               bytesRead = fileInputStream.read(buffer, 0, bufferSize);
               while (bytesRead > 0){
                   out.write(buffer, 0, bufferSize);
                   bytesAvailable = fileInputStream.available();
                   bufferSize = Math.min(bytesAvailable, maxBufferSize);
                   bytesRead = fileInputStream.read(buffer, 0, bufferSize);
               }
               //out.write(params[1].getBytes());
               fileInputStream.close();
               out.flush();
               out.close();

               int responseCode = conn.getResponseCode();// 调用此方法就不必再使用conn.connect()方法
               System.out.println("response Code:" + String.valueOf(responseCode));
               if (responseCode == 200) {
                   InputStream is = conn.getInputStream();
                   String state = getStringFromInputStream(is);
                   System.out.println(params[1]);
                   System.out.println(state);
                   if (conn != null) {
                       is.close();
                       conn.disconnect();// 关闭连接
                   }
                   return state;
               } else {
                   System.out.println("Unable to reach the server. errorcode:" + responseCode);
                   return null;
               }

           } catch (Exception e) {
               e.printStackTrace();
           } finally {
               if (conn != null) {
                   conn.disconnect();// 关闭连接
               }
           }
       } else {
           HttpURLConnection conn = null;
           try {
               // 创建一个URL对象
               URL mURL = new URL(params[0]);

               // 调用URL的openConnection()方法,获取HttpURLConnection对象
               conn = (HttpURLConnection) mURL.openConnection();

               conn.setRequestMethod("POST");// 设置请求方法为post
               //conn.setReadTimeout(5000);// 设置读取超时为5秒
               //conn.setConnectTimeout(10000);// 设置连接网络超时为10秒
               conn.setDoOutput(true);// 设置此方法,允许向服务器输出内容

               // post请求的参数
               //String data = "userId=999999&event=bodyrecord&method=compareData&data="+datetimeListJson;
               // 获得一个输出流,向服务器写数据,默认情况下,系统不允许向服务器输出内容
               OutputStream out = conn.getOutputStream();// 获得一个输出流,向服务器写数据
               out.write(params[1].getBytes());
               out.flush();
               out.close();

               int responseCode = conn.getResponseCode();// 调用此方法就不必再使用conn.connect()方法
               if (responseCode == 200) {
                   InputStream is = conn.getInputStream();
                   String state = getStringFromInputStream(is);
                   System.out.println(params[1]);
                   System.out.println(state);
                   if (conn != null) {
                       is.close();
                       conn.disconnect();// 关闭连接
                   }
                   return state;
               } else {
                   System.out.println("Unable to reach the server. errorcode:" + responseCode);
                   return null;
               }

           } catch (Exception e) {
               e.printStackTrace();
           } finally {
               if (conn != null) {
                   conn.disconnect();// 关闭连接
               }
           }
           return null;
       }
       return "finish worker";
   }

    @Override
    protected void onPostExecute(String result) {
        delegate.processFinish(result);
    }
}
