package com.dmc.myapplication.widgets;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.dmc.myapplication.Models.bodyRecord;
import com.dmc.myapplication.Models.foodRecord;
import com.dmc.myapplication.R;
import com.dmc.myapplication.bodyRecord.BodyRecord;
import com.dmc.myapplication.login.UserLocalStore;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by januslin on 26/3/2017.
 */

public class HomePageRecordGlucoseRemind {
    public static void process(View v, Activity activity) {

        System.out.println("=== HomePageRecordGlucoseRemind started! ===");
        foodRecord foodRecord = new foodRecord(activity.getBaseContext());
        bodyRecord bodyRecord = new bodyRecord(activity.getBaseContext());

        com.dmc.myapplication.Models.foodRecord.foodRecord_class lastFood = foodRecord.getLastRecord();
        BodyRecord.bodyRecord lastGlucose = bodyRecord.getLastGLUCOSE();

        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {
            Date foodRecordDate = format1.parse(lastFood.getFOOD_DATE() + " " + lastFood.getFOOD_TIME());
            Calendar foodRecordDatec = Calendar.getInstance();
            Calendar foodRecordDatec2 = Calendar.getInstance();
            foodRecordDatec.setTime(foodRecordDate);
            foodRecordDatec2.setTime(foodRecordDate);
            foodRecordDatec2.add(Calendar.HOUR_OF_DAY, 2);

            Date bodyRecordDate = format1.parse(lastGlucose.getDate() + " " + lastGlucose.getTime());
            Calendar bodyRecordDatec = Calendar.getInstance();
            bodyRecordDatec.setTime(bodyRecordDate);

            Calendar c = Calendar.getInstance();

            System.out.println(lastFood.getFOOD_DATE() + " " + lastFood.getFOOD_TIME());
            System.out.println(foodRecordDate.toString());
            System.out.println(foodRecordDatec2.toString());
            System.out.println(foodRecordDatec.toString());


            if (c.after(foodRecordDatec2) && bodyRecordDatec.before(foodRecordDatec)) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
                alertDialogBuilder.setMessage("請記錄你最新血糖數值!");
                alertDialogBuilder.setCancelable(false);
                alertDialogBuilder.setPositiveButton("好的", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things
                    }
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
            System.out.println("=== HomePageRecordGlucoseRemind ended! ===");


        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
