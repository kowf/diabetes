package com.dmc.myapplication.widgets;

import android.app.Activity;
import java.util.Calendar;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dmc.myapplication.Models.bodyRecord;
import com.dmc.myapplication.R;
import com.dmc.myapplication.bodyRecord.BodyRecord;
import com.dmc.myapplication.login.UserLocalStore;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by januslin on 26/3/2017.
 */

public class HomePageAdvisor {
    public static void process(View v, Activity activity) {
        UserLocalStore userLocalStore = new UserLocalStore(activity.getBaseContext());
        String username = userLocalStore.getLoggedInUser().name;

        String time = "";
        String mainMsg = "";
        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);

        FrameLayout home_advisor_bg = (FrameLayout) v.findViewById(R.id.home_advisor_bg);
        ImageView home_advisor_icon = (ImageView) v.findViewById(R.id.home_advisor_icon);
        Button advisor_help_button = (Button) v.findViewById(R.id.advisor_help_button);
        Button advisor_help_button2 = (Button) v.findViewById(R.id.advisor_help_button2);

        TextView advisorstring = (TextView) v.findViewById(R.id.advisorstring);

        if(timeOfDay >= 4 && timeOfDay < 12){
            time = "早晨";
            home_advisor_bg.setBackground(activity.getResources().getDrawable(R.drawable.advisor_light_bg));
            home_advisor_icon.setImageResource(R.drawable.advisor_light_icon);
        }else if(timeOfDay >= 12 && timeOfDay < 19){
            time = "午安";
            home_advisor_bg.setBackground(activity.getResources().getDrawable(R.drawable.advisor_light_bg));
            home_advisor_icon.setImageResource(R.drawable.advisor_light_icon);
        }else{
            time = "晚安";
            home_advisor_bg.setBackground(activity.getResources().getDrawable(R.drawable.advisor_night_bg));
            home_advisor_icon.setImageResource(R.drawable.advisor_night_icon);
        }

        try {
            bodyRecord bodyRecord = new bodyRecord(activity.getBaseContext());
            List<BodyRecord.bodyRecord> AllGLUCOSE = bodyRecord.getAllGLUCOSE();
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            Date lastRecordDate = format1.parse(AllGLUCOSE.get(0).getDate()+" "+AllGLUCOSE.get(0).getTime());
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.HOUR, -12);
            Date now = cal.getTime();
            if (lastRecordDate.after(now)) {
                home_advisor_bg.setVisibility(View.VISIBLE);
                if (AllGLUCOSE.get(0).getType_glucose() == 1) {
                    //Before Meal

                    if (AllGLUCOSE.get(0).getglucose() < 4.0) {
                        mainMsg = "低血糖症，請留意你的低血糖徵狀。";
                        advisor_help_button.setVisibility(View.VISIBLE);
                        advisor_help_button2.setVisibility(View.GONE);


                    }else if(AllGLUCOSE.get(0).getglucose() <= 6.0) {
                        mainMsg = "你的血糖值理想，做得好！";
                        advisor_help_button.setVisibility(View.GONE);
                        advisor_help_button2.setVisibility(View.GONE);


                    }else if(AllGLUCOSE.get(0).getglucose() <= 10.0) {
                        mainMsg = "請繼續努力控糖。";
                        advisor_help_button.setVisibility(View.GONE);
                        advisor_help_button2.setVisibility(View.GONE);


                    }else if(AllGLUCOSE.get(0).getglucose() <= 16.0) {
                        mainMsg = "你的血糖值欠佳，請加倍努力控糖。";
                        advisor_help_button.setVisibility(View.GONE);
                        advisor_help_button2.setVisibility(View.GONE);


                    }else if(AllGLUCOSE.get(0).getglucose() <= 25) {
                        mainMsg = "你的血糖值超高，請安排盡早覆診見醫生。";
                        advisor_help_button.setVisibility(View.GONE);
                        advisor_help_button2.setVisibility(View.GONE);


                    } else {
                        mainMsg = "高血糖症，請盡快找醫護人員診治。";
                        advisor_help_button.setVisibility(View.GONE);
                        advisor_help_button2.setVisibility(View.VISIBLE);

                    }

                }else{
                    //After Meal

                    if (AllGLUCOSE.get(0).getglucose() < 4.0) {
                        mainMsg = "低血糖症，請留意你的低血糖徵狀。";
                        advisor_help_button.setVisibility(View.VISIBLE);
                        advisor_help_button2.setVisibility(View.GONE);

                    }else if(AllGLUCOSE.get(0).getglucose() <= 8.0) {
                        mainMsg = "你的血糖值理想，做得好！";
                        advisor_help_button.setVisibility(View.GONE);
                        advisor_help_button2.setVisibility(View.GONE);

                    }else if(AllGLUCOSE.get(0).getglucose() <= 12.0) {
                        mainMsg = "請繼續努力控糖。";
                        advisor_help_button.setVisibility(View.GONE);
                        advisor_help_button2.setVisibility(View.GONE);

                    }else if(AllGLUCOSE.get(0).getglucose() <= 16.0) {
                        mainMsg = "你的血糖值欠佳，請加倍努力控糖。";
                        advisor_help_button.setVisibility(View.GONE);
                        advisor_help_button2.setVisibility(View.GONE);

                    }else if(AllGLUCOSE.get(0).getglucose() <= 25) {
                        mainMsg = "你的血糖值超高，請安排盡早覆診見醫生。";
                        advisor_help_button.setVisibility(View.GONE);
                        advisor_help_button2.setVisibility(View.GONE);

                    } else {
                        mainMsg = "高血糖症，請盡快找醫護人員診治。";
                        advisor_help_button.setVisibility(View.GONE);
                        advisor_help_button2.setVisibility(View.VISIBLE);
                    }

                }

                advisorstring.setText(time+", "+username+"! "+mainMsg);

            }else{
                home_advisor_bg.setVisibility(View.GONE);
            }

        }catch (Exception e) {

        }



    }
}
