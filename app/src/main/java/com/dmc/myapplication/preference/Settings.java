package com.dmc.myapplication.preference;

import com.dmc.myapplication.R;
import com.dmc.myapplication.login.User;
import com.dmc.myapplication.login.UserLocalStore;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.preference.PreferenceFragment;
import android.util.Log;

import java.util.Map;
import java.util.prefs.Preferences;


/**
 * Created by PoWu on 1/1/16.
 */


public class Settings extends PreferenceFragment {

    private final static String TAG = Settings.class.getName();
    public final static String SP_NAME = TAG + ".SP_NAME";
    private UserLocalStore userLocalStore;
    private User user;
    private String oldCatName;
    private EditTextPreference catName;
    private SharedPreferences.OnSharedPreferenceChangeListener listener;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PreferenceManager prefMgr = getPreferenceManager();
        Log.d("Preference ", "Value of TAG " + TAG);
        Log.d("Preference ", "Value of SP_NAME " + SP_NAME);
        prefMgr.setSharedPreferencesName(SP_NAME);
        addPreferencesFromResource(R.xml.pref_general);

        SharedPreferences sp = prefMgr.getDefaultSharedPreferences(getActivity().getApplicationContext());


        userLocalStore = new UserLocalStore(getActivity().getApplicationContext());
        User user = userLocalStore.getLoggedInUser();
        Log.d("Preference ", "Value of user.name " +  user.name);
        Log.d("Preference ", "Value of preference name " + findPreference("name"));

        //get preferences
        bindPreferenceSummaryToValue(prefMgr.findPreference("name"));
        bindPreferenceSummaryToValue(prefMgr.findPreference("sex"));
        bindPreferenceSummaryToValue(prefMgr.findPreference("smoker"));
        bindPreferenceSummaryToValue(prefMgr.findPreference("height_unit"));
        bindPreferenceSummaryToValue(prefMgr.findPreference("weight_unit"));
        bindPreferenceSummaryToValue(prefMgr.findPreference("waist_unit"));





    }


    /**
     * A preference value change listener that updates the preference's summary
     * to reflect its new value.
     */
    private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object value) {

            String stringValue = value.toString();

            if (preference instanceof ListPreference) {
                // For list preferences, look up the correct display value in
                // the preference's 'entries' list.
                ListPreference listPreference = (ListPreference) preference;
                int index = listPreference.findIndexOfValue(stringValue);

                // Set the summary to reflect the new value.
                preference.setSummary(
                        index >= 0
                                ? listPreference.getEntries()[index]
                                : null);

            }  else {
                // For all other preferences, set the summary to the value's
                // simple string representation.
                preference.setSummary(stringValue);
            }
            return true;
        }
    };


    /**
     * Binds a preference's summary to its value. More specifically, when the
     * preference's value is changed, its summary (line of text below the
     * preference title) is updated to reflect the value. The summary is also
     * immediately updated upon calling this method. The exact display format is
     * dependent on the type of preference.
     *
     * @see #sBindPreferenceSummaryToValueListener
     */
    private static void bindPreferenceSummaryToValue(Preference preference) {
        // Set the listener to watch for value changes.
        preference
                .setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);


        if (preference instanceof ListPreference)
        {
            // Trigger the listener immediately with the preference's
            // current value.
            sBindPreferenceSummaryToValueListener.onPreferenceChange(
                    preference,
                    PreferenceManager.getDefaultSharedPreferences(
                            preference.getContext()).getInt(preference.getKey(),0));
        }
        else if (preference instanceof EditTextPreference)
        {
            // Trigger the listener immediately with the preference's
            // current value.
            sBindPreferenceSummaryToValueListener.onPreferenceChange(
                    preference,
                    PreferenceManager.getDefaultSharedPreferences(
                            preference.getContext()).getString(preference.getKey(),""));
        }
    }






}
