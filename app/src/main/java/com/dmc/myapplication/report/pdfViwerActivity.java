package com.dmc.myapplication.report;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.dmc.myapplication.R;
import com.github.barteksc.pdfviewer.PDFView;

import java.io.File;

public class pdfViwerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf_viwer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        PDFView pdfView = (PDFView) findViewById(R.id.pdfView);

        File file = new File(getExternalFilesDir(null), "report.pdf");
        pdfView.fromFile(file)
                .enableSwipe(true)
                .swipeHorizontal(false)
                .enableDoubletap(true)
                .defaultPage(0)
                .load();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                handleBackAction();
                return true;

            case R.id.report_share_button:
                File file = new File(getExternalFilesDir(null), "report.pdf");
                //Uri path = Uri.fromFile(file);
                //Intent emailIntent = new Intent(Intent.ACTION_SEND);
                //emailIntent.setType("vnd.android.cursor.dir/email");
                //emailIntent .putExtra(Intent.EXTRA_STREAM, path);
                //emailIntent .putExtra(Intent.EXTRA_SUBJECT, "我的控糖式記錄報告");
                //startActivity(Intent.createChooser(emailIntent , "傳送電郵..."));
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW).setDataAndType(Uri.fromFile(file), "application/pdf"));
                }
                catch (ActivityNotFoundException e) {
                    Toast.makeText(this,
                            "你沒有其他應用程式能夠讀取 PDF 檔案。",
                            Toast.LENGTH_SHORT).show();
                }
                break;

            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void handleBackAction(){
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.report_pdf_viewer, menu);
        return super.onCreateOptionsMenu(menu);

    }


}
