package com.dmc.myapplication.report;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import com.dmc.myapplication.R;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.systemConstant;
import com.dmc.myapplication.webHelper;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.ProgressCallback;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class downloadPDFDialog extends android.app.DialogFragment {


    public downloadPDFDialog() {
        // Required empty public constructor
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_download_pdfdialog, null);

        ArrayList<String> years = new ArrayList<String>();
        int thisYear = Calendar.getInstance().get(Calendar.YEAR);
        int thisMonth = Calendar.getInstance().get(Calendar.MONTH);
        for (int i = 1900; i <= thisYear; i++) {
            years.add(Integer.toString(i));
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, years);

        ArrayList<String> months = new ArrayList<String>();
        for (int i = 1; i <= 12; i++) {
            months.add(Integer.toString(i));
        }
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, months);

        final Spinner spinYear1 = (Spinner)view.findViewById(R.id.spinner_report_1);
        spinYear1.setAdapter(adapter);
        spinYear1.setSelection(years.size() -1);

        final Spinner spinYear2 = (Spinner)view.findViewById(R.id.spinner3);
        spinYear2.setAdapter(adapter);
        spinYear2.setSelection(years.size() -1);

        final Spinner spinMonth1 = (Spinner)view.findViewById(R.id.spinner2);
        spinMonth1.setAdapter(adapter2);
        spinMonth1.setSelection(0);

        final Spinner spinMonth2 = (Spinner)view.findViewById(R.id.spinner4);
        spinMonth2.setAdapter(adapter2);
        spinMonth2.setSelection(thisMonth);

        final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBar);


        builder.setView(view)
                // Add action buttons
                .setPositiveButton("Submit",
                        new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int id)
                            {
                               reportActivity callingActivity = (reportActivity) getActivity();
                                callingActivity.onUserSelectValue(spinYear1.getSelectedItem().toString(), spinMonth1.getSelectedItem().toString(),
                                        spinYear2.getSelectedItem().toString(), spinMonth2.getSelectedItem().toString());
                                dialog.dismiss();

                            }
                        }).setNegativeButton("Cancel", null);

        return builder.create();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        /*View view = inflater.inflate(R.layout.fragment_download_pdfdialog, container, false);

        Spinner spinYear = (Spinner)view.findViewById(R.id.yearspin);*/

        return super.onCreateView(inflater, container, savedInstanceState);
    }
}
