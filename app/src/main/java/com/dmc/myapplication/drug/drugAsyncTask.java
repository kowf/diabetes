package com.dmc.myapplication.drug;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.dmc.myapplication.Models.INJECTION_RECORD;
import com.dmc.myapplication.Models.MEDICATION_RECORD;
import com.dmc.myapplication.dmAppAsyncTask;
import com.dmc.myapplication.noInternetConnectionDialog;
import com.dmc.myapplication.serverNoResponseDialog;

import java.net.SocketTimeoutException;
import java.net.URLEncoder;

/**
 * Created by KwokSinMan on 7/2/2016.
 */
public class drugAsyncTask extends AsyncTask<String,Void,String> {

    private ProgressDialog progressBar;
    private String model;
    private Activity activity;
    private Fragment fragment;

    public drugAsyncTask(Activity activity, Fragment fragment, String model ) {
        this.activity = activity;
        this.fragment = fragment;
        this.model = model;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressBar = new ProgressDialog(activity);
        progressBar.setCancelable(false);
        progressBar.setTitle("載入中...");
        progressBar.setMessage("請稍候 !!");
        progressBar.show();
    }

    @Override
    protected String doInBackground(String... arg0) {
        String data="";
        try {
            if (model.equals(drugConstant.GET_DRUG_RECORD)){
                String userId = (String)arg0[0];
                String drugDate = (String)arg0[1];
                data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(userId, "UTF-8");
                data += "&" + URLEncoder.encode("drug_date", "UTF-8") + "=" + URLEncoder.encode(drugDate, "UTF-8");
                return drugDate;
            }

            if (model.equals(drugConstant.CHANGE_DRUG_RECORD)){
                String userId = (String)arg0[0];
                String drugDate = (String)arg0[1];
                String drugAns = (String)arg0[2];
                data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(userId, "UTF-8");
                data += "&" + URLEncoder.encode("drug_date", "UTF-8") + "=" + URLEncoder.encode(drugDate, "UTF-8");
                data += "&" + URLEncoder.encode("drug_ans", "UTF-8") + "=" + URLEncoder.encode(drugAns, "UTF-8");

                MEDICATION_RECORD medication_record = new MEDICATION_RECORD(activity.getBaseContext());
                MEDICATION_RECORD.MEDICATION_RECORD_class record = medication_record.getByDate(drugDate);

                if (record != null){
                    record.setHAS_TAKE_MEDICATION(drugAns);
                    medication_record.update(record);
                }else {
                    medication_record.insert(medication_record.new MEDICATION_RECORD_class(0, Integer.parseInt(userId), drugDate, drugAns));
                }

                medication_record.close();

                return drugDate+"||"+drugAns;
            }
            if(model.equals(drugConstant.DELETE_DRUG_RECORD)){
                String userId = (String)arg0[0];
                String drugDate = (String)arg0[1];
                data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(userId, "UTF-8");
                data += "&" + URLEncoder.encode("drug_date", "UTF-8") + "=" + URLEncoder.encode(drugDate, "UTF-8");

                MEDICATION_RECORD medication_record = new MEDICATION_RECORD(activity.getBaseContext());
                medication_record.deleteByDate(drugDate);
                medication_record.close();
                return "{}";
            }
            if(model.equals(drugConstant.ADD_INJECTION_RECORD)){
                String userId = (String)arg0[0];
                String injectionDate = (String)arg0[1];
                String injectionTime = (String)arg0[2];
                String injectionName = (String)arg0[3];
                String injectionValue = (String)arg0[4];
                data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(userId, "UTF-8");
                data += "&" + URLEncoder.encode("injection_date", "UTF-8") + "=" + URLEncoder.encode(injectionDate, "UTF-8");
                data += "&" + URLEncoder.encode("injection_time", "UTF-8") + "=" + URLEncoder.encode(injectionTime, "UTF-8");
                data += "&" + URLEncoder.encode("injection_name", "UTF-8") + "=" + URLEncoder.encode(injectionName, "UTF-8");
                data += "&" + URLEncoder.encode("injection_value", "UTF-8") + "=" + URLEncoder.encode(injectionValue, "UTF-8");

                INJECTION_RECORD injection_record = new INJECTION_RECORD(activity.getBaseContext());
                injection_record.insert(injection_record.new INJECTION_RECORD_class(0, Integer.parseInt(userId), injectionDate, injectionTime, injectionName, Integer.parseInt(injectionValue)));
                injection_record.close();

                return "{}";
            }
            if(model.equals(drugConstant.CHANGE_INJECTION_RECORD)){
                String userId = (String)arg0[0];
                String injectionRecordId = (String)arg0[1];
                String injectionDate = (String)arg0[2];
                String injectionTime = (String)arg0[3];
                String injectionName = (String)arg0[4];
                String injectionValue = (String)arg0[5];
                data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(userId, "UTF-8");
                data += "&" + URLEncoder.encode("injection_record_id", "UTF-8") + "=" + URLEncoder.encode(injectionRecordId, "UTF-8");
                data += "&" + URLEncoder.encode("injection_date", "UTF-8") + "=" + URLEncoder.encode(injectionDate, "UTF-8");
                data += "&" + URLEncoder.encode("injection_time", "UTF-8") + "=" + URLEncoder.encode(injectionTime, "UTF-8");
                data += "&" + URLEncoder.encode("injection_name", "UTF-8") + "=" + URLEncoder.encode(injectionName, "UTF-8");
                data += "&" + URLEncoder.encode("injection_value", "UTF-8") + "=" + URLEncoder.encode(injectionValue, "UTF-8");

                INJECTION_RECORD injection_record = new INJECTION_RECORD(activity.getBaseContext());
                INJECTION_RECORD.INJECTION_RECORD_class record = injection_record.get(Integer.parseInt(injectionRecordId));
                record.setINJECTION_DATE(injectionDate);
                record.setINJECTION_TIME(injectionTime);
                record.setINJECTION_NAME(injectionName);
                record.setINJECTION_VALUE(Integer.parseInt(injectionValue));
                injection_record.update(record);

                return "{}";

            }
            if(model.equals(drugConstant.DELETE_INJECTION_RECORD)){
                String userId = (String)arg0[0];
                String injectionRecordId = (String)arg0[1];
                data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(userId, "UTF-8");
                data += "&" + URLEncoder.encode("injection_record_id", "UTF-8") + "=" + URLEncoder.encode(injectionRecordId, "UTF-8");

                INJECTION_RECORD injection_record = new INJECTION_RECORD(activity.getBaseContext());
                injection_record.delete(Integer.parseInt(injectionRecordId));
                injection_record.close();

                return "{}";

            }

            //get result  from server
            //dmAppAsyncTask dmasyncTask = new dmAppAsyncTask(model);
            return "{}";
            //return dmasyncTask.getDataFromServer(data);
        //}  //catch (SocketTimeoutException e){
           // e.printStackTrace();
           // return dmAppAsyncTask.SERVER_SLEEP;
        }catch (Exception e) {
            e.printStackTrace();
            return dmAppAsyncTask.NO_INTERNET_CONNECTION;
        }
    }


    @Override
    protected void onPostExecute(String result){
        if (result!=null) {
            if(result.equals(dmAppAsyncTask.NO_INTERNET_CONNECTION)){
                progressBar.dismiss();
                noInternetConnectionDialog dialog = new noInternetConnectionDialog();
                dialog.show(this.activity.getFragmentManager(), "NoInternetConnectionDialog");
            }else if(result.equals(dmAppAsyncTask.SERVER_SLEEP)){
                progressBar.dismiss();
                serverNoResponseDialog dialog = new serverNoResponseDialog();
                dialog.show(this.activity.getFragmentManager(), "NoServerResponseDialog");
            }else {

                drugBiz drugBiz = new drugBiz();
                if (model.equals(drugConstant.GET_DRUG_RECORD)) {
                    drugBiz.setDrugRecordNew(this.activity, this.fragment, "{}", result);
                }
                if (model.equals(drugConstant.CHANGE_DRUG_RECORD)){
                    drugBiz.afterSaveDrugRecord(this.activity);
                }
                if (model.equals(drugConstant.DELETE_DRUG_RECORD)){
                    drugBiz.afterSaveDrugRecord(this.activity);
                }
                if(model.equals(drugConstant.ADD_INJECTION_RECORD)){
                    drugBiz.afterSaveInjectionRecord(this.activity, false);
                }
                if(model.equals(drugConstant.CHANGE_INJECTION_RECORD)){
                    drugBiz.afterSaveInjectionRecord(this.activity, false);
                }
                if(model.equals(drugConstant.DELETE_INJECTION_RECORD)){
                    drugBiz.afterSaveInjectionRecord(this.activity,true);
                }
                progressBar.dismiss();
            }
       }
    }

}
