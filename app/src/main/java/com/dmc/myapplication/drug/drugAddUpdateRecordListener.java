package com.dmc.myapplication.drug;

import android.app.Activity;
import android.support.v7.widget.TintContextWrapper;
import android.view.View;
import android.widget.RadioGroup;

import com.dmc.myapplication.R;
import com.dmc.myapplication.login.UserLocalStore;

/**
 * Created by KwokSinMan on 8/3/2016.
 */
public class drugAddUpdateRecordListener implements View.OnClickListener{
    @Override
    public void onClick(View arg0) {
        Activity activity = getActivityContentView(arg0);
        UserLocalStore userLocalStore = new UserLocalStore(arg0.getContext());
        String userId = Integer.toString(userLocalStore.getLoggedInUser().userid);

        String drugDate =  activity.getIntent().getExtras().getString(drugConstant.GET_DRUG_DATE);
        int checkedId =  ((RadioGroup) activity.findViewById(R.id.question)).getCheckedRadioButtonId();
        drugAsyncTask connect = new drugAsyncTask(activity, null, drugConstant.CHANGE_DRUG_RECORD);
        if (checkedId == R.id.ansY) {
            connect.execute(userId, drugDate, drugConstant.ANS_YES);
        } else if (checkedId == R.id.ansN) {
            connect.execute(userId, drugDate, drugConstant.ANS_NO);
        }
    }

    public Activity getActivityContentView(View arg0){
        try {
            if (arg0.getContext() instanceof TintContextWrapper){
                return ((Activity) ((TintContextWrapper) arg0.getContext()).getBaseContext());
            }else{
                return ((Activity) arg0.getContext());
            }
        }catch (ClassCastException e){
            throw new ClassCastException("Cast Error");
        }
    }
}
