package com.dmc.myapplication.drug;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.dmc.myapplication.R;

/**
 * Created by KwokSinMan on 15/3/2016.
 */
public class injectionNumberPickerFragment extends DialogFragment implements DialogInterface.OnClickListener {


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final NumberPicker gymValuePick = new NumberPicker(getActivity());
        gymValuePick.setMaxValue(180);
        gymValuePick.setMinValue(1);
        TextView gymValue = (TextView) getActivity().findViewById(R.id.injectionValue);
        gymValuePick.setValue(Integer.parseInt((String) gymValue.getText()));
        gymValuePick.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        gymValuePick.setWrapSelectorWheel(false);


        return new AlertDialog.Builder(getActivity())
                .setTitle("注射劑量")
                .setView(gymValuePick)
                .setPositiveButton("確定",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                TextView gymValue = (TextView) getActivity().findViewById(R.id.injectionValue);
                                int oldGymValue = Integer.parseInt((String) gymValue.getText());
                                if (gymValuePick.getValue() != oldGymValue) {
                                    gymValue.setText(String.valueOf(gymValuePick.getValue()));
                                }
                            }
                        })
                .setNegativeButton("取消",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                            }
                        })
                .create();
    }

    @Override
    public void onClick(DialogInterface dialog, int whichButton){}
}