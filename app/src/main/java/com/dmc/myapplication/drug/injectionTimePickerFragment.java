package com.dmc.myapplication.drug;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.TimePicker;

import com.dmc.myapplication.R;
import com.dmc.myapplication.gym.gymDateTimeTool;

/**
 * Created by KwokSinMan on 15/3/2016.
 */
public class injectionTimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String displayTime = (String)((TextView) getActivity().findViewById(R.id.injectionTime)).getText();
        gymDateTimeTool displayDateTimeObject = new gymDateTimeTool(displayTime);
        int hour = displayDateTimeObject.getHour();
        int min = displayDateTimeObject.getMin();
        return new TimePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, this,hour,min,true);
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        gymDateTimeTool gymTime = new gymDateTimeTool();
        String displayTime = gymTime.getTimeString(hourOfDay)+":"+gymTime.getTimeString(minute);
        ((TextView) getActivity().findViewById(R.id.injectionTime)).setText(displayTime);
    }
}