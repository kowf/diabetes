package com.dmc.myapplication.drug;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.TintContextWrapper;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.R;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.systemConstant;
import com.dmc.myapplication.tool.dateTool;
import com.dmc.myapplication.tool.timeObject;

import java.util.Calendar;

/**
 * Created by KwokSinMan on 15/3/2016.
 */
public class injectionAddActivty extends AppCompatActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.injection_add);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // hidden other session
        LinearLayout editRecordButtonSession = (LinearLayout) findViewById(R.id.editRecordButtonSession);
        editRecordButtonSession.setVisibility(View.GONE);

        // set Date
        String drugDate = getIntent().getExtras().getString(drugConstant.GET_DRUG_DATE);
        TextView drugDateTextView = (TextView)findViewById(R.id.injectionDate);
        drugDateTextView.setText(drugDateTool.getUIDateFormal(drugDate));

        //set gym Time
        TextView injectionTime = (TextView)findViewById(R.id.injectionTime);
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int min = calendar.get(Calendar.MINUTE);
        timeObject timeObject = new timeObject();
        injectionTime.setText(timeObject.getTimeString(hour) + ":" + timeObject.getTimeString(min));

        // set data picker
        ImageView setInjectionDate = (ImageView) findViewById(R.id.setInjectionDate);
        setInjectionDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new injectionDatePickerFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        // set time  picker
        ImageView setInjectionTime = (ImageView) findViewById(R.id.setInjectionTime);
        setInjectionTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new injectionTimePickerFragment();
                newFragment.show(getFragmentManager(), "TimePicker");
            }
        });

        //set value
        TextView injectionValue = (TextView) findViewById(R.id.injectionValue);
        injectionValue.setText("1");

        // set number  picker
        ImageView setInjectionValue = (ImageView) findViewById(R.id.setInjectionValue);
        setInjectionValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new injectionNumberPickerFragment();
                newFragment.show(getFragmentManager(), "NumberPicker");
            }
        });

        EditText injectionNameEditText = (EditText) findViewById(R.id.injectionName);
        injectionNameEditText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                String drugName = ((EditText) findViewById(R.id.injectionName)).getText().toString();
                // System.out.println("Candy drugNameEditText="+drugName);
                Button addButton = (Button) findViewById(R.id.injectionAdd);
                if (drugName.isEmpty()) {
                    addButton.setEnabled(false);
                } else {
                    addButton.setEnabled(true);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        Button addButton = (Button) findViewById(R.id.injectionAdd);
        addButton.setEnabled(false);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserLocalStore userLocalStore = new UserLocalStore(getActivityContentView(view));
                String userId = Integer.toString(userLocalStore.getLoggedInUser().userid);
                String injectionDate = dateTool.getDBDateFormal((String) ((TextView) findViewById(R.id.injectionDate)).getText());
                String injectionTime = (String) ((TextView) findViewById(R.id.injectionTime)).getText();
                String injectionName =  ((EditText) findViewById(R.id.injectionName)).getText().toString();
                String injectionValue = (String) ((TextView) findViewById(R.id.injectionValue)).getText();

                drugAsyncTask connect = new drugAsyncTask(getActivityContentView(view),null, drugConstant.ADD_INJECTION_RECORD);
                connect.execute(userId,injectionDate,injectionTime,injectionName,injectionValue);

            }
        });


        // SET return <- into left hand side
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public Activity getActivityContentView(View arg0){
        try {
            if (arg0.getContext() instanceof TintContextWrapper){
                return ((Activity) ((TintContextWrapper) arg0.getContext()).getBaseContext());
            }else{
                return ((Activity) arg0.getContext());
            }
        }catch (ClassCastException e){
            throw new ClassCastException("Cast Error");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() ==  android.R.id.home){
            handleBackAction();
        }
        return true;
    }

    // handle back button in android
    @Override
    public void onBackPressed() {
        handleBackAction();
    }

    private void handleBackAction(){
        String getDrugDate = getIntent().getExtras().getString(drugConstant.GET_DRUG_DATE);
        Intent intent = new Intent();
        intent.setClass(this, MainActivity.class);
        intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_DRUG);
        intent.putExtra(drugConstant.GET_DRUG_DATE, getDrugDate);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        finish();
    }
}
