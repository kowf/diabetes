package com.dmc.myapplication.drug;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.R;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.systemConstant;

import java.util.Calendar;

/**
 * Created by KwokSinMan on 29/2/2016.
 */
public class drugAddActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(R.anim.slide1, R.anim.slide2);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drug_add);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // set Date
        String drugDate = getIntent().getExtras().getString(drugConstant.GET_DRUG_DATE);
        TextView drugDateTextView = (TextView)findViewById(R.id.drugViewDate);
        drugDateTextView.setText(drugDateTool.getUIDateFormal(drugDate));

        //set defult radio button
        RadioGroup questionRadioGroup = (RadioGroup) findViewById(R.id.question);
        questionRadioGroup.setOnCheckedChangeListener(new radioOnCheckedChangeListener(this));

        // hidden other session
        //LinearLayout drugAddArea = (LinearLayout) findViewById(R.id.drugAddArea);
        //drugAddArea.setVisibility(View.GONE);
        LinearLayout noDrugRecord = (LinearLayout) findViewById(R.id.noDrugRecord);
        noDrugRecord.setVisibility(View.GONE);
        LinearLayout editRecordButtonSession = (LinearLayout) findViewById(R.id.editRecordButtonSession);
        editRecordButtonSession.setVisibility(View.GONE);
        ImageView setDrugDate = (ImageView) findViewById(R.id.setDrugViewDate);
        setDrugDate.setVisibility(View.GONE);

        // set broder of drug recod
        LinearLayout drugRecord = (LinearLayout) findViewById(R.id.drugDetail);
        drugRecord.setBackgroundResource(R.drawable.border_drug_normal);


       /*
        setDrugDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new drugDatePickerFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });
*/
        Button drugAddFinish = (Button) findViewById(R.id.drugAddFinish);
        drugAddFinish.setEnabled(false);
        drugAddFinish.setOnClickListener(new drugAddUpdateRecordListener());

        // SET return <- into left hand side
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() ==  android.R.id.home){
            handleBackAction();
        }
        return true;
    }

    // handle back button in android
    @Override
    public void onBackPressed() {
        handleBackAction();
    }

    private void handleBackAction(){
        String getDrugDate = getIntent().getExtras().getString(drugConstant.GET_DRUG_DATE);
        Intent intent = new Intent();
        intent.setClass(this, MainActivity.class);
        intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_DRUG);
        intent.putExtra(drugConstant.GET_DRUG_DATE,getDrugDate);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        finish();
    }


    private class radioOnCheckedChangeListener implements RadioGroup.OnCheckedChangeListener {
        Activity activity;
        public radioOnCheckedChangeListener(Activity activity){ this.activity=activity; }

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            Button drugAddFinish = (Button) activity.findViewById(R.id.drugAddFinish);
            drugAddFinish.setEnabled(true);
        }
    }



    private class drugDatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            String displayDate = (String)((TextView) getActivity().findViewById(R.id.drugViewDate)).getText();
            String [] displayDateArray = displayDate.split("/");
            int yy = Integer.parseInt(displayDateArray[2]);
            int mm = Integer.parseInt(displayDateArray[1]) - 1; // because Jan =0
            int dd = Integer.parseInt(displayDateArray[0]);
            DatePickerDialog dialog =  new DatePickerDialog(getActivity(), this, yy,mm,dd);
            dialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
            return dialog;
        }

        // On the date picker , to confirm the following action
        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            // set display date
            mm = mm + 1; // because Jan =0
            String displayAsDate = dd + "/" + mm + "/" + yy;
            ((TextView) getActivity().findViewById(R.id.drugViewDate)).setText(displayAsDate);
        }
    }

}
