package com.dmc.myapplication.phyActivityRecord.helper;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

/**
 * Created by terry on 9/1/2018.
 */

public class PhyStrengthInfoDialog {
    public static void getDialog(Context ctx){
        AlertDialog alertDialog = new AlertDialog.Builder(ctx).create();
        alertDialog.setTitle("活動強度指南");
        alertDialog.setMessage("低等強度活動： 簡單、輕量、容易應付的活動\n\n" +
                "中等強度活動：進行這煩活動時，仍能說話但不能唱歌\n\n" +
                "劇烈活動：除非停下來呼吸，否則說不完 整個句子"
        );
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "確定",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }
}
