package com.dmc.myapplication.phyActivityRecord.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.dmc.myapplication.phyActivityRecord.service.PhyUploadIntentService;

public class BootCompleteReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("BootCompleteReceiver", "-");
        PhyUploadIntentService.startUploadAll();
    }
}
