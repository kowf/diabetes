package com.dmc.myapplication.phyActivityRecord.helper;

import com.dmc.myapplication.MyApplication;
import com.dmc.myapplication.phyActivityRecord.db.PhyActivity;
import com.dmc.myapplication.phyActivityRecord.db.PhyActivityRecord;
import com.dmc.myapplication.phyActivityRecord.db.PhyActivityRecordForUpload;
import com.dmc.myapplication.phyActivityRecord.db.dao.PhyActivityDao;
import com.dmc.myapplication.phyActivityRecord.db.dao.PhyActivityRecordDao;
import com.dmc.myapplication.phyActivityRecord.db.dao.PhyActivityRecordForUploadDao;
import com.dmc.myapplication.phyActivityRecord.service.PhyUploadIntentService;

import org.apache.commons.lang.time.DateUtils;
import org.greenrobot.greendao.query.WhereCondition;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by terry on 11/1/2018.
 */

public class PhyDBHelper {
    // Phy Record
    private static PhyActivityRecordDao phyActivityRecordDao = MyApplication.getApplication().getDaoSession().getPhyActivityRecordDao();

    // Custom Phy Activity
    private static PhyActivityDao phyActivityDao = MyApplication.getApplication().getDaoSession().getPhyActivityDao();

    // Phy Record For Upload
    private static PhyActivityRecordForUploadDao phyActivityRecordForUploadDao = MyApplication.getApplication().getDaoSession().getPhyActivityRecordForUploadDao();

    private static int userId = UserHelper.getId(MyApplication.getApplication().getApplicationContext());

    //
    // Phy Record
    public static List<PhyActivityRecord> getRecordExistsDays() {
        return phyActivityRecordDao.queryBuilder().where(PhyActivityRecordDao.Properties.UserId.eq(userId), new WhereCondition.StringCondition("1 GROUP BY date")).list();
    }
//
//    public static void setUserId(int userId) {
//        this.userId = userId;
//    }

    public static Date isRecordExists(Date date) {
        Date d = DateUtils.truncate(date, Calendar.DATE);
        System.out.println("userId: " + userId);
        List<PhyActivityRecord> result = phyActivityRecordDao.queryBuilder().where(PhyActivityRecordDao.Properties.UserId.eq(userId), PhyActivityRecordDao.Properties.Date.eq(d)).limit(1).list();
        if (result.size() > 0) {
            return result.get(0).getDate();
        }
        return null;
    }

    // Insert data
    public static void insertRecord(PhyActivityRecord phyActivity) {
        // get dao
        phyActivity.setUserId(userId);
        phyActivityRecordDao.insert(phyActivity);
        insertUploadRecord(phyActivityToPhyActivityForUpload(phyActivity, PhyActivityRecordForUpload.ActionEnum.INSERT));
    }

    public static void deleteRecord(long id) {

        PhyActivityRecord r = getRecord(id);
        if (r != null) {
            insertUploadRecord(phyActivityToPhyActivityForUpload(r, PhyActivityRecordForUpload.ActionEnum.DELETE));
        }

        phyActivityRecordDao.deleteByKey(id);
    }

    public static PhyActivityRecord getRecord(long id) {
        return phyActivityRecordDao.load(id);

    }

    public static List<PhyActivityRecord> getRecords(Date date) {
        Date d = DateUtils.truncate(date, Calendar.DATE);
//        Log.d("PhyMainGet", d.toString());
//        Log.d("PhyMainGet", date.toString());
        return phyActivityRecordDao.queryBuilder().where(PhyActivityRecordDao.Properties.UserId.eq(userId), PhyActivityRecordDao.Properties.Date.eq(d)).orderDesc(PhyActivityRecordDao.Properties.Id).list();
    }

    public static List<PhyActivityRecord> getRecords(Date from, Date to) {
        //Log.d("統計", DateUtils.truncate(from, Calendar.DATE) + "  " + DateUtils.truncate(to, Calendar.DATE));
        return phyActivityRecordDao.queryBuilder().where(PhyActivityRecordDao.Properties.UserId.eq(userId), PhyActivityRecordDao.Properties.Date.ge(DateUtils.truncate(from, Calendar.DATE)), PhyActivityRecordDao.Properties.Date.lt(DateUtils.truncate(to, Calendar.DATE))).list();
    }

    //
    // Custom Activity

    public static void insertCustomActivity(PhyActivity activity) {
        phyActivityDao.insert(activity);

    }

    public static List<PhyActivity> getAllCustomActivities() {
        return phyActivityDao.loadAll();
    }

    public static void deleteCustomActivity(long id) {
        phyActivityDao.deleteByKey(id);
    }

    public static void insertUploadRecord(PhyActivityRecordForUpload phyActivity) {
        phyActivityRecordForUploadDao.insert(phyActivity);
        PhyUploadIntentService.startUpload(phyActivity);
    }

    public static List<PhyActivityRecordForUpload> getUploadRecord() {
        return phyActivityRecordForUploadDao.loadAll();
    }

    public static void deleteUploadRecord(long id) {
        phyActivityRecordForUploadDao.deleteByKey(id);
    }

    public static PhyActivityRecordForUpload phyActivityToPhyActivityForUpload(PhyActivityRecord phyActivity, PhyActivityRecordForUpload.ActionEnum action) {
        return new PhyActivityRecordForUpload(
                null,
                phyActivity.getUserId(),
                phyActivity.getActivity(),
                phyActivity.getStrength(),
                phyActivity.getCategory(),
                phyActivity.getDuration(),
                action.ordinal());
    }

}
