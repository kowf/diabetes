package com.dmc.myapplication.phyActivityRecord;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dmc.myapplication.R;
import com.dmc.myapplication.phyActivityRecord.db.PhyActivityRecord;
import com.dmc.myapplication.phyActivityRecord.helper.PhyActivityStrengthEnum;
import com.dmc.myapplication.phyActivityRecord.helper.PhyDBHelper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class PhyActivityDayRecord extends AppCompatActivity {
    Toolbar toolbar;
    android.support.design.widget.SubtitleCollapsingToolbarLayout collapsingToolbarLayout;
    RecyclerView rvPhyDayRecord;
    FloatingActionButton fabAdd;
    PhyDayRecordCVAdapter mAdapter;
    Date date;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phy_day_record);

        // Get intent message
        //
        Intent intent = getIntent();
        date = (Date)intent.getSerializableExtra(MainActivity.EXTRA_DATE);

        // Init fab
        //
        fabAdd = findViewById(R.id.fcb_phy_day_new_record);
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CreatePhyRecordActivity.class);
                intent.putExtra(MainActivity.EXTRA_DATE, date);
                PhyActivityDayRecord.this.startActivityForResult(intent, 1);
            }
        });

        // Init toolbar
        //
        collapsingToolbarLayout = findViewById(R.id.ctb_phy_day_record);
        collapsingToolbarLayout.setTitle("體能活動記錄");
        collapsingToolbarLayout.setSubtitle(new SimpleDateFormat("yyyy年MM月dd日", Locale.TAIWAN).format(date));

        toolbar = findViewById(R.id.tb_phy_day_record);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Init recycle view
        //
        rvPhyDayRecord = findViewById(R.id.rv_phy_day_record);
        rvPhyDayRecord.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rvPhyDayRecord.setLayoutManager(llm);

        List<PhyActivityRecord> records = PhyDBHelper.getRecords(date);
        mAdapter = new PhyDayRecordCVAdapter(records.toArray(new PhyActivityRecord[records.size()]));

        mAdapter.setOnItemClickListener(new PhyDayRecordCVAdapter.OnItemClickListener() {
            Toast toast;
            @Override
            public void onItemClick(View view, PhyActivityRecord obj) {
                if(toast != null) {
                    toast.cancel();
                }

                toast = Toast.makeText(view.getContext(), "長按刪除活動", Toast.LENGTH_LONG);
                toast.show();
            }

            @Override
            public void onItemLongClick(View view, final PhyActivityRecord obj) {
                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(PhyActivityDayRecord.this, android.R.style.Theme_Material_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(PhyActivityDayRecord.this);
                }
                builder.setTitle("刪除活動")
                        .setMessage("是否確定刪除活動: " + obj.getActivity() + "?")
                        .setNeutralButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                                PhyDBHelper.deleteRecord(obj.getId());
                                PhyActivityDayRecord.this.refreshList();
                            }
                        })
                        .setPositiveButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });

        rvPhyDayRecord.setAdapter(mAdapter);
        rvPhyDayRecord.setVerticalScrollBarEnabled(true);
        rvPhyDayRecord.setNestedScrollingEnabled(false);

    }

    private void refreshList(){
        // refresh view
        List<PhyActivityRecord> records = PhyDBHelper.getRecords(date);
        mAdapter.setData(records.toArray(new PhyActivityRecord[records.size()]));
        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //Log.d("onActivityResult", requestCode + "," + resultCode);
        if (requestCode == 1) {
            PhyActivityDayRecord.this.refreshList();
        }
    }
}

// For recycle view data display
class PhyDayRecordCVAdapter extends RecyclerView.Adapter<PhyDayRecordCVAdapter.ViewHolder> {

    public interface OnItemClickListener {
        void onItemClick(View view, PhyActivityRecord obj);
        void onItemLongClick(View view, PhyActivityRecord obj);
    }

    private PhyActivityRecord[] mDataset;
    private OnItemClickListener onItemClickListener;

    // Provide a suitable constructor (depends on the kind of dataset)
    public PhyDayRecordCVAdapter(PhyActivityRecord[] myDataset) {
        setData(myDataset);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public PhyDayRecordCVAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                            int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.vh_phy_day_record_item, parent, false);

        ViewHolder vh = new ViewHolder(v);

        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final int p = position;

        // set data
        holder.mTvActivity.setText(mDataset[position].getActivity());
        holder.mTvStrength.setText(PhyActivityStrengthEnum.getName(mDataset[position].getStrength()));
        holder.mTvDuration.setText(mDataset[position].getDuration().toString() + "分鐘");
        holder.mRlStrength.setBackgroundColor(PhyActivityStrengthEnum.getColor(mDataset[position].getStrength()));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onItemClickListener != null){
                    onItemClickListener.onItemClick(v, mDataset[p]);
                }
            }
        });

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if(onItemClickListener != null){
                    onItemClickListener.onItemLongClick(v, mDataset[p]);
                }
                return false;
            }
        });
    }

    public void setData(PhyActivityRecord[] myDataset){
        mDataset = myDataset;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.length;
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView mTvActivity;
        public TextView mTvStrength;
        public TextView mTvDuration;
        public RelativeLayout mRlStrength;
        public ViewHolder(View v) {
            super(v);
            mTvActivity = v.findViewById(R.id.tv_vh_phy_day_record_activity);
            mTvStrength = v.findViewById(R.id.tv_vh_phy_day_record_strength);
            mTvDuration = v.findViewById(R.id.tv_vh_phy_day_record_duration);
            mRlStrength = v.findViewById(R.id.rl_vh_phy_day_record_strength_color);
        }
    }

}

