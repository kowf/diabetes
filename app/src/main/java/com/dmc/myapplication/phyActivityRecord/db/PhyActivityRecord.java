package com.dmc.myapplication.phyActivityRecord.db;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

import java.util.Date;

/**
 * Created by terry on 2/1/2018.
 */

@Entity(nameInDb = "phy_activity_record")
public class PhyActivityRecord {
    @Id(autoincrement = true)
    private Long id;

    @Property(nameInDb = "user_id")
    private Integer userId;

    @Property(nameInDb = "activity")
    private String activity;

    @Property(nameInDb = "date")
    private Date date;

    @Property(nameInDb = "strength")
    private Integer strength;

    @Property(nameInDb = "duration")
    private Integer duration;

    @Property(nameInDb = "category")
    private String category;

    @Generated(hash = 1588787307)
    public PhyActivityRecord(Long id, Integer userId, String activity, Date date,
            Integer strength, Integer duration, String category) {
        this.id = id;
        this.userId = userId;
        this.activity = activity;
        this.date = date;
        this.strength = strength;
        this.duration = duration;
        this.category = category;
    }

    @Generated(hash = 1982197134)
    public PhyActivityRecord() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getUserId() {
        return this.userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getActivity() {
        return this.activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public Date getDate() {
        return this.date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getStrength() {
        return this.strength;
    }

    public void setStrength(Integer strength) {
        this.strength = strength;
    }

    public Integer getDuration() {
        return this.duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getCategory() {
        return this.category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}

