package com.dmc.myapplication.phyActivityRecord;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.dmc.myapplication.R;
import com.dmc.myapplication.phyActivityRecord.db.PhyActivityRecord;
import com.dmc.myapplication.phyActivityRecord.helper.PhyActivityStrengthEnum;
import com.dmc.myapplication.phyActivityRecord.helper.PhyDBHelper;
import com.hookedonplay.decoviewlib.DecoView;
import com.hookedonplay.decoviewlib.charts.SeriesItem;
import com.hookedonplay.decoviewlib.events.DecoEvent;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.apache.commons.lang.time.DateUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ChartPhyActivity extends AppCompatActivity {

    class PhySummary {
        public Integer duration;
        public Float percentage;

        PhySummary(Integer duration, Float percentage) {
            this.duration = duration;
            this.percentage = percentage;
        }
    }

    DecoView arcView;
    TextView tvHigh;
    TextView tvMedium;
    TextView tvLow;
    TextView tvCustomDateRange;
    TextView tvSum;

    ArrayList<Integer> seriesHighIndexes = new ArrayList<>();
    final List<String> spinnerListItem = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phy_chart);
        setTitle("活動記錄統計");
        arcView = findViewById(R.id.dynamicArcView);
        View borderHigh = findViewById(R.id.border_phy_chart_high);
        View borderMediumTitle = findViewById(R.id.border_phy_chart_medium);
        View borderLowTitle = findViewById(R.id.border_phy_chart_low);

        tvHigh = findViewById(R.id.tv_phy_chart_high);
        tvMedium = findViewById(R.id.tv_phy_chart_medium);
        tvLow = findViewById(R.id.tv_phy_chart_low);
        tvCustomDateRange = findViewById(R.id.tv_phy_chart_custom_date_range);
        tvSum = findViewById(R.id.tv_phy_chart_sum);

        Spinner spinner = findViewById(R.id.spinner_phy_chart);

        borderHigh.setBackgroundColor(PhyActivityStrengthEnum.getColor(PhyActivityStrengthEnum.HIGH.ordinal()));
        borderMediumTitle.setBackgroundColor(PhyActivityStrengthEnum.getColor(PhyActivityStrengthEnum.MEDIUM.ordinal()));
        borderLowTitle.setBackgroundColor(PhyActivityStrengthEnum.getColor(PhyActivityStrengthEnum.LOW.ordinal()));

        // follow preset
        spinnerListItem.add("本週統計");
        spinnerListItem.add("上週統計");
        spinnerListItem.add("本月統計");
        spinnerListItem.add("上月統計");
        spinnerListItem.add("自訂時間");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                R.layout.spinner_phy_chart_item, spinnerListItem);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                displayChart(spinnerListItem.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        int seriesHighIndex = arcView.addSeries(new SeriesItem.Builder(PhyActivityStrengthEnum.getColor(PhyActivityStrengthEnum.HIGH.ordinal()))
                .setRange(0, 100, 0)
                .setLineWidth(32f)
                .build());


        int seriesMediumIndex = arcView.addSeries(new SeriesItem.Builder(PhyActivityStrengthEnum.getColor(PhyActivityStrengthEnum.MEDIUM.ordinal()))
                .setRange(0, 100, 0)
                .setLineWidth(32f)
                .build());


        int seriesLowIndex = arcView.addSeries(new SeriesItem.Builder(PhyActivityStrengthEnum.getColor(PhyActivityStrengthEnum.LOW.ordinal()))
                .setRange(0, 100, 0)

                .setLineWidth(32f)
                .build());

        seriesHighIndexes.add(seriesLowIndex);
        seriesHighIndexes.add(seriesMediumIndex);
        seriesHighIndexes.add(seriesHighIndex);

        displayChart("本週統計");

    }

    private void updateChart(PhySummary[] ps) {
        float serisePercentage = 0f;
        int sumDuration = 0;
        for (int i = 0; i < ps.length; i++) {
            if (ps[i].percentage.isNaN()) {
                ps[i].percentage = 0f;
            }

            sumDuration += ps[i].duration;

            // set Text
            String txt = String.format("%d分鐘，%.2f%%", ps[i].duration, ps[i].percentage);
            switch (i) {
                case 0:
                    tvLow.setText(txt);
                case 1:
                    tvMedium.setText(txt);
                case 2:
                    tvHigh.setText(txt);
            }

            // update chart
            long chartDuration = 800;
            if (i == 2) {
                chartDuration = 500;
            }

            // Log.d("updateChart", ps[i].percentage + "");



            serisePercentage += ps[i].percentage;

            arcView.addEvent(new DecoEvent.Builder(DecoEvent.EventType.EVENT_SHOW, true).setIndex(seriesHighIndexes.get(i)).setDuration(chartDuration).build());
            arcView.addEvent(new DecoEvent.Builder(serisePercentage).setIndex(seriesHighIndexes.get(i)).setDuration(chartDuration).build());

            if (ps[i].percentage == 0 || ps[i].percentage.isNaN()) {
                //arcView.addEvent(new DecoEvent.Builder(DecoEvent.EventType.EVENT_HIDE, true).setIndex(seriesHighIndexes.get(i)).setDuration(chartDuration).build());
            }
        }

        tvSum.setText(String.format("總活動時間 %d 分鐘", sumDuration));
    }

    private void displayChart(String preset) {
        PhySummary[] ps;

        // follow list name
        switch (preset) {
            case "本週統計":
                Calendar calendar = Calendar.getInstance();
                int day = calendar.get(Calendar.DAY_OF_WEEK);
                // Log.d("本週統計", DateUtils.truncate(DateUtils.addDays(new Date(), Calendar.MONDAY - day), Calendar.DATE) + "  " + DateUtils.truncate(DateUtils.addDays(new Date(), day - Calendar.SUNDAY), Calendar.DATE));
                ps = getPhyActivitySummary(DateUtils.truncate(DateUtils.addDays(new Date(), Calendar.MONDAY - day), Calendar.DATE), DateUtils.truncate(DateUtils.addDays(new Date(), day - Calendar.SUNDAY), Calendar.DATE));
                break;
            case "上週統計":
                calendar = Calendar.getInstance();
                day = calendar.get(Calendar.DAY_OF_WEEK);
                // Log.d("上週統計", DateUtils.truncate(DateUtils.addDays(new Date(), Calendar.MONDAY - day - 7), Calendar.DATE) + "  " + DateUtils.truncate(DateUtils.addDays(new Date(), day - Calendar.SUNDAY -7), Calendar.DATE));
                ps = getPhyActivitySummary(DateUtils.truncate(DateUtils.addDays(new Date(), Calendar.MONDAY - day - 7), Calendar.DATE), DateUtils.truncate(DateUtils.addDays(new Date(), day - Calendar.SUNDAY - 7), Calendar.DATE));
                break;
            case "本月統計":
                // Log.d("本月統計", DateUtils.truncate(new Date(), Calendar.MONTH) + "  " + DateUtils.ceiling(new Date(), Calendar.MONTH));
                ps = getPhyActivitySummary(DateUtils.truncate(new Date(), Calendar.MONTH), DateUtils.ceiling(new Date(), Calendar.MONTH));
                break;
            case "上月統計":
                Date prevMonth = DateUtils.addMonths(new Date(), -1);
                // Log.d("上月統計", DateUtils.truncate(prevMonth, Calendar.MONTH) + "  " + DateUtils.ceiling(prevMonth, Calendar.MONTH));
                ps = getPhyActivitySummary(DateUtils.truncate(prevMonth, Calendar.MONTH), DateUtils.ceiling(prevMonth, Calendar.MONTH));

                break;
            case "自訂時間":
                View mView = LayoutInflater.from(this).inflate(R.layout.dialog_phy_chart_custom_date_range, null);
                final TextView tvFrom = mView.findViewById(R.id.tv_phy_chart_custom_date_range_from);
                final TextView tvTo = mView.findViewById(R.id.tv_phy_chart_custom_date_range_to);

                final Calendar calendarFrom = Calendar.getInstance();
                final Calendar calendarTo = Calendar.getInstance();

                tvFrom.setText(calendarFrom.get(Calendar.YEAR) + "年" + (calendarFrom.get(Calendar.MONTH) + 1) + "月" + calendarFrom.get(Calendar.DAY_OF_MONTH) + "日");
                tvTo.setText(calendarFrom.get(Calendar.YEAR) + "年" + (calendarFrom.get(Calendar.MONTH) + 1) + "月" + calendarFrom.get(Calendar.DAY_OF_MONTH) + "日");

                tvFrom.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        // open date picker
                        DatePickerDialog dpd = DatePickerDialog.newInstance(
                                new DatePickerDialog.OnDateSetListener() {
                                    @Override
                                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                        tvFrom.setText(year + "年" + (monthOfYear + 1) + "月" + dayOfMonth + "日");
                                        calendarTo.set(year, monthOfYear, dayOfMonth);
                                        calendarFrom.set(year, monthOfYear, dayOfMonth);
                                    }
                                },
                                calendarFrom.get(Calendar.YEAR),
                                calendarFrom.get(Calendar.MONTH),
                                calendarFrom.get(Calendar.DAY_OF_MONTH)
                        );
                        dpd.show(getFragmentManager(), "Datepickerdialog");
                    }
                });

                tvTo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // open date picker
                        DatePickerDialog dpd = DatePickerDialog.newInstance(
                                new DatePickerDialog.OnDateSetListener() {
                                    @Override
                                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                        tvTo.setText(year + "年" + (monthOfYear + 1) + "月" + dayOfMonth + "日");
                                        calendarTo.set(year, monthOfYear, dayOfMonth);
                                    }
                                },
                                calendarTo.get(Calendar.YEAR),
                                calendarTo.get(Calendar.MONTH),
                                calendarTo.get(Calendar.DAY_OF_MONTH)
                        );
                        dpd.show(getFragmentManager(), "Datepickerdialog");
                    }
                });

                AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(this);
                alertDialogBuilderUserInput.setView(mView);
                alertDialogBuilderUserInput
                        .setTitle("新增自訂活動")
                        .setPositiveButton("完成", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogBox, int id) {
                                // Log.d("自訂時間統計", DateUtils.truncate(calendarFrom.getTime(), Calendar.DATE) + "  " + DateUtils.addDays(DateUtils.truncate(calendarTo.getTime(), Calendar.DATE), 1));
                                PhySummary[] ps = getPhyActivitySummary(DateUtils.truncate(calendarFrom.getTime(), Calendar.DATE), DateUtils.addDays(DateUtils.truncate(calendarTo.getTime(), Calendar.DATE), 1));
                                updateChart(ps);

                                tvCustomDateRange.setText("自訂時間： " + tvFrom.getText() + "至" + tvTo.getText());
                            }
                        })
                        .setNegativeButton("取消",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogBox, int id) {

                                    }
                                });

                AlertDialog alertDialog = alertDialogBuilderUserInput.create();
                alertDialog.show();
                return;
            default:
                return;
        }

        // reset 自訂時間
        tvCustomDateRange.setText("");

        // update chart
        updateChart(ps);
    }

    private PhySummary[] getPhyActivitySummary(Date from, Date to) {
        List<PhyActivityRecord> list = PhyDBHelper.getRecords(from, to);

        // array index = strength enum
        PhySummary[] ps = new PhySummary[]{
                new PhySummary(0, 0f),
                new PhySummary(0, 0f),
                new PhySummary(0, 0f),
        };

        float sumDuration = 0;
        for (PhyActivityRecord par : list) {
            // 3 strength type
            if (par.getStrength() < 0 && par.getStrength() >= 3) {
                continue;
            }

            ps[par.getStrength()].duration += par.getDuration();
            sumDuration += par.getDuration();

        }

        for (int i = 0; i < ps.length; i++) {
            ps[i].percentage = ps[i].duration / sumDuration * 100;

        }

        return ps;
    }
}