package com.dmc.myapplication.phyActivityRecord.db;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

import java.io.Serializable;

/**
 * Created by terry on 2/1/2018.
 */

@Entity(nameInDb = "phy_activity")
public class PhyActivity implements Serializable {
        static final long serialVersionUID = 536871008;
        
        @Id(autoincrement = true)
        private Long id;

        @Property(nameInDb = "activity")
        private String activity;

        @Property(nameInDb = "strength")
        private Integer strength;

        @Property(nameInDb = "category")
        private String category;

        @Generated(hash = 846913022)
        public PhyActivity(Long id, String activity, Integer strength,
                String category) {
            this.id = id;
            this.activity = activity;
            this.strength = strength;
            this.category = category;
        }

        @Generated(hash = 1146946412)
        public PhyActivity() {
        }

        public Long getId() {
            return this.id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getActivity() {
            return this.activity;
        }

        public void setActivity(String activity) {
            this.activity = activity;
        }

        public Integer getStrength() {
            return this.strength;
        }

        public void setStrength(Integer strength) {
            this.strength = strength;
        }

        public String getCategory() {
            return this.category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

}
