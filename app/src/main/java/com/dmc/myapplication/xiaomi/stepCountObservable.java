package com.dmc.myapplication.xiaomi;

import android.content.Context;
import com.dmc.myapplication.Models.stepRecord;

import java.util.Observable;

/**
 * Created by januslin on 11/1/2017.
 */
public class stepCountObservable extends Observable {
    private static stepCountObservable ourInstance = new stepCountObservable();

    public static stepCountObservable getInstance() {
        return ourInstance;
    }

    private stepCountObservable() {
    }


    public static Integer count = 0;

    public void setData(int data, Context context){
        stepRecord stepRecordDB = new stepRecord(context);

        if(data > stepRecordDB.getTodayLatestStep()){
            //假如資料有變動
            this.count = data;
            stepRecordDB.insertOrUpdate(data, context);
            setChanged();
            //設定有改變
        }
        stepRecordDB.close();
        notifyObservers();
        //發送給觀察者
    }

    public void setDBDatatoPresent(int data){

            this.count = data;
            setChanged();
        notifyObservers();
        //發送給觀察者
    }

}
