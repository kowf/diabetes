package com.dmc.myapplication.prediction;

/**
 * Created by lamivan on 30/8/2017.
 */

public class MSPredictionData {
  public String TagId;
  public String Tag;
  public String Probability;

  @Override
  public String toString(){
    return Tag + " - " + Probability;
  }
}
