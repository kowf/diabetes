package com.dmc.myapplication.prediction;

/**
 * Created by lamivan on 30/8/2017.
 */

public class MSPredictList {
  public String Id;
  public String Project;
  public String Iteration;
  public String Created;
  public MSPredictionData[] Predictions;

}
