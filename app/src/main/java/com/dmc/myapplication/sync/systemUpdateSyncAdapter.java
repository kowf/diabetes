package com.dmc.myapplication.sync;

import android.accounts.Account;
import android.content.*;
import android.os.Bundle;
import android.util.Log;
import com.dmc.myapplication.Models.exercise_type;
import com.dmc.myapplication.Models.sync_data_version;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.systemConstant;
import com.dmc.myapplication.webHelper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import javax.net.ssl.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.List;

/**
 * Created by januslin on 25/12/2016.
 */
public class systemUpdateSyncAdapter extends AbstractThreadedSyncAdapter {
    ContentResolver contentResolver;

    public systemUpdateSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);

        contentResolver = context.getContentResolver();
    }

    public systemUpdateSyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);

        contentResolver = context.getContentResolver();
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {

        // this method is on background thread
        // perform network tasks here
        // I am just inserting data in database manually

        System.out.println("System Update Sync Working!");
        /*sync_data_version sdvDB = new sync_data_version(getContext());
        if (sdvDB.get("EXERCISE_TYPE") == null){
            sdvDB.insert(sdvDB.new sync_data_version_class("EXERCISE_TYPE", 0));
            sdvDB.insert(sdvDB.new sync_data_version_class("FOOD", 0));
            sdvDB.insert(sdvDB.new sync_data_version_class("FOOD_CATEGORIES", 0));
            sdvDB.insert(sdvDB.new sync_data_version_class("FOOD_SUB_CATEGORIES", 0));

        }*/

        checkLatestVersion();

    }

    private void checkLatestVersion(){
        UserLocalStore userLocalStore = new UserLocalStore(getContext());

        String responce = webHelper.sendRequest("POST", "userId="+userLocalStore.getLoggedInUser().userid+"&event=syncdataversion&method=getLatestVersion&data=");
        if (responce != null){
            Gson gson = new Gson();
            HashMap<String, Integer> latestVersion = gson.fromJson(responce, new TypeToken<HashMap<String, Integer>>(){}.getType());
            sync_data_version sdvDB = new sync_data_version(getContext());
            for (int x = 0; x < latestVersion.size(); x ++){
                if (sdvDB.get(latestVersion.keySet().toArray()[x].toString()).getVERSION() != latestVersion.get(latestVersion.keySet().toArray()[x].toString())){
                    switch (latestVersion.keySet().toArray()[x].toString()){
                        case "EXERCISE_TYPE":
                            updateExerciseTypeTable("EXERCISE_TYPE", latestVersion.get(latestVersion.keySet().toArray()[x].toString()));
                            break;

                        default:
                            break;
                    }
                }
            }
        }
    }

    private void updateExerciseTypeTable(String TABLE, Integer VERSION){
        exercise_type etDB = new exercise_type(getContext());
        etDB.deleteAll();

        UserLocalStore userLocalStore = new UserLocalStore(getContext());
        String responce = webHelper.sendRequest("POST", "userId="+userLocalStore.getLoggedInUser().userid+"&event=syncdataversion&method=getLatestExerciseType&data=");
        Gson gson = new Gson();
        List<HashMap<String, String>> newData = gson.fromJson(responce, new TypeToken<List<HashMap<String, String>>>(){}.getType());
        for (int x = 0; x < newData.size(); x++){
            etDB.insert(etDB.new exercise_type_class(Integer.parseInt(newData.get(x).get("EXERCISE_TYPE_ID")), newData.get(x).get("EXERCISE_TYPE_NAME")));
        }

        //UpdateVersion
        updateLocalVersion(TABLE, VERSION);
    }

    private void updateLocalVersion(String TABLE, Integer VERSION){
        sync_data_version sdvDB = new sync_data_version(getContext());
        sdvDB.update(sdvDB.new sync_data_version_class(TABLE, VERSION));
    }

}
