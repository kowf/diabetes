package com.dmc.myapplication.sync;

import android.accounts.Account;
import android.accounts.OperationCanceledException;
import android.app.Service;
import android.content.*;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;

import java.util.Date;

/**
 * Created by januslin on 15/12/2016.
 */
public class dataSyncService extends Service {
    private static final String TAG = "dataSyncService";
    private boolean isRunning  = false;

    dataSyncService(){
        super();
    }

    @Override
    public void onCreate() {
        System.out.println(TAG+" Started!");


        try {
            while (true) {
                System.out.println(new Date());
                Thread.sleep(2 * 1000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    @Override
    public void onDestroy() {
        System.out.println(TAG+" Destroy!");

    }

}
